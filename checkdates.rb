#!/usr/bin/env ruby

require 'yaml'
require 'date'

def yaml_load(text)
  # not sure if 3.0.0 is the right threshold, but it works
  if (RUBY_VERSION.split(".").map(&:to_i) <=> [3,0,0]) < 0
    YAML.load(text)
  else
    YAML.load(text, permitted_classes: [Symbol, Date, Time])
  end
end

class YAMLFrontMatter
  PATTERN = /\A(---\r?\n(.*?)\n?^---\s*$\n?)/m.freeze

  class << self
    def extract(content)
      if content =~ PATTERN
        [yaml_load(Regexp.last_match(2)), content.sub(Regexp.last_match(1), "")]
      else
        [{}, content]
      end
    end
  end
end

Dir["content/eventos/*/index.md"].each do |filename|
  metadata, _ = YAMLFrontMatter.extract(File.read(filename, encoding: 'UTF-8'))

  raise "#{filename}: wrong layout: #{metadata["layout"]}" unless metadata["layout"] == "evento"
  raise "#{filename}: no title" unless metadata.keys.include?("title")
  raise "#{filename}: no metadata" unless metadata.keys.include?("metadata")

  raise "#{filename}: no event location" unless metadata.dig("metadata", "event", "location")

  start = metadata.dig("metadata", "event", "date", "start")
  finish = metadata.dig("metadata", "event", "date", "finish")

  raise "#{filename}: no event start date" unless start
  raise "#{filename}: no event finish date" unless finish

  raise "#{filename}: time paradox: #{start} - #{finish}" unless start.to_time <= finish.to_time
end
