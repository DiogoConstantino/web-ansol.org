---
layout: article
title: Diz Não ao DRM
featured: true
date: 2012-03-25
---

A [diretiva europeia
2001/29/CE](http://en.wikipedia.org/wiki/Copyright_Directive), que a ANSOL
acompanhou, resultou na lei
[50/2004](https://dre.pt/dre/detalhe/lei/50-2004-479605).
Entre outros problemas, dá força legal ao DRM.

A ANSOL tem-se debatido contra o DRM, e tem um site de campanha em
[drm-pt.info](https://drm-pt.info/)
