---
layout: evento
title: Workshop de Introdução ao Flutter
metadata:
  event:
    location: Calçada do Moinho de Vento, 14B, 1150-236 Lisboa
    site:
      url: https://centrolinux.pt/post/2023-fevereiro-workshop-flutter/
    date:
      start: 2023-03-18 14:30:00.000000000 +01:00
      finish: 2023-03-18 18:30:00.000000000 +01:00
---

[![Cartaz](cartaz.png)](https://centrolinux.pt/post/2023-fevereiro-workshop-flutter/)

Cria aplicações para qualquer ecrã, usando Flutter em Ubuntu.

Detalhes na página do Centro Linux para o [evento](https://centrolinux.pt/post/2023-fevereiro-workshop-flutter/).
