---
layout: evento
title: Workshop de Introdução ao Linux
metadata:
  event:
    location: Sala 4.1.02, no DETI, Universidade de Aveiro
    site:
      url: https://glua.ua.pt/workshop-introducao-linux-2023/
    date:
      start: 2023-10-24 18:00:00
      finish: 2023-10-24 20:00:00
---

![Cartaz](cartaz.png)

# Workshop de Introdução ao Linux

Já conheces o Linux? Sabes como funciona? Gostarias de aprender?


O GLUA preparou o Workshop de Introdução ao Linux! Queremos ajudar a que não te sintas tão perdido.


Talvez até te divirtas com o nosso amigo pinguim... 👀


🗓️ **Terça, 24 de outubro de 2023**

🕕 **18:00h**

📍 **Sala 4.1.02, no DETI**

### Inscrições:
[https://bit.ly/glua-wil23](https://bit.ly/glua-wil23)

### Requisitos:

- Computador ou Máquina Virtual com Linux ([Tutorial de instalação de Ubuntu Linux em Dual-boot](https://www.youtube.com/watch?v=42FogLuA48w))
- Vontade de aprender
