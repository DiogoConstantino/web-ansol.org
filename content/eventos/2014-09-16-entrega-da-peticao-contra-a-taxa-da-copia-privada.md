---
categories:
- "#pl118"
- "#pl246"
- cópia privada
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 44
  - tags_tid: 45
  - tags_tid: 11
  node_id: 223
  event:
    location: Assembleia da República
    site:
      title: ''
      url: https://ansol.org/pr-20140916-2
    date:
      start: 2014-09-17 12:00:00.000000000 +01:00
      finish: 2014-09-17 15:00:00.000000000 +01:00
    map: {}
layout: evento
title: Entrega da Petição contra a Taxa da Cópia Privada
created: 1410892283
date: 2014-09-16
aliases:
- "/evento/223/"
- "/node/223/"
---
<p>A ANSOL (Associação Nacional para o Software Livre) opõe-se à proposta de lei 246/XII e deslocar-se-á às 12:00 à porta principal da Assembleia da República para entrega da petição.</p><p>«<em>O Parlamento não pode ignorar esta petição, que reuniu mais de 4000 assinaturas em cerca de 24h</em>», diz Rui Seabra, presidente da Direção da ANSOL referindo-se ao regulamento da Assembleia da República obriga a que as petições com mais de 4000 subscritores sejam debatidas em Plenário.</p><p>Acrescenta ainda que «<em>é imperativo suspender o processo de discussão da PL 246 e dar oportunidade a uma discussão pública e aberta sobre conteúdos digitais e a&nbsp; reforma do direito de autor</em>».</p><p>A petição conta já com quase 4500 assinaturas e encontra-se disponível em <a href="http://bit.do/pl246">http://bit.do/pl246</a>.</p>
