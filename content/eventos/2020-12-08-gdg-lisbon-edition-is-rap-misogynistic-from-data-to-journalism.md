---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 768
  event:
    location: Online
    site:
      title: ''
      url: https://eu-sessions.gdgmadeira.xyz/events/gdg-tech-sessions-gdg-lisbon-edition-from-data-to-journalism/
    date:
      start: 2020-12-09 18:00:00.000000000 +00:00
      finish: 2020-12-09 18:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'GDG Lisbon Edition: Is rap misogynistic? From data to journalism'
created: 1607438261
date: 2020-12-08
aliases:
- "/evento/768/"
- "/node/768/"
---

