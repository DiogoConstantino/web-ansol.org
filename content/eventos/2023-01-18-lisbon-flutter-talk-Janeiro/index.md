---
layout: evento
title: Lisbon Flutter Talk
metadata:
  event:
    location: Volkswagen Digital Solutions, Unipessoal Lda - Rua do Sol ao Rato 11 · Lisboa
    site:
      url: https://www.meetup.com/flutterportugal/events/290595256/
    date:
      start: 2023-01-18 18:00:00.000000000 +00:00
      finish: 2023-01-18 20:00:00.000000000 +00:00
---

# Lisbon Flutter Talk

Welcome to the Flutter community, we work to bring Flutter developers together and welcome new developers. There will be talks, workshops, and showcases, at various cities in Portugal.

- Event will be in English.
- Will not be recorded.
- Will not be streamed.
- 100% In-Person.
- Want to have an event at your city? DM us.

## 📅 Schedule
18:00 - Welcoming
18:10 - Accelerating Flutter development with modern low-code tools
19:30 - Community Hangout


## 🎁 Talk:

    Accelerating development with modern low-code tools
    Target Audience : Beginner
    Leveraging Flutterflow to build lightning fast MVPs and reducing the total time to build an app


## 📍Host - Volkswagen Digital Solutions
A special thanks to Volkswagen Digital in accepting us with open arms. Big Kudos

---

💛 Flutter Portugal Social
Twitter https://twitter.com/FlutterPortugal
Chat: http://chat.flutter.pt
