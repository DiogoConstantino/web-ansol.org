---
categories:
- maker
- open source
- free software
- hardware
- meeting
- fair
- feira
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 138
  - tags_tid: 127
  - tags_tid: 122
  - tags_tid: 136
  - tags_tid: 129
  - tags_tid: 267
  - tags_tid: 268
  node_id: 582
  event:
    location: FEZ-Berlin, Berlin, Alemanha
    site:
      title: MakerFair Berlin
      url: https://en.maker-faire.de/berlin/
    date:
      start: 2018-05-26 00:00:00.000000000 +01:00
      finish: 2018-05-27 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: MakerFair Berlin 2018
created: 1522863343
date: 2018-04-04
aliases:
- "/evento/582/"
- "/node/582/"
---
<h3>A festival of invention, creativity and resourcefulness.</h3><p><big>Coming to&nbsp;Berlin for the fourth time in 2018! See you again on May 26 and 27, 2018 at our new venue: FEZ-Berlin!</big></p>
