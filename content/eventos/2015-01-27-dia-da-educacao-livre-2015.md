---
categories:
- efd
- eventos
- encontro
- '2015'
- educação
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 72
  - tags_tid: 73
  - tags_tid: 74
  - tags_tid: 75
  - tags_tid: 76
  node_id: 273
  event:
    location: Entrada do Centro Comercial Vasco da Gama
    site:
      title: 
      url: 
    date:
      start: 2015-03-21 18:30:00.000000000 +00:00
      finish: 2015-03-21 18:30:00.000000000 +00:00
    map: {}
layout: evento
title: Dia da Educação Livre 2015
created: 1422372212
date: 2015-01-27
aliases:
- "/EFD2015/"
- "/evento/273/"
- "/node/273/"
---
<img src="https://ansol.org/attachments/efd.png" alt="EFD 2015">

**Dia da Educação Livre** é uma celebração mundial dos [**Recursos Educacionais
Livres**](http://www.educationfreedomday.org/about/free-educational-resources).
Iniciado em 2013 pela mesma organização por detrás do <a
href="https://ansol.org/sfd2014">Software Freedom Day</a>, o evento tem como
objectivo educar o público mundial sobre os benefícios de usar Software Livre e
Recursos Educacionais Livres na educação. Providencia também um dia
internacional para server como plataforma para aumentar o conhecimento de
projectos e comunidades existentes por todo o mundo, bem como encorajar a
participalão em iniciativas locais de Software Livre e Recursos Educacionais
Livres. A organização sem fins lucrativos <a
href="http://www.digitalfreedomfoundation.org" target="_blank"><strong>Digital
Freedom Foundation</strong></a> coordena o EFD a um nível global,
providenciando apoio e um ponto de colaboração, e a ANSOL colabora com ela
também neste evento, celebrando-o em Portugal.

O Dia da Educação Livre será celebrado em Portugal num evento-convívio a
ocorrer em Lisboa, organizado pela ANSOL, em que todos serão bem vindos.

* **18:30** - encontro perto da entrada (virada para o rio) do Centro Comercial
  Vasco da Gama (Lisboa), onde estaremos para convívio e para falar com a
  população em geral sobre a importância da Educação Livre, incluindo a
  distribuição de material informativo;
* **20:00** - jantar-convívio
