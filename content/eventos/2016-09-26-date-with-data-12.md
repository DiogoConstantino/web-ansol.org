---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 460
  event:
    location: 
    site:
      title: ''
      url: http://www.transparenciahackday.org/2016/09/1-de-outubro-date-with-data-12/
    date:
      start: 2016-10-01 00:00:00.000000000 +01:00
      finish: 2016-10-01 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Date With Data #12'
created: 1474923255
date: 2016-09-26
aliases:
- "/evento/460/"
- "/node/460/"
---
<p>Vamos falar de turismo? Na nossa demanda por informação pública acessível a todas as pessoas, o tema do turismo veio ao de cima como algo que devíamos debruçar-nos. E é exatamente isso que vamos fazer no próximo&nbsp;<a href="http://datewithdata.pt/" target="_blank">Date With Data</a>!</p><p>&nbsp;</p>
