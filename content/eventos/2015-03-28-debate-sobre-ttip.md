---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 303
  event:
    location: Lisboa
    site:
      title: ''
      url: https://www.nao-ao-ttip.pt/debate-dia-16-de-abril-spgl-lisboa/
    date:
      start: 2015-04-16 00:00:00.000000000 +01:00
      finish: 2015-04-16 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Debate sobre TTIP
created: 1427561116
date: 2015-03-28
aliases:
- "/evento/303/"
- "/node/303/"
---

