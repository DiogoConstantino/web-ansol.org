---
layout: evento
title: Devops na Praia #2
metadata:
  event:
    location: Wunderman Thompson Commerce - Edifício Transparente, Porto
    site:
      url: https://www.eventbrite.pt/e/bilhetes-devops-na-praia-2-720753933417?aff=oddtdtcreator
    date:
      start: 2023-10-12 18:00:00
      finish: 2023-10-12 21:00:00
---

> Venha participar do segundo encontro do DevOps na praia! Neste evento, vamos
> reunir profissionais e entusiastas de DevOps para compartilhar conhecimentos
> e experiências de forma descontraída e informal.
>
> Aproveite a oportunidade de se conectar com pessoas da área, trocar ideias e
> expandir sua rede de contatos. O evento será realizado no dia Thu Oct 12
> 2023, a partir das 18:00, no seguinte endereço: 395 Via do Castelo do Queijo,
> 4100-429 Porto.
>
> Traga suas perguntas, desafios e curiosidades sobre DevOps e junte-se a nós
> para uma noite divertida e enriquecedora. Não perca essa chance de aprimorar
> suas habilidades e se atualizar sobre as últimas tendências da área.
>
> Inscreva-se agora mesmo e garanta sua vaga no DevOps na praia #2. Nos vemos
> lá!
