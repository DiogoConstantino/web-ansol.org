---
categories:
- sfd2016
- sfd
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 163
  - tags_tid: 99
  node_id: 414
  event:
    location: Auditório JJ Laginha (Edifício I), ISCTE, Lisboa
    site:
      title: 
      url: 
    date:
      start: 2016-09-17 14:30:00.000000000 +01:00
      finish: 2016-09-17 19:00:00.000000000 +01:00
    map: {}
layout: evento
title: Dia do Software Livre 2016
created: 1460391539
date: 2016-04-11
aliases:
- "/SFD2016/"
- "/evento/414/"
- "/node/414/"
---
Como todos os anos, a ANSOL irá celebrar o Dia do Software Livre em Portugal!

<img src="https://ansol.org/attachments/IMG_20150919_134644.jpg" alt="Software Freedom Day 2015">

Este ano teremos uma tarde de apresentações e convívio, a decorrer no Auditório JJ Laginha, no ISCTE, em Lisboa.

### Programa:

* <strong>14:30</strong> - Sessão de Abertura
* <strong>15:15 </strong>- Alguns Aspectos do Software Livre no Brasil - <em>Lis Rodrigues</em>
* <strong>16:15</strong> -&nbsp;Raspberry Pi: o lado do software - <em>Manuel Silva</em>
* <strong>17:15</strong> - Intervalo/Convívio
* <strong>17:45</strong> - OOZ2Mars - <a href="http://labs.oneoverzero.org/"><em>Luis Correia</em></a>
* <strong>18:45</strong> - Sessão de Encerramento

Este evento tem o apoio do <a href="http://moss.dcti.iscte.pt/">MOSS</a> e da
<a href="http://iscte.acm.org/">ISCTE-IUL ACM</a>.

A entrada é livre e gratuita, não sendo necessária qualquer inscrição: é só
aparecer!

<hr>

O Dia do Software Livre é um evento internacional celebrado anualmente um pouco
por todo o mundo, desde 2004. Pode saber mais informação sobre este evento, e o
que vai acontecer nos outros países, no <a
href="http://softwarefreedomday.org/">site internacional</a>.
