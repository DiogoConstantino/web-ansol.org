---
layout: evento
title: "Privacy Café: Workshops de Segurança na Internet"
showcover: false
metadata:
  event:
    location: Biblioteca dos Coruchéus, Lisboa
    site:
      url: https://privacylx.org/events/2024-01-13-privacy-cafe/
    date:
      start: 2024-01-13 15:00:00
      finish: 2024-01-13 19:00:00
---

![](cartaz.jpg)

- 15:00 - 16:00: Introdução à Privacidade e Segurança Digital
- 16:15 - 17:45: Redes e Capitalismo de Plataformas
- 18:00 - 19:00: Ameaças Digitais ao Jornalismo em Portugal
