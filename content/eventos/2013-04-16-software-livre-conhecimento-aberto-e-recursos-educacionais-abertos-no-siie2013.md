---
categories: []
metadata:
  node_id: 139
  event:
    location: Viseu
    site:
      title: ''
      url: http://siie13esev.ipv.pt/
    date:
      start: 2013-11-13 00:00:00.000000000 +00:00
      finish: 2013-11-15 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Software Livre, Conhecimento Aberto e Recursos Educacionais Abertos no SIIE2013
created: 1366138214
date: 2013-04-16
aliases:
- "/evento/139/"
- "/node/139/"
---
Está aberta a Chamada de Trabalhos para o Simpósio Internacional de Informática Educativa (SIIE) 2013 e um dos tópicos para submissão é Software Livre, Conhecimento Aberto e Recursos Educacionais Abertos.

Aqui ficam as datas importantes:
15/06/2013 - Data limite para submissão de trabalhos
29/7/2013 - Notificação dos autores sobre aceitação dos trabalhos.
9/9/2013 - Envio da versão definitiva dos trabalhos aceites.
1/10/2013 - Limite para a inscrição de autores com trabalhos aceites
13/11/2013 - Início do SIIE 2013
