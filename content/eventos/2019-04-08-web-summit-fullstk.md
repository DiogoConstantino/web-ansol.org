---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 664
  event:
    location: Lisbon
    site:
      title: ''
      url: https://websummit.com/fullstk
    date:
      start: 2019-11-04 00:00:00.000000000 +00:00
      finish: 2019-11-07 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Web Summit - FullSTK
created: 1554720262
date: 2019-04-08
aliases:
- "/evento/664/"
- "/node/664/"
---
<p>Within the Web Summit, FullSTK,&nbsp;the world’s leading developer conference, showcases some of the world’s most impressive developers, engineers, investors and data scientists.</p><p>One of FullSTK's four tracks is dedicated to "Open Source".</p>
