---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 731
  event:
    location: MILL, Lisboa
    site:
      title: ''
      url: https://privacylx.org/events/privacy-cafe-mill-feb2020/
    date:
      start: 2020-02-15 00:00:00.000000000 +00:00
      finish: 2020-02-15 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Privacy Cafe 001
created: 1580839642
date: 2020-02-04
aliases:
- "/evento/731/"
- "/node/731/"
---

