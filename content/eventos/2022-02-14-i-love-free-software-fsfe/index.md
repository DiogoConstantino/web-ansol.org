---
layout: evento
title: I Love Free Software Day 2022 - Games Event
metadata:
  event:
    location: Online
    site:
      url: https://registration.fsfe.org/ilovefs
    date:
      start: 2022-02-14 17:00:00
      finish: 2022-02-14 19:00:00
---

> To say "Thank you" to all contributors to Free Software, the Free Software
> Foundation Europe organises and celebrates the "I Love Free Software Day". A
> day full of positive, creative and lovely messages directed to the whole Free
> Software community, celebrated on the 14th of February every year.
>
> For the I Love Free Software Day 2022, we are planning something special and
> fun, with a focus on Free Software games. We are organising an event
> dedicated to these games. We will have the participation of three experts in
> Free Software games, games engines and Game Jams. They will share their
> experiences, and will immerse us in the Free Software games world. For the
> second part of our event, we invite you to play some fun Free Software games
> with us. We will start with Verloren and afterwards we will play Armagetron
> Advanced.
