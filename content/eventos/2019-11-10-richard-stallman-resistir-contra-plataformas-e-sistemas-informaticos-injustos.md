---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 709
  event:
    location: 
    site:
      title: ''
      url: https://www.leffest.com/
    date:
      start: 2019-11-17 00:00:00.000000000 +00:00
      finish: 2019-11-17 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'Richard Stallman: Resistir contra Plataformas e Sistemas Informáticos Injustos'
created: 1573416445
date: 2019-11-10
aliases:
- "/evento/709/"
- "/node/709/"
---
<p>Richard Stallman no TEATRO TIVOLI BBVA, 17 de Novembro, às 15:30: Simpósio Resistências - PALESTRA: Resistir contra Plataformas e Sistemas Informáticos Injustos com Richard Stallman<br> <br> Mais info em <a href="https://www.leffest.com/descarregar-programa-pdf" target="_blank" rel="noreferrer" data-saferedirecturl="https://www.google.com/url?q=https://www.leffest.com/descarregar-programa-pdf&amp;source=gmail&amp;ust=1573502759796000&amp;usg=AFQjCNFB0ZPqrteh6TlXKMfPmCqchT4OUw">https://www.leffest.com/descarregar-programa-pdf</a></p>
