---
excerpt: "O Open Data Day é uma celebração anual dedicada aos dados abertos. Anualmente
  em Março, em vários locais do mundo, grupos organizam eventos dedicados aos dados
  abertos nas suas comunidades locais. É uma oportunidade para mostrar os benefícios
  dos dados abertos e incentivar à adoção de políticas abertas de dados junto do governo,
  empresas e sociedade civil.\r\n"
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAACIhTshwNxE2h5fk0RA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.4115134034784e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.8616253137589e1
    mapa_left: !ruby/object:BigDecimal 27:-0.8616253137589e1
    mapa_top: !ruby/object:BigDecimal 27:0.4115134034784e2
    mapa_right: !ruby/object:BigDecimal 27:-0.8616253137589e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.4115134034784e2
    mapa_geohash: ez3f5gmgftzfzvrg
  slide:
  - slide_value: 1
  node_id: 652
  event:
    location: Praça Coronel Pacheco, 2 4050-453 Porto, Portugal
    site:
      title: Transparencia Hack Day
      url: http://www.transparenciahackday.org/2019/03/open-data-day-2019/
    date:
      start: 2019-03-09 10:30:00.000000000 +00:00
      finish: 2019-03-09 17:30:00.000000000 +00:00
    map: {}
layout: evento
title: Open Data Day 2019
created: 1551992572
date: 2019-03-07
aliases:
- "/evento/652/"
- "/node/652/"
---
<div class="ace-line"><div id="magicdomid4" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">Sábado, 9 de Março, celebramos o </span><a href="https://opendataday.org/"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3 u">Open Data Day</span></a><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3"> 2019, em Portugal e na cidade do Porto.&nbsp;</span></div><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3 b"><strong><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">O </span><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3 u">Open Data Day</span><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3"> é uma celebração anual dedicada aos </span><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3 u"><span style="text-decoration: underline;">dados abertos</span></span><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">. </span><span class="author-a-0m3nfdjwl1tz80zpxvc">Anualmente em Março</span><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">, em vários locais do mundo, grupos organizam eventos dedicados aos dados abertos nas suas comunidades locais. É uma oportunidade para mostrar os benefícios dos dados abertos e incentivar à adoção de políticas abertas de dados junto do governo, empresas e sociedade civil.<br><br></span></span>Para participar registem-se <a href="https://www.eventbrite.com/e/open-data-day-porto-tickets-57942109409">aqui</a>!</strong></span></div><div class="ace-line">&nbsp;<span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3 b"><strong><br>Manhã: Sessão especial de trabalho</strong></span></div><div id="magicdomid16" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">10:30 – 11:00: Receção<br> </span></div><div id="magicdomid17" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">11:00 – 13:30: Workshop Wikihacking com Waldir Pimenta da <a href="https://pt.wikimedia.org/wiki/Wikimedia_Portugal">Wikimedia</a> (traz o teu computador portátil)</span></div><div id="magicdomid18" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">13:30 – 15:00: Almoço (livre)</span></span></div><div id="magicdomid20" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3 b"><strong>Tarde: Sessão com oradores</strong></span></div><div id="magicdomid21" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">15:00 – 15:15: Receção e café</span></div><div id="magicdomid22" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">15:15 – 15:25: Sobre o Open Data Day</span></div><div id="magicdomid23" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">15:30 – 16:00: </span><span class="author-a-pz122zz84zp8z78zz90zbz72zb4z65zutz87zz70z">Daniel da Silva (<a href="https://developmentseed.org">Development Seed</a>)</span></div><div id="magicdomid24" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">16:00 – 16:30: </span><span class="author-a-pz122zz84zp8z78zz90zbz72zb4z65zutz87zz70z">Rui Barros (<a href="https://gitlab.com/Renascenca/dados">Renascença</a>)</span></div><div id="magicdomid25" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">16:30 – 17:00: Date With Data (a confirmar)</span></div><div id="magicdomid26" class="ace-line"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">17:00 – 17:30: Conversa aberta</span></div><p><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3"><span class="author-a-z87zs597uz90zqz122zz67zz73zw9hz73z3">Despedida</span></span></p>
