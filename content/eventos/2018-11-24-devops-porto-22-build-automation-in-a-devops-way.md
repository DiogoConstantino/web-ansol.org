---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 634
  event:
    location: 
    site:
      title: ''
      url: https://www.meetup.com/devopsporto/events/256370094
    date:
      start: 2018-11-27 00:00:00.000000000 +00:00
      finish: 2018-11-27 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'DevOps Porto #22: Build Automation in a DevOps way!'
created: 1543074115
date: 2018-11-24
aliases:
- "/evento/634/"
- "/node/634/"
---

