---
categories:
- security
- floss
- meeting
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 276
  - tags_tid: 273
  - tags_tid: 129
  node_id: 590
  event:
    location: Polytech Lille, Lille,  France
    site:
      title: Pass the Salt
      url: https://2018.pass-the-salt.org
    date:
      start: 2018-07-02 00:00:00.000000000 +01:00
      finish: 2018-07-04 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Pass the SALT 2018
created: 1522876240
date: 2018-04-04
aliases:
- "/evento/590/"
- "/node/590/"
---
<div class="row"><div class="col-lg-6 col-md-6 text-center"><div class="service-box"><h1>July 2-4, 2018 - Lille, France.</h1><h2><strong>A <span style="text-decoration: underline;">free</span> Security &amp; Free Software conference.</strong></h2><h1>For setting up bridges between Security communities and Free Software hackers!</h1><h3>Conferences</h3><p class="text-muted">Meet speakers coming to expose Free Software projects for Security purposes, Security topics in <strong>Free Software</strong> projects or open standards dealing with <strong>Security</strong> ! <br><strong>All the talks will be given in English</strong> in order to share the conference with non French speaking speakers in the more confortable manner.<br> You don't see what we are talking about ? Have a look to previous editions contents and <a href="https://2018.pass-the-salt.org/achievements/#speakers">speakers who came to speak</a> !</p></div></div><div class="col-lg-6 col-md-6 text-center"><div class="service-box"><h3>Networking</h3><p class="text-muted">Exchange experiences, set up new relationships with accessible attendees and speakers coming from very various areas from hackitivists to professional security reversers or Free Software enthusiasts.<br><br> Moreover these exchanges will be done in a beautiful place and in a warm and open-minded ambiance :)</p></div></div></div>
