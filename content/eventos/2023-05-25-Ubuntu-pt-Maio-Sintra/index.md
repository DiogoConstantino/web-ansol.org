---
layout: evento
title: Encontro Ubuntu-pt Maio @ Sintra
metadata:
  event:
    location: Avenida Movimento das Forças Armadas 5, 2710-436 Sintra, Portugal
    site:
      url: https://loco.ubuntu.com/events/ubuntu-pt/4331-encontro-ubuntu-pt-maio-sintra/
    date:
      start: 2023-05-25 20:00:00.000000000 +01:00
      finish: 2023-05-25 23:00:00.000000000 +01:00
---

[![Cartaz](https://ansol.org/eventos/2023-05-25-ubuntu-pt-maio-sintra/cover.png)](https://loco.ubuntu.com/events/ubuntu-pt/4331-encontro-ubuntu-pt-maio-sintra/)

Encontro da Comunidade Ubuntu-pt de Maio de 2023, em Sintra.

Vem conhecer a comunidade, conversar sobre Ubuntu, Software Livre e tecnologia em ambiente de convívio descontraído, e traz um amigo também.

Dia 25 de Maio a partir das 20h no Bar Saloon, em Sintra.
Detalhes e inscrição (não obrigatória) na [página do evento](https://loco.ubuntu.com/events/ubuntu-pt/4331-encontro-ubuntu-pt-maio-sintra/).
