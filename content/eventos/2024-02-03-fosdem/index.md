---
layout: evento
title: FOSDEM 2024
metadata:
  event:
    location: ULB Campus Solbosch, Bruxelas, Bélgica
    site:
      url: https://fosdem.org/2024/
    date:
      start: 2024-02-03
      finish: 2024-02-04
---
FOSDEM is a free event for software developers to meet, share ideas and
collaborate.

Every year, thousands of developers of free and open source software from all
over the world gather at the event in Brussels.
