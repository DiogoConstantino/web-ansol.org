---
categories: []
metadata:
  node_id: 93
  event:
    location: Porto
    site:
      title: ''
      url: http://portolinux.org
    date:
      start: 2012-11-10 10:00:00.000000000 +00:00
      finish: 2012-11-10 10:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro Técnico Porto Linux
created: 1350237676
date: 2012-10-14
aliases:
- "/evento/93/"
- "/node/93/"
---
<p>A gest&atilde;o de dados aplicacionais e de desenvolvimento de software s&atilde;o temas que, por vezes, colocam em conflito quem desenvolve software e quem o &ldquo;encomenda&rdquo;. Quem encomenda quer sempre o melhor desempenho, o melhor &ldquo;interface&rdquo; pelo melhor pre&ccedil;o no mais curto espa&ccedil;o de tempo; quem desenvolve quer sempre o c&oacute;digo mais ger&iacute;vel, mais sistematizado e expans&iacute;vel e tende a n&atilde;o ter grandes preocupa&ccedil;&otilde;es temporais para atingir os seus objetivos.</p>
<p>Apresenta&ccedil;&otilde;es:</p>
<ul>
	<li class="level1">
		<div class="li">
			<strong>How to un-lame your (relational) database</strong>, por <a class="urlextern" href="http://thasha-in-wonderland.blogspot.pt/" rel="nofollow" title="http://thasha-in-wonderland.blogspot.pt/">Katarzyna Kittel</a></div>
	</li>
	<li class="level1">
		<div class="li">
			<strong>Scrumify</strong>, por <a class="urlextern" href="http://www.pedrogustavotorres.com/" rel="nofollow" title="http://www.pedrogustavotorres.com/">Pedro Torres</a></div>
	</li>
</ul>
<p>10 de Novembro, pelas 10h, nas instala&ccedil;&otilde;es da <a class="urlextern" href="http://www.oern.pt/" rel="nofollow" title="http://www.oern.pt/">Ordem dos Engenheiros da Regi&atilde;o Norte</a>.</p>
