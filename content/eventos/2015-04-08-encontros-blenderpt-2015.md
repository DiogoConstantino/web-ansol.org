---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 306
  event:
    location: 
    site:
      title: ''
      url: http://problender.pt/conf2015/
    date:
      start: 2015-04-17 00:00:00.000000000 +01:00
      finish: 2015-04-17 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Encontros BlenderPT 2015
created: 1428523206
date: 2015-04-08
aliases:
- "/evento/306/"
- "/node/306/"
---
<p><span lang="PT-BR">Encontros Blender PT 2015 é um evento que reúne investigadores, profissionais e freelancers no sentido de promover a ferramenta digital livre Blender, debater acerca do seu real impacto a nível académico e profissional e estabelecer pontes entre as mais diversas comunidades de produção 3D no âmbito da partilha de conhecimento, composto por palestras, workshops, coffee breaks e muitas outras supresas.</span></p>
