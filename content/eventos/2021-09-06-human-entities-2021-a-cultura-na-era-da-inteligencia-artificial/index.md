---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 814
  event:
    location: Palácio Sinel de Cordes - Trienal de Arquitectura de Lisboa
    site:
      title: ''
      url: https://www.eventbrite.pt/e/registo-human-entities-2021-ines-cisneiros-163552101739
    date:
      start: 2021-09-15 00:00:00.000000000 +01:00
      finish: 2021-09-15 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Human Entities 2021: a cultura na era da inteligência artificial'
created: 1630925936
date: 2021-09-06
aliases:
- "/evento/814/"
- "/node/814/"
---
