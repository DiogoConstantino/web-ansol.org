---
categories:
- sound
- linux
- conference
- floss
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 289
  - tags_tid: 242
  - tags_tid: 290
  - tags_tid: 273
  node_id: 605
  event:
    location: C-Base, Berlim, Alemanha
    site:
      title: LAC2018
      url: http://lac.linuxaudio.org/2018/
    date:
      start: 2018-06-07 00:00:00.000000000 +01:00
      finish: 2018-06-10 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: LAC 2018 - Linux Audio Conference 2018
created: 1522958708
date: 2018-04-05
aliases:
- "/evento/605/"
- "/node/605/"
---
<div class="e-content entry-content"><div><div class="line-block"><div class="line">We are happy to announce, that the review process has started. As everything has been delayed in the past weeks, we have updated the deadlines accordingly:</div></div><ul class="simple"><li>Acceptance notification: <strong>April 14th, 2018</strong></li><li>Final deadline for 'camera ready' paper: <strong>April 28th, 2018</strong></li><li>Author registration deadline: <strong>April 29th, 2018</strong></li><li>Final program: <strong>May 1st, 2018</strong></li></ul><div class="line-block"><div class="line">We hope there will be no further delays and are looking forward to reviewing your submissions!</div></div></div></div><h1 class="p-name entry-title"><a href="http://lac.linuxaudio.org/2018/deadline-extended/" class="u-url">Deadline extended</a></h1><div class="metadata"><p class="byline author vcard"><span class="byline-name fn"> Linux Audio Conference Team </span></p><p class="dateline"><a href="http://lac.linuxaudio.org/2018/deadline-extended/" rel="bookmark">2018-02-18 15:18</a></p></div><div class="e-content entry-content"><div><div class="line-block"><div class="line">You might be happy to hear, that we have just extended the submission deadline by two weeks (<strong>March 15th, 2018</strong>)!</div><div class="line">Due to <a href="http://lac.linuxaudio.org/2018/services-are-back" target="_blank">unforseen circumstances</a> the past few weeks have been quite bumpy.</div><div class="line">All the more reason to give everyone a little more time!</div></div><div class="line-block"><div class="line">We have also updated the information on the submission process regarding the upload of PDFs. We hope this will clear any uncertainties you might have.</div></div><div class="line-block"><div class="line">If you have any questions, don't hesitate to <a href="http://lac.linuxaudio.org/2018/pages/contact" class="reference external">contact</a> us!</div></div></div></div><h1 class="p-name entry-title"><a href="http://lac.linuxaudio.org/2018/services-are-back/" class="u-url">Services are back!</a></h1><div class="metadata"><p class="byline author vcard"><span class="byline-name fn"> Linux Audio Conference Team </span></p><p class="dateline"><a href="http://lac.linuxaudio.org/2018/services-are-back/" rel="bookmark">2018-02-18 12:50</a></p></div><div class="e-content entry-content"><div><div class="line-block"><div class="line">As you might have noticed, on January 29th the linuxaudio.org server (hosting many more websites and the affiliated mailing lists) was compromised. You can read the <a href="https://lists.linuxaudio.org/archives/linux-audio-user/2018-February/109620.html" target="_blank">full recap</a> by Jeremy Jongepier, if you're interested.</div></div><div class="line-block"><div class="line">After moving all services to a new location and getting things from backups, everything should be operational by now! However, if you see any glitches with the openconf submission system, please <a href="mailto:lac@linuxaudio.org" target="_blank">contact us</a>!</div></div></div></div><h1 class="p-name entry-title"><a href="http://lac.linuxaudio.org/2018/call-for-papers-starts/" class="u-url">Call for Papers starts!</a></h1><div class="metadata"><p class="byline author vcard"><span class="byline-name fn"> Linux Audio Conference Team </span></p><p class="dateline"><a href="http://lac.linuxaudio.org/2018/call-for-papers-starts/" rel="bookmark">2018-01-07 19:26</a></p></div><div class="e-content entry-content"><div><div class="line-block"><div class="line">We are happy to announce, that the <a href="http://lac.linuxaudio.org/2018/pages/cfp" target="_blank">call for papers and works</a> has started!</div><div class="line">All relevant information on the submission process can now be found on this website.</div></div><div class="line-block"><div class="line">Additionally, we're very pleased to have another institution (<a href="http://spektrumberlin.de" target="_blank">Spektrum</a>) onboard in support of this year's conference.</div></div></div></div><h1 class="p-name entry-title"><a href="http://lac.linuxaudio.org/2018/lac-2018-is-happening-in-berlin/" class="u-url">LAC 2018 is happening in Berlin</a></h1><div class="metadata"><p class="byline author vcard"><span class="byline-name fn"> Linux Audio Conference Team </span></p><p class="dateline"><a href="http://lac.linuxaudio.org/2018/lac-2018-is-happening-in-berlin/" rel="bookmark">2017-12-18 20:43</a></p></div><div class="e-content entry-content"><div><p>Hey all, we have some good news!</p><p>Linux Audio Conference 2018 will be hosted at <a href="https://c-base.org" target="_blank">c-base</a> - in partnership with the <a href="https://www.ak.tu-berlin.de/menue/elektronisches_studio/" target="_blank">Electronic Studio at TU Berlin</a> - again in 2018 and we even have a date for it already!</p><p><strong>7th - 10th June 2018</strong></p><p>We will have a <em>Call for Papers</em> and a <em>Call for Submissions</em> in the beginning of next year.</p><p>More information will follow in the coming weeks.</p></div></div>
