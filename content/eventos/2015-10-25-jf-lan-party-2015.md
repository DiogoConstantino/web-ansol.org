---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 374
  event:
    location: Figueiró dos Vinhos, Portugal
    site:
      title: JF
      url: http://www.pcspeed.pt/
    date:
      start: 2015-10-30 00:00:00.000000000 +00:00
      finish: 2015-11-01 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: JF Lan Party 2015
created: 1445803835
date: 2015-10-25
aliases:
- "/evento/374/"
- "/node/374/"
---
<p>Lan Party organizada pela empresa JF - Soluções informáticas em Figueiró dos Vinhos.</p>
