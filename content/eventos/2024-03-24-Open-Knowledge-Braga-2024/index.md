---
layout: evento
title: Open Knowledge Braga 2024
metadata:
  event:
    location: Braga
    site:
      url: https://pt.wikimedia.org/wiki/Open_Knowledge_Braga_2024
    date:
      start: 2024-03-24 11:00:00.000000000 +00:00
      finish: 2024-03-24 20:00:00.000000000 +00:00
---

A primeira edição do evento "Open Knowledge Braga" decorrerá no próximo dia 24 de Março de 2024. Entre outras actividades, neste evento vai-se celebrar o "Dia da Liberdade Documental".
