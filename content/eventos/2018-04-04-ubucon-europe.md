---
categories:
- ubuntu
- comunidade
- software livre
- ubucon
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 162
  - tags_tid: 251
  - tags_tid: 41
  - tags_tid: 252
  node_id: 572
  event:
    location: " Antiguo Instituto, Xixón, Asturies, Spain"
    site:
      title: Ubucon 2018
      url: http://ubucon.org/en/events/ubucon-europe/
    date:
      start: 2018-04-27 00:00:00.000000000 +01:00
      finish: 2018-04-29 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Ubucon Europe
created: 1522854303
date: 2018-04-04
aliases:
- "/evento/572/"
- "/node/572/"
---
<p>Encontro europeu da comunidade Ubuntu. Haverá palestras, encontros sociais e uma espicha asturiana (comes e besbes com cidra fresca...) durante os 3 dias do encontro.</p><p>Pretende ser um encontro não só de negócios, técnico mas principalmente que envolva a comunidade dos utilizadores developers e voluntários.</p>
