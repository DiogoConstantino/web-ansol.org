---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 645
  event:
    location: 'Lisbon Congress Center '
    site:
      title: ''
      url: https://www.cloudbees.com/devops-world/lisbon
    date:
      start: 2019-12-02 00:00:00.000000000 +00:00
      finish: 2019-12-05 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Jenkins World 2019 Europe
created: 1549209609
date: 2019-02-03
aliases:
- "/evento/645/"
- "/node/645/"
---

