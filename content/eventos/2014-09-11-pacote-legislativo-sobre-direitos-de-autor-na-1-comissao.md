---
categories:
- "#pl118"
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 44
  node_id: 219
  event:
    location: Assembleia da República
    site:
      title: ''
      url: http://app.parlamento.pt/webutils/docs/doc.pdf?Path=6148523063446f764c324679626d56304c334e706447567a4c31684a5355786c5a793944543030764d554e425130524d52793942636e463161585a765132397461584e7a5957387654334a6b5a57357a4947526c4946527959574a68624768764c304e425130524d52313878587a49314e5335775a47593d&Fich=CACDLG_1_255.pdf&Inline=true
    date:
      start: 2014-09-17 10:00:00.000000000 +01:00
      finish: 2014-09-17 13:00:00.000000000 +01:00
    map: {}
layout: evento
title: Pacote Legislativo sobre Direitos de Autor na 1ª Comissão
created: 1410462670
date: 2014-09-11
aliases:
- "/evento/219/"
- "/node/219/"
---
<p>A "Comissão de Assuntos Constitucionais, Direitos, Liberdades e Garantias" irá, na sua reunião ordinária de dia 17 de manhã, efectuar à "apreciação e votação dos pareceres sobre as seguintes iniciativas legislativas":</p><blockquote><p>Proposta de Lei n.º 245/XII/3.ª (GOV) - Regula as entidades de gestão coletiva do direito de autor e dos direitos conexos, inclusive quanto ao estabelecimento em território nacional e à livre prestação de serviços das entidades previamente estabelecidas noutro Estado-Membro da União Europeia ou do Espaço Económico Europeu</p><p>Proposta de Lei n.º 246/XII/3.ª (GOV) - Procede à segunda alteração à Lei n.º 62/98, de 1 de setembro, que regula o disposto no artigo 82.º do Código do Direito de Autor e dos Direitos Conexos, sobre a compensação equitativa relativa à cópia privada</p><p>Proposta de Lei n.º 247/XII/3.ª (GOV) - Transpõe a Diretiva n.º 2012/28/UE, do Parlamento Europeu e do Conselho, de 25 de outubro, relativa a determinadas utilizações permitidas de obras órfãs, e procede à décima alteração ao Código do Direito de Autor e dos Direitos Conexos, aprovado pelo Decreto-Lei n.º 63/85, de 14 de março</p></blockquote><p>&nbsp;</p>
