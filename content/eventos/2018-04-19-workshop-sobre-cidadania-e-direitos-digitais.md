---
categories:
- cidadania
- direitos digitais
- workshop
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 303
  - tags_tid: 113
  - tags_tid: 149
  node_id: 613
  event:
    location: Palácio dos Marqueses da Praia e Monforte, Loures, Lisboa, Portugal
    site:
      title: " Workshop sobre Cidadania e Direitos Digitais"
      url: http://ensinolivre.pt/?p=879
    date:
      start: 2018-04-20 00:00:00.000000000 +01:00
      finish: 2018-04-21 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Workshop sobre Cidadania e Direitos Digitais
created: 1524158519
date: 2018-04-19
aliases:
- "/evento/613/"
- "/node/613/"
---
<p>O <a href="https://sites.google.com/a/milip.org/ctp2/home" target="_blank" rel="noopener">II Encontro Caminhos e Trajectos em Projectos</a>, organizado pelo CENFORES, começa amanhã com um aliciante programa de painéis e workshops sobre educação nas suas várias vertentes.</p><p>No Sábado, estarei, com o Eduardo da <a href="https://direitosdigitais.pt/" target="_blank" rel="noopener">Associação D3 – Defesa dos Direitos Digitais</a>, a dar um workshop sobre Cidadania e Direitos Digitais, que inclui:</p><ul><li>demonstrar como as alterações legislativas têm consequências directas no ensino e naquilo que os professores podem ou não fazer;</li><li>dar a conhecer ferramentas e recursos que os professores podem usar com segurança nas suas aulas (Domínio Público; Creative Commons; Acesso Aberto);</li><li>sensibilizar os professores para as propostas, que estão a ser discutidas no Parlamento Europeu, tais como:<ul><li>restrição da utilização digital de obras para fins de ensino a estabelecimentos de ensino ou redes privadas, deixando de fora bibliotecas e outros espaços de aprendizagem;</li><li>possibilidade dos países optarem por um sistema de licenças, obrigando os estabelecimentos de ensino a negociar e a pagar pela utilização de obras;</li><li>possibilidade dos países criarem uma taxa a ser paga pelos estabelecimentos de ensino aos representantes dos titulares dos direitos, à semelhança da taxa da cópia privada;</li></ul></li><li>sugerir acções que os professores podem fazer para melhorar as propostas que estão a ser discutidas;</li><li>apresentar brevemente associações que estão a seguir estes desenvolvimentos.</li></ul><p>A participação no evento é gratuita, mas é <a href="https://sites.google.com/a/milip.org/ctp2/inscricoes-e-submissoes" target="_blank" rel="noopener">necessária inscrição aqui</a>.</p><p>Local do Evento:<br> Palácio dos Marqueses da Praia e Monforte<br> <a href="https://www.google.com/maps/@38.8250738,-9.1620178,18z?hl=pt-PT" target="_blank" rel="noopener">Estrada Nacional 8, 2670-331 Loures</a></p>
