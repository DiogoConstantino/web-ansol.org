---
excerpt: "<p>Debate sobre a partilha de conte&uacute;dos na actualidade: a pirataria
  tem cada vez mais adeptos? os downloads ilegais comprometem a produtividade de um
  pa&iacute;s? Quem &eacute; prejudicado? Qu&atilde;o permissiva pode ou n&atilde;o
  ser a legisla&ccedil;&atilde;o, para contemplar produtividade, e quest&otilde;es
  &eacute;ticas associadas, como as da propriedade intelectual?</p>\r\n<p>Debate inclu&iacute;do
  no programa da XX Semana Inform&aacute;tica, que contar&aacute; com a presen&ccedil;a
  da ANSOL.</p>\r\n"
categories: []
metadata:
  node_id: 96
  event:
    location: Grande Anfiteatro do Centro de Congressos do Pavilhão de Civil, Campus
      Alameda do IST
    site:
      title: ''
      url: http://xx.sinfo.org/pt/painel-partilha-de-conteudos-e-pirataria
    date:
      start: 2013-02-25 16:30:00.000000000 +00:00
      finish: 2013-02-25 16:30:00.000000000 +00:00
    map: {}
layout: evento
title: Partilha de Conteúdos e Pirataria - XX SINFO
created: 1351293427
date: 2012-10-27
aliases:
- "/evento/96/"
- "/node/96/"
---
<p>Debate sobre a partilha de conte&uacute;dos na actualidade: a pirataria tem cada vez mais adeptos? os downloads ilegais comprometem a produtividade de um pa&iacute;s? Quem &eacute; prejudicado? Qu&atilde;o permissiva pode ou n&atilde;o ser a legisla&ccedil;&atilde;o, para contemplar produtividade, e quest&otilde;es &eacute;ticas associadas, como as da propriedade intelectual?</p>
<div>
	Moderador: Vasco Trigo</div>
<div>
	&nbsp;</div>
<div>
	Mesa:</div>
<ul>
	<li>
		Rick Falkvinge - Fundador do Partido Pirata Sueco</li>
	<li>
		Rui Seabra - Presidente da ANSOL</li>
	<li>
		Lu&iacute;s Rede de Sousa - Presidente da Direc&ccedil;&atilde;o da Assoft</li>
	<li>
		Miguel Afonso Caetano - Investigador em Ci&ecirc;ncias da Comunica&ccedil;&atilde;o e Novos Media</li>
	<li>
		Nuno Cardoso - Membro e Coordenador Internacional do Partido Pirata Portugu&ecirc;s, Membro da Direc&ccedil;&atilde;o do Pirate Parties International</li>
</ul>
