---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 235
  event:
    location: Universidade Católica Portuguesa
    site:
      title: ''
      url: https://docs.google.com/spreadsheet/viewform?fromEmail=true&formkey=dG9NZlJ3N0VHVFRrelBidnNGNngzTlE6MA
    date:
      start: 2014-10-23 17:00:00.000000000 +01:00
      finish: 2014-10-23 17:00:00.000000000 +01:00
    map: {}
layout: evento
title: Católica Talks - Fair Use in Europe? A View from the Other Side of the Atlantic
created: 1413221527
date: 2014-10-13
aliases:
- "/evento/235/"
- "/node/235/"
---
<p>O&nbsp;<em>Católica&nbsp;Research Centre for the Future of Law&nbsp;</em>(Lisboa) apresenta a segunda sessão do&nbsp;<em>Católica&nbsp;Talks 2014-2015,&nbsp;</em>a proferir por&nbsp;<strong>James Boyle&nbsp;</strong>e&nbsp;<strong>Jennifer Jenkins&nbsp;</strong>(Universidade de Duke | Professores no LL.M.&nbsp;<em>Law in a European and Global Context</em>), seguido de comentário a cargo de Tito Rendas.</p><p>&nbsp;O evento terá lugar no dia 23 de Outubro, às 17:00h, na Universidade&nbsp;Católica&nbsp;Portuguesa.</p><p>&nbsp;A&nbsp;<strong>inscrição é obrigatória&nbsp;</strong>através do preenchimento de&nbsp;<a href="https://docs.google.com/spreadsheet/viewform?fromEmail=true&amp;formkey=dG9NZlJ3N0VHVFRrelBidnNGNngzTlE6MA" target="_blank">formulário</a>.</p>
