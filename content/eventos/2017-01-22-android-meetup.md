---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 481
  event:
    location: Aptoide, Lisboa
    site:
      title: ''
      url: https://www.meetup.com/Android-LX/events/236987866/?rv=ea1&_af=event&_af_eid=236987866&https=on
    date:
      start: 2017-01-25 19:00:00.000000000 +00:00
      finish: 2017-01-25 19:00:00.000000000 +00:00
    map: {}
layout: evento
title: Android Meetup
created: 1485104196
date: 2017-01-22
aliases:
- "/evento/481/"
- "/node/481/"
---

