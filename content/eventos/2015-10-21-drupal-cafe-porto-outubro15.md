---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 354
  event:
    location: Porto - a combinar
    site:
      title: Drupal Café Porto - Outubro'15
      url: https://groups.drupal.org/node/487403
    date:
      start: 2015-10-28 20:00:00.000000000 +00:00
      finish: 2015-10-28 20:00:00.000000000 +00:00
    map: {}
layout: evento
title: Drupal Café Porto - Outubro'15
created: 1445453153
date: 2015-10-21
aliases:
- "/evento/354/"
- "/node/354/"
---
<p><span>Start: &nbsp;2015-10-28 19:00 - 20:30 Europe/Lisbon</span><br><span>Organizers:&nbsp;</span><br><span>rfmarcelino</span><br><br><span>Drupal 8 is coming! There are a lot of new features and improvements, but also with a lot of challenges for developers.</span><br><span>This event intends to share knowledge and the potential of Drupal as well as let drupalers from the North (of Portugal) to get to know each other .</span><br><br><span>Drupa 8 está a chegar! Há muitas novidades e melhorias mas também muitos desafios para os developers.</span><br><span>Este encontro destina-se a partilhar conhecimento e o potencial do Drupal, bem como dar a conhecer drupaleiros do Norte*.</span><br><br><span>*Não só do Norte. Há simultaneamente um evento em Lisboa, mas todos são bem vindos.</span></p>
