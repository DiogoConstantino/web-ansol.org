---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 465
  event:
    location: Aveiro
    site:
      title: ''
      url: https://enei.pt/pt/
    date:
      start: 2016-10-28 00:00:00.000000000 +01:00
      finish: 2016-10-31 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro Nacional de Estudantes de Informática
created: 1476043978
date: 2016-10-09
aliases:
- "/evento/465/"
- "/node/465/"
---

