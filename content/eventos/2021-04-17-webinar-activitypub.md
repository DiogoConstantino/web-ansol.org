---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 790
  event:
    location: Online
    site:
      title: ''
      url: https://socialhub.activitypub.rocks/pub/ec-ngi0-liaison-webinars-and-workshop-april-2021
    date:
      start: 2021-04-26 00:00:00.000000000 +01:00
      finish: 2021-04-26 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Webinar – ActivityPub
created: 1618678676
date: 2021-04-17
aliases:
- "/evento/790/"
- "/node/790/"
---

