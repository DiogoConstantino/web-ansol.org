---
categories: []
metadata:
  event:
    location: Online
    site:
      url: https://fundamentalrightsforum.eu/
    date:
      start: 2021-10-11 00:00:00.000000000 +01:00
      finish: 2021-10-12 00:00:00.000000000 +01:00
layout: evento
title: Fundamental Rights Forum 2021
created: 1621432976
date: 2021-05-19
aliases:
- "/evento/801/"
- "/node/801/"
---

