---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 520
  event:
    location: Porto
    site:
      title: ''
      url: https://makeorbreak.portosummerofcode.com/
    date:
      start: 2017-09-08 00:00:00.000000000 +01:00
      finish: 2017-09-10 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Make or Break - Porto Summer of Code
created: 1502816913
date: 2017-08-15
aliases:
- "/evento/520/"
- "/node/520/"
---

