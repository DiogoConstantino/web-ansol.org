---
layout: evento
title: Festa do Software Livre 2024
metadata:
  event:
    date:
      start: 2024-10-11
      finish: 2024-10-12
    location: Universidade de Aveiro
    site:
      url: https://festa2024.softwarelivre.eu
---

A Festa do Software Livre é um encontro nacional de várias comunidades
interessadas em Software Livre. Ao longo dos dois dias vamos contar com várias
apresentações, painéis e workshops.

Mais informações em breve!
