---
categories:
- "#ilovefs"
- ilovefs
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 62
  - tags_tid: 56
  node_id: 483
  event:
    location: Todo o Mundo
    site:
      title: ''
      url: https://fsfe.org/campaigns/ilovefs/
    date:
      start: 2017-02-14 00:00:00.000000000 +00:00
      finish: 2017-02-14 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Eu <3 Software Livre 2017
created: 1487070196
date: 2017-02-14
aliases:
- "/evento/483/"
- "/ilovefs2017/"
- "/node/483/"
---
<img src="https://ansol.org/attachments/ilovefs-heart-px.png" alt="#ILoveFS" title="#ILoveFS">

Todos os anos a 14 de Fevereiro, a ANSOL junta-se à Free Software Foundation
Europe e pede a todos os utilizadores de Software Livre que pensem naquelas
pessoas trabalhadoras na comunidade de Software Livre e mostrem o seu apreço
individualmente neste dia de "Eu adoro o Software Livre".

Tal como no ano passado, a campanha é dedicada às pessoas por detrás do
Software Livre porque eles permitem-nos usar, estudar, partilhar e melhorar o
software que nos permite trabalhar em liberdade.

Este ano, a ANSOL celebra o dia de forma ligeiramente aos anteriores: quando
mostrarem (durante o dia de hoje) o vosso apreço pelo software livre (no geral,
software em particular, ou mesmo uma pessoa ou equipa que contribui para o
Software Livre), <a href="mailto:marcos.marado@ansol.org">enviem-nos um
mail</a> com informação do que fizeram (uma foto, um link, o que for mais
conveniente). A declaração de amor pelo software livre mais criativa será
premiada com uma anuidade nas quotas de sócio da ANSOL\* para o seu
criador.</p><p>Mais sobre a campanha (e ideias sobre como festejar o dia) <a
href="https://fsfe.org/campaigns/ilovefs/">no site oficial</a>.

<hr>

*\* Esta campanha está aberta a todos. Se o vencedor não for sócio terá a
hipótese de se associar gratuitamente*
