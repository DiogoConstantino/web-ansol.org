---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 707
  event:
    location: Porto
    site:
      title: ''
      url: https://datewithdata.pt/
    date:
      start: 2019-11-16 00:00:00.000000000 +00:00
      finish: 2019-11-16 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'Date With Data #41'
created: 1573415113
date: 2019-11-10
aliases:
- "/evento/707/"
- "/node/707/"
---

