---
layout: evento
title: EU Open Source Policy Summit 2024
metadata:
  event:
    location: Bruxelas
    site:
      url: https://openforumeurope.org/event/the-eu-open-source-policy-summit-2024/
    date:
      start: 2024-02-02 09:00:00.000000000 +01:00
      finish: 2024-02-02 17:00:00.000000000 +01:00
---

![](cartaz.png)

Este será o 10º ano em que esta cimeira Europeia se realiza.
