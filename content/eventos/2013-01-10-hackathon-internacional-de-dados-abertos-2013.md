---
categories: []
metadata:
  node_id: 121
  event:
    location: Porto
    site:
      title: ''
      url: http://opendataday.org/index_pt.html
    date:
      start: 2013-02-23 00:00:00.000000000 +00:00
      finish: 2013-02-23 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Hackathon Internacional de Dados Abertos 2013
created: 1357820630
date: 2013-01-10
aliases:
- "/evento/121/"
- "/node/121/"
---
Este evento é um ajuntamento de cidadãos em cidades de todo o mundo para escrever aplicações, libertar dados, criar visualizações e publicar análises usando dados abertos públicos para dar apoio e encorajar a adoção de políticas de libertação de dados por instituições, governos locais, regionais e nacionais em todo o mundo.
