---
layout: evento
title: Debian no Centro Linux
metadata:
  event:
    date:
      start: 2023-10-07 11:30:00.000000000 +01:00
      finish: 2023-10-07 19:00:00.000000000 +01:00
    location: Centro Linux
    site:
      url: https://centrolinux.pt/post/2023-outubro-debian/
---

[![Cartaz](cartaz.png)](https://centrolinux.pt/post/2023-outubro-debian/)

O Debian é uma das distribuções de GNU/Linux mais antigas, uma das mais
populares e certamente uma das mais influentes social e tecnicamente, sendo
também reconheco pela estabilidade.

A dimensão dos seus repositórios, a sua disponibilidade e influência em outras
distribuiçẽos e sistemas operativos faz com que seja justamente apelidado como
o sistema operativo Universal e com que seja essêncial que os fãs e estudiosos
de outros sistemas operativos também estudem o Debian.

Agora com ajuda de membros da comunidade Debian, também vocês podem aprender
mais sobre Debian no Centro Linux.

## Quando?

No Sábado dia 7 de Outubro de 2023 a partir das 11:30. Haverá interrupção das
actividades para almoço.

## Local:

No [Centro Linux/Makers In Little Lisbon](https://centrolinux.pt/ondeestamos/).
