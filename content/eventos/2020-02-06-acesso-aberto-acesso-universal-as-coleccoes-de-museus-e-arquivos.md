---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 732
  event:
    location: Porto, Casa Allen (Casa das Artes)
    site:
      title: ''
      url: https://acessocultura.org/acesso-aberto/
    date:
      start: 2020-02-19 00:00:00.000000000 +00:00
      finish: 2020-02-19 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'Acesso aberto: acesso universal às colecções de museus e arquivos'
created: 1581022864
date: 2020-02-06
aliases:
- "/evento/732/"
- "/node/732/"
---

