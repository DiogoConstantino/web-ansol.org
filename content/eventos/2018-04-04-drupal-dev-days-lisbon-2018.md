---
categories:
- drupal
- floss
- developers
- meeting
- portugal
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 64
  - tags_tid: 273
  - tags_tid: 145
  - tags_tid: 129
  - tags_tid: 200
  node_id: 593
  event:
    location: ISCTE, Lisboa, Portugal
    site:
      title: Drupal Dev Days
      url: https://lisbon2018.drupaldays.org/
    date:
      start: 2018-07-02 00:00:00.000000000 +01:00
      finish: 2018-07-06 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Drupal Dev Days - Lisbon 2018
created: 1522882137
date: 2018-04-04
aliases:
- "/evento/593/"
- "/node/593/"
---
<div class="page-full__header__wrapper"><div class="page-full__header"><div class="field field--name-node-title field--type-ds field--label-hidden basic-page-full__title field__item"><h1>The event</h1></div></div></div><div class="page-full__content-outer-wrapper"><div class="page-full__content-wrapper"><div class="page-full__content"><div class="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden basic-page-full__body field__item"><h3>MONDAY 2ND JULY - FRIDAY 6TH JULY ON ISCTE</h3><p>Join us for a week of Drupal with a Portuguese touch</p><ul><li>Monday 2nd July - Community reception in the evening.</li><li>Tuesday 3rd July - Registration, Sessions, Sprints</li><li>Wednesday 4th July - Sessions, Sprints</li><li>Thursday 5th July - Sessions, Sprints</li><li>Friday 6th July - Sessions, Sprints and Closure</li></ul><h3>SPRINTS, SESSIONS, WORKSHOPS, SOCIAL ACTIVITIES.</h3><p>Drupal Developer Days is an event organized by the Drupal community which gathers people from all over the world who contribute to the progress of Drupal, taking place in Europe every year.&nbsp;</p><p>DDD Lisbon 2018 will be the meeting place of students, amateurs and professionals interested in learning about the latest updates, exchanging information with leading international experts and strengthening the network within the community. Event highlights include sessions, workshops and code sprints, where participants come together to develop, test, document, fix and design to improve the Drupal platform.</p><h3>IMPORTANT DATES</h3><ul><li>February 9 - Start ticket sales</li><li>March 5 - Call for Session proposals submission</li><li>April 30 - Deadline for Session proposals submission</li><li>May 14 - Sessions and schedule announced</li><li>July 1 - End ticket registration</li><li>July 2/6 - Drupal Dev Days Lisbon</li></ul></div></div></div></div>
