---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 793
  event:
    location: Online
    site:
      title: ''
      url: https://pt.wikimedia.org/wiki/WikidataDays_Sessions_II
    date:
      start: 2021-04-25 16:00:00.000000000 +01:00
      finish: 2021-04-25 16:00:00.000000000 +01:00
    map: {}
layout: evento
title: WikidataDays Sessions II
created: 1619015716
date: 2021-04-21
aliases:
- "/evento/793/"
- "/node/793/"
---
<p>A segunda edição dos WikidataDays Sessions será completamente online e dedicada à democracia e partidos políticos em Portugal. Tendo como base de trabalho uma listagem de todos os partidos políticos fundados e extintos em Portugal, iremos atualizar os dados no Wikidata e na Wikipédia, corrigir a informação que necessite ser corrigida assim como analisar as fontes de informação que podem ser usadas.</p>
