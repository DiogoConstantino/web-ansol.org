---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 174
  event:
    location: Sede Nacional do Bloco de Esquerda, Lisboa
    site:
      title: ''
      url: http://www.esquerda.net/events1
    date:
      start: 2013-06-06 21:30:00.000000000 +01:00
      finish: 2013-06-06 21:30:00.000000000 +01:00
    map: {}
layout: evento
title: Copiar é ser pirata? (Lisboa)
created: 1370381925
date: 2013-06-04
aliases:
- "/evento/174/"
- "/node/174/"
---
<p>Debate: “Copiar é ser pirata? DRM's e a privatização da Cultura”<br>Com Tiago Ivo Cruz, Pedro Filipe Soares e Rui Seabra (ANSOL). Ver&nbsp;<a href="http://www.esquerda.net/sites/default/files/files/DRM's-cutura-lx.jpg" target="_blank">cartaz</a>.</p><div align="center"><img src="http://www.esquerda.net/sites/default/files/files/DRM's-cutura-lx.jpg" alt="cartaz" width="50%"></div><p><br> <strong>Lisboa</strong>, Sede Nacional do Bloco – Rua da Palma 268,&nbsp;<strong>21h30</strong>.</p><p>Mais informação sobre a posição da ANSOL sobre este assunto <a href="https://drm-pt.info/2013/04/27/be-quer-resolver-problema-do-drm-com-projecto-de-lei/">aqui</a>.</p>
