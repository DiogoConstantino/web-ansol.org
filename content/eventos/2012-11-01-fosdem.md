---
categories: []
metadata:
  node_id: 100
  event:
    location: ULB campus Solbosh, Bruxelas, Bélgica
    site:
      title: FOSDEM
      url: http://fosdem.org/
    date:
      start: 2013-02-02 00:00:00.000000000 +00:00
      finish: 2013-02-03 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: FOSDEM
created: 1351800256
date: 2012-11-01
aliases:
- "/evento/100/"
- "/node/100/"
---
<div id="slogan">
	A FOSDEM &eacute; um evento gratuito e sem necessidade de pr&eacute;-registo de participa&ccedil;&atilde;o que oferece &agrave;s comunidades de software livre um ponto de encontro para partilhar ideias e colaborar. &Eacute; famoso por ser muito orientado aos que colaboram no desenvolvimento de software livre e reune mais de 5 mil geeks do mundo inteiro.</div>
