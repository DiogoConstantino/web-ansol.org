---
layout: evento
title: Wordcamp Lisboa 2023
metadata:
  event:
    date:
      start: 2023-05-19
      finish: 2023-05-20
    location: ISCTE-IUL
    site:
      url: https://lisboa.wordcamp.org/2023/
---

> 2 dias de talks, workshops, contribuição, networking e uma espectacular afterparty.
> Tudo sobre WordPress, WooCommerce e não só.
>
> Junta-te à comunidade lisboeta no maior evento presencial desde o
> confinamento. Assiste às palestras sobre Marketing, SEO, comércio
> electrónico, tecnologia, conteúdo e tudo o que está relacionado com o
> WordPress e a web. Participa nos workshops, contribui para este projecto open
> source e faz networking com a TUA comunidade.
