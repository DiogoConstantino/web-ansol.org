---
categories:
- ilovefs
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 56
  node_id: 272
  event:
    location: 
    site:
      title: ''
      url: http://fsfe.org/campaigns/ilovefs/2015/ilovefs.pt.html
    date:
      start: 2015-02-14 00:00:00.000000000 +00:00
      finish: 2015-02-14 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Eu <3 Software Livre 2015
created: 1422372091
date: 2015-01-27
aliases:
- "/evento/272/"
- "/ilovefs2015/"
- "/node/272/"
---
<img src="https://ansol.org/attachments/ilovefs-heart-px.png" alt="#ilovefs">

Todos os anos a 14 de Fevereiro, a ANSOL junta-se à Free Software Foundation
Europe e pede a todos os utilizadores de Software Livre que pensem naquelas
pessoas trabalhadoras na comunidade de Software Livre e mostrem o seu apreço
individualmente neste dia de "Eu adoro o Software Livre".

Tal como no ano passado, a campanha é dedicada às pessoas por detrás do
Software Livre porque eles permitem-nos usar, estudar, partilhar e melhorar o
software que nos permite trabalhar em liberdade. Desta vez o foco vai
especialmente para as pequenas contribuições para o grande conceito da
comunidade de Software Livre.
