---
categories:
- software livre
- brazil
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 41
  - tags_tid: 236
  node_id: 563
  event:
    location: Centro de Eventos PUCRS, Porto Alegre, RS, Brasil
    site:
      title: Fórum Internacional do Software Livre em Porto Alegre
      url: http://fisl.softwarelivre.org/
    date:
      start: 2018-07-11 00:00:00.000000000 +01:00
      finish: 2018-07-14 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: FISL18 - Fórum Internacional do Software Livre
created: 1522843965
date: 2018-04-04
aliases:
- "/evento/563/"
- "/node/563/"
---
<p><strong>Fórum Internacional do Software Livre</strong> (<strong>FISL</strong>) (<strong>International Free Software Forum</strong>) is an event sponsored by <a href="https://en.wikipedia.org/w/index.php?title=Associa%C3%A7%C3%A3o_Software_Livre.Org&amp;action=edit&amp;redlink=1" title="Associação Software Livre.Org (page does not exist)" class="new">Associação Software Livre.Org</a> (Free Software Association), a Brazilian <a href="https://en.wikipedia.org/wiki/NGO" title="NGO" class="mw-redirect">NGO</a> that, among other goals, seeks the promotion and adoption of <a href="https://en.wikipedia.org/wiki/Free_software" title="Free software">free software</a>. It takes place every year in <a href="https://en.wikipedia.org/wiki/Porto_Alegre" title="Porto Alegre">Porto Alegre</a>, the capital of <a href="https://en.wikipedia.org/wiki/Rio_Grande_Do_Sul" title="Rio Grande Do Sul" class="mw-redirect">Rio Grande Do Sul</a>, the southernmost state of <a href="https://en.wikipedia.org/wiki/Brazil" title="Brazil">Brazil</a>.</p><p>The event is meant as a "get-together" of <a href="https://en.wikipedia.org/wiki/Student" title="Student">students</a>, <a href="https://en.wikipedia.org/wiki/Researcher" title="Researcher" class="mw-redirect">researchers</a>, <a href="https://en.wikipedia.org/wiki/Social_movements" title="Social movements" class="mw-redirect">social movements</a> for <a href="https://en.wikipedia.org/wiki/Freedom_of_information" title="Freedom of information">freedom of information</a>, <a href="https://en.wikipedia.org/wiki/Entrepreneur" title="Entrepreneur" class="mw-redirect">entrepreneurs</a>, <a href="https://en.wikipedia.org/wiki/Information_Technology" title="Information Technology" class="mw-redirect">Information Technology</a> (IT) enterprises, governments, and other interested people. It is considered one of the world´s largest free software events, harboring technical, political and social debates in an integrated way. It gathers discussions, speeches, personalities and novelties both national and international in the free software world.</p><p>(citado da wikipedia)</p>
