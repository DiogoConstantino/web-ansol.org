---
categories:
- '2004'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 23
  node_id: 188
  event:
    location: Universidade do Minho, Pólo de Azurém
    site:
      title: ''
      url: http://www.jornadas-ig.com/
    date:
      start: 2004-04-21 00:00:00.000000000 +01:00
      finish: 2004-04-21 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: XI Jornadas de Informática de Gestão da Universidade do Minho
created: 1371949151
date: 2013-06-23
aliases:
- "/evento/188/"
- "/node/188/"
---
<p>Jaime Villate (ANSOL) vai fazer uma apresentação.</p>
