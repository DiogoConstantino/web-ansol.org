---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 762
  event:
    location: 
    site:
      title: ''
      url: https://drupal.pt/drupal-day-pt-2020
    date:
      start: 2020-11-13 00:00:00.000000000 +00:00
      finish: 2020-11-13 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Drupal Day PT '20
created: 1604167820
date: 2020-10-31
aliases:
- "/evento/762/"
- "/node/762/"
---

