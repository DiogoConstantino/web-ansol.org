---
layout: evento
title: Digital Rights Drinks - janeiro
metadata:
  event:
    location: Apple House, Avenida Elias Garcia 19B, Lisboa
    date:
      start: 2024-01-05 18:30:00
      finish: 2024-01-05 20:00:00
---

![](cartaz.png)

> Digital Rights Drinks: Monthly Meet Up on Human Rights in the Digital Age
>
> subjects: privacy, copyleft, net neutrality, law & tech, open data, open... everything, internet freedom, etc
>
> When: January 5th 6:30PM (Every 1st friday of the month)
>
> Where: Apple House (Avenida Elias Garcia 19B, Lisboa)
