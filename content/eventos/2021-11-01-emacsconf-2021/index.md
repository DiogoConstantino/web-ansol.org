---
metadata:
  event:
    location: Online
    site:
      url: https://emacsconf.org/2021/
    date:
      start: 2021-11-27
      finish: 2021-11-28
layout: evento
title: EmacsConf 2021
---
