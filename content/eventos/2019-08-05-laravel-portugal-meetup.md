---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 686
  event:
    location: Porto
    site:
      title: ''
      url: https://github.com/laravel-portugal/meetup
    date:
      start: 2019-09-07 00:00:00.000000000 +01:00
      finish: 2019-09-07 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Laravel Portugal Meetup
created: 1565000394
date: 2019-08-05
aliases:
- "/evento/686/"
- "/node/686/"
---

