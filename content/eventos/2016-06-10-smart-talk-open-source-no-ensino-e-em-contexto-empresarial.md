---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 429
  event:
    location: 
    site:
      title: ''
      url: https://www.eventbrite.pt/e/bilhetes-smart-talkopensource-no-ensino-e-em-contexto-empresarial-25993297653
    date:
      start: 2016-06-23 18:30:00.000000000 +01:00
      finish: 2016-06-23 18:30:00.000000000 +01:00
    map: {}
layout: evento
title: 'Smart Talk: Open Source no ensino e em contexto empresarial'
created: 1465594720
date: 2016-06-10
aliases:
- "/evento/429/"
- "/node/429/"
---

