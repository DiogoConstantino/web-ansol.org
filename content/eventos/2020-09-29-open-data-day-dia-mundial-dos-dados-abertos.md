---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 750
  event:
    location: 
    site:
      title: Open Data Day
      url: https://opendataday.org/
    date:
      start: 2021-03-06 00:00:00.000000000 +00:00
      finish: 2021-03-06 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Open Data Day / Dia Mundial dos Dados Abertos
created: 1601391079
date: 2020-09-29
aliases:
- "/evento/750/"
- "/node/750/"
---

