---
layout: evento
title: OWASP 2024 Global AppSec
metadata:
  event:
    location: Lisbon Congress Centre, Lisboa
    date:
      start: 2024-06-24
      finish: 2024-06-28
---

![](cartaz.png)
