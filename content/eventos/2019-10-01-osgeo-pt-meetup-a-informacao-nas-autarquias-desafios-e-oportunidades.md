---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 698
  event:
    location: Albergaria-a-Velha (Biblioteca Municipal)
    site:
      title: ''
      url: http://osgeopt.pt/meetup2019/
    date:
      start: 2019-10-02 00:00:00.000000000 +01:00
      finish: 2019-10-02 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'OSGeo-PT Meetup: A INFORMAÇÃO nas Autarquias - Desafios e Oportunidades'
created: 1569947415
date: 2019-10-01
aliases:
- "/evento/698/"
- "/node/698/"
---

