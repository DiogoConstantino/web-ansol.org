---
layout: evento
title: Evento de Fevereiro do Centro Linux
showcover: false
metadata:
  event:
    date:
      start: 2024-02-17 11:30:00.000000000 +01:00
      finish: 2024-02-17 19:00:00.000000000 +01:00
    location: Centro Linux
    site:
      url: https://fev2024.centrolinux.pt
---

[![Cartaz](cartaz.png)](https://fev2024c.centrolinux.pt)

Neste mês de Fevereiro o evento do Centro Linux será dedicado às seguintes actividades:

Primeira Sessão do Grupo de Estudo do Centro Linux (curso de introdução ao gitlab com git).
Introdução ao Home Assistant.
Investigação de armazenamento em rede.

O git é uma ferramenta colaborativa essencial para quem contribui para o Software Livre e Open Source Software, ou o quer utilizar de uma forma mais avançada, e o GitLab é uma plataforma online colaborativa baseada em git. O Grupo de Estudo do Centro Linux, vai debruçar-se sobre este tema durante os próximos dois meses.
Venham adquirir um conjunto de competências muito úteis!

O Home Assistant é uma plataforma de automação desenhada para a Internet das Coisas. É Software Livre e Open Source Software, e desenhada para podermos utilizar a Internet das coisas de forma mais independente, aberta, privada, e segura.
Se querem introduzir-se a esta plataforma, esta é uma boa oportunidade!

Por vezes precisamos de armazenar e partilhar dados entre diversos computadores, o armazenamento em rede, é a forma mais simples de resolver esse problema. Juntem-se a nós enquanto exploramos várias opções!

## Quando?

Sábado dia 17 de Fevereiro de 2024

## Local:

No [Centro Linux/Makers In Little Lisbon](https://centrolinux.pt/ondeestamos/).

## Detalhes

A inscrição é gratuita mas obrigatória (para garantir o cumprimento de limites de segurança do espaço),

Todos os detalhes e link para inscrição estão ná página da [Mantic Party](https://fev2024.centrolinux.pt) no site do [Centro Linux](https://centrolinux.pt)
