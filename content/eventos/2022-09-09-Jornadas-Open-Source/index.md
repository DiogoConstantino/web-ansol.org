---
layout: evento
title: "IV Jornadas Open Source"
metadata:
  event:
    date:
      start: 2022-09-09 09:30:00
      finish: 2022-09-10 18:00:00
    location: Instituto Politécnico de Bragança
    site:
      url: https://eventos.bad.pt/event/iv-jornadas-de-open-source/
---

As IV Jornadas de Open Source decorrerão a 9 e 10 de setembro de 2022, no Instituto Politécnico de Bragança (IPB). Trata-se de uma organização conjunta entre as Delegações Norte e Centro da BAD e o IPB.

O crescente movimento internacional no desenvolvimento e promoção de sistemas em Open Source, a par de um aprimoramento de todo um contexto legal, leva-nos a constatar que, hoje em dia, o Open Source é, em larga escala, assumido como padrão no mercado para desenvolvimento de software.
A adoção por esta forma de criação de software tem trazido muitos frutos, não só para quem concebe e cria, mas também para a comunidade que, continuamente e de forma ativa, contribui para o desenvolvimento dos produtos adequados às suas necessidades e contextos.

As IV Jornadas de Open Source pretendem trazer à discussão as mais recentes tendências, a uma escala internacional, no que toca à adoção do Open Source na rotina diária das instituições em geral e nos Serviços de Informação em particular. Simultaneamente, pretendem identificar quais as preocupações atuais relativamente à sua sustentabilidade e confiabilidade, perspetivando direções para o futuro. Pretende-se igualmente criar momentos de partilha de experiências e boas práticas, no que toca à implementação de sistemas de informação em Open Source em Bibliotecas, Arquivos e Museus.

# Datas importantes:

 * 20 de Abril a 17 de Junho – Chamada de propostas para Apresentações e Workshops
 * 16 de Maio a 2 de setembro – Inscrições
 * 27 de Junho – Comunicação das propostas selecionadas
 * 4 Julho – Programa provisório
