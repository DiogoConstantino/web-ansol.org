---
layout: evento
title: Workshop de introdução a Flutter
metadata:
  event:
    date:
      start: 2023-05-27 14:30:00.000000000 +01:00
      finish: 2023-05-27 18:00:00.000000000 +01:00
    location: Pink Room HQ, Coimbra
    site:
      url: https://gdg.community.dev/events/details/google-gdg-coimbra-presents-workshop-de-introducao-a-flutter
---

Workshop gratuito de introdução a Flutter.
