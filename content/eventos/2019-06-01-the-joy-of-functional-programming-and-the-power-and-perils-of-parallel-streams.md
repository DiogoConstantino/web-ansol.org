---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 678
  event:
    location: 
    site:
      title: ''
      url: https://www.meetup.com/Lisbon-JUG/events/261618398/
    date:
      start: 2019-06-05 00:00:00.000000000 +01:00
      finish: 2019-06-05 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: The Joy of Functional Programming & The Power and Perils of Parallel Streams
created: 1559400220
date: 2019-06-01
aliases:
- "/evento/678/"
- "/node/678/"
---

