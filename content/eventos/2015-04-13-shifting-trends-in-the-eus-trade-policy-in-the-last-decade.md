---
categories:
- ttip
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 307
  event:
    location: ISCTE
    site:
      title: ''
      url: http://www.pdpp.cies.iscte-iul.pt/np4/np4/?newsId=83&fileName=16abr2015_AT_pp.pdf
    date:
      start: 2015-04-16 18:00:00.000000000 +01:00
      finish: 2015-04-16 18:00:00.000000000 +01:00
    map: {}
layout: evento
title: Shifting Trends in the EU's Trade Policy in the Last Decade
created: 1428958821
date: 2015-04-13
aliases:
- "/evento/307/"
- "/node/307/"
---
<p><strong><strong>16 de abril de 2015</strong>, 18h00, Auditório B203, Ed. II, ISCTE-IUL<br></strong>Ciclo Internacional de Conferências Doutorais<a href="http://www.pdpp.cies.iscte-iul.pt/np4/np4/?newsId=83&amp;fileName=16abr2015_AT_pp.pdf"><br></a></p><p>Um evento sobre os Tratados de Comércio como o <a href="https://c.ansol.org/node/37">TTIP</a>.</p>
