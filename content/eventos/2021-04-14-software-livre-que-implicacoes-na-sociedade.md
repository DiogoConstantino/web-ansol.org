---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 785
  event:
    location: Online
    site:
      title: ''
      url: https://sites.google.com/anpri.pt/mind-bytes-week/dia-15?authuser=0#h.rkbf9sxge74d
    date:
      start: 2021-04-15 11:30:00.000000000 +01:00
      finish: 2021-04-15 11:30:00.000000000 +01:00
    map: {}
layout: evento
title: Software livre! que implicações na sociedade
created: 1618409722
date: 2021-04-14
aliases:
- "/evento/785/"
- "/node/785/"
---
Inserido no programa da "<a
href="https://sites.google.com/anpri.pt/mind-bytes-week/página-inicial?authuser=0">Mind
&amp; Bytes Week</a>", Tiago Carrondo, presidente da ANSOL - Associação
Nacional para o Software Livre, fez uma palestra entitulada "Software livre!
que implicações na sociedade", cujo vídeo agora se encontra disponível.

<img src="https://ansol.org/attachments/ANSOL-ANPRI.png" alt="Imagem do evento">
