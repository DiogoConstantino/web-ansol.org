---
layout: evento
title: Inércia 2023
metadata:
  event:
    location: Salão de Festas Incrivel Almadense
    site:
      url: https://2023.inercia.pt
    date:
      start: 2023-12-01 13:37:00
      finish: 2023-12-03 13:37:00
---

Organizada por pessoas envolvidas na demoscene há mais de 20 anos, a ediçao 2023 da Inércia Demoparty será nos dias 1 a 3 de Dezembro, e está de regresso a Almada, desta vez no legendário Salão de Festas Incrivel Almadense.

