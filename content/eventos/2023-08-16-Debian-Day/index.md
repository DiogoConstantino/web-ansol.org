---
layout: evento
title: Debian Day - Jantar do 30º Aniversário do Debian
metadata:
  event:
    location: Lamego, Portugal
    site:
      url: https://wiki.debian.org/DebianDay/2023/Portugal/Lamego
    date:
      start: 2023-08-16 20:30:00.000000000 +01:00
      finish: 2023-08-16 23:00:00.000000000 +01:00
---

Este ano o aniversário do Debian é especial: o projecto faz 30 anos!

Para celebrar, estamos a organizar um evento social: vamo-nos jantar, comer, beber, conversar e festejar!

### Local
O jantar irá decorrer num restaurante a definir, na cidade de Lamego. O restaurante será decidido mediante o número de inscritos, e as restrições e preferências dietárias de quem for. Para isso, é necessário que confirmes a tua presença, enviando até ao dia 15 um email para marcos.marado(at)ansol.org .

Lamego é uma cidade no distrito de Viseu (Norte de Portugal Continental). Há autocarros de e para as maiores cidades. A cidade é servida de auto-estradas, e de carro fica a cerca de 1h20m do Porto, 3h30m de Lisboa, ou 45m de Viseu.
