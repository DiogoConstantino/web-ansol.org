---
layout: evento
title: Encontro Ubuntu-pt Setembro @ Sintra
metadata:
  event:
    date:
      start: 2023-09-28 20:00:00.000000000 +01:00
      finish: 2023-09-28 23:00:00.000000000 +01:00
    location: Bar Saloon
    site:
      url: https://discourse.ubuntu.com/t/encontro-ubuntu-pt-de-setembro-sintra
---

[![Cartaz](cartaz.png)](https://discourse.ubuntu.com/t/encontro-ubuntu-pt-de-setembro-sintra)

Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal encontra-se no Saloon, em Sintra.

Vem, traz um amigo ou um familiar e vem conviver com a comunidade portuguesa…

Já experimentaste a versão beta do Ubuntu 23.10?
Vem partilha as tuas experiências e aspirações!

## Local:

## Morada:
Saloon
Avenida Movimento das Forças Armadas n 5, 2710-433 Sintra

(2 min a pé da estação de comboios da portela de Sintra)


## Mapa:
<iframe width="100%" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=-9.38313961029053%2C38.79927673038549%2C-9.37713146209717%2C38.80176631581731&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/38.80052/-9.38014">Ver mapa maior</a></small>
