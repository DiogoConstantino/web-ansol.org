---
layout: evento
title: Drupal Ibéria 2024
metadata:
  event:
    location: PACT - Parque do Alentejo de Ciência e Tecnologia, Évora
    site:
      url: https://2024.drupaliberia.eu/
    date:
      start: 2024-05-10
      finish: 2024-05-11
---

O primeiro evento Drupal ibérico.
