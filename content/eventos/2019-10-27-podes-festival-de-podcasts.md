---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 703
  event:
    location: Lisboa
    site:
      title: ''
      url: https://podes.pt/
    date:
      start: 2019-11-09 00:00:00.000000000 +00:00
      finish: 2019-11-09 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'PODES: Festival de Podcasts'
created: 1572193510
date: 2019-10-27
aliases:
- "/evento/703/"
- "/node/703/"
---

