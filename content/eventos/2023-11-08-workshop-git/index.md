---
layout: evento
title: Workshop de Git
metadata:
  event:
    location: Sala 4.1.19, no DETI, Universidade de Aveiro
    site:
      url: https://glua.ua.pt/workshop-git-2023/ 
    date:
      start: 2023-11-08 18:00:00
      finish: 2023-11-08 20:00:00
---

![Cartaz](cartaz.png)

# Workshop de Iniciação ao Git e GitHub

Já ouviste falar de Git ou sabes o que é mas não entendes como trabalhar com ele?

Na quarta-feira, dia 8 de novembro, o GLUA, em colaboração com a AETTUA, traz-te um Workshop de Git!
Com o [André Clérigo](https://www.linkedin.com/in/andreclerigo/) como orador, vamos dar-te as bases para explorares o Git e o GitHub.
Vamos dar-te a oportunidade de conhecer e aprender a trabalhar com Git, a ferramenta mais usada para controlo de versões, e com o GitHub que é a maior plataforma de hospedagem de código e ficheiros com controlo de versões.

O Workshop é direcionado para todos os que necessitem desta ferramenta muito importante.

E como não podia faltar, a AETTUA vai-te proporcionar um coffee break! ☕

## Detalhes do evento
👨<200d>💻 [André Clérigo](https://www.linkedin.com/in/andreclerigo/)

📆 Quarta-feira, 8 de Novembro de 2023

🕖 18:00

📍 DETI na sala 4.1.19

📝 [Inscrições](https://glua.ua.pt/workshop-git-23)

**Tópicos do Workshop:**
- Básicos de Git
- Branching
- GitHub

## Requisitos:
- Computador pessoal
- Git instalado
- Conta GitHub criada ([link](https://github.com/))
- Vontade de aprender

