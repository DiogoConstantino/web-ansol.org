---
layout: evento
title: OWASP Lisboa Chapter Meetup 2
metadata:
  event:
    location: INESC-ID R. Alves Redol 9, Lisboa
    site:
      url: https://www.meetup.com/owasp-lisboa-chapter/events/291153982/
    date:
      start: 2023-02-28
      finish: 2023-02-28
---

# Fundação OWASP®
A Fundação OWASP é uma organização sem fins lucrativos que trabalha para melhorar a segurança do software. Através de vários projetos de software livre liderados pela comunidade, centenas de delegações locais no mundo, milhares de membros e recursos de aprendizagem de classe mundial, a OWASP é das fontes mais conhecidas e mais usadas por programadores e tecnológicos para segurar a web.

Mais informação sobre a OWASP e o capítulo Português em [https://owasp.org/www-chapter-lisboa/](https://owasp.org/www-chapter-lisboa/).
