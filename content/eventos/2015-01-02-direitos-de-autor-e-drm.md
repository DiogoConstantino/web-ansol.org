---
categories:
- direitos de autor
- drm
metadata:
  slide:
  - slide_value: 1
  tags:
  - tags_tid: 96
  - tags_tid: 10
  node_id: 264
  event:
    location: Escola Secundária Marques de Castilho - Águeda
    site:
      title: ''
      url: http://all.cm-agueda.pt/
    date:
      start: 2015-05-09 14:00:00.000000000 +01:00
      finish: 2015-05-09 17:00:00.000000000 +01:00
    map: {}
layout: evento
title: Direitos de Autor e DRM
created: 1420222728
date: 2015-01-02
aliases:
- "/evento/264/"
- "/node/264/"
---
<img src="https://ansol.org/attachments/all.jpg" alt="Cartaz" title="Cartaz">

<em>Um Workshop sobre Direitos de Autor e Digital Rights Management e um macaco
no cartaz? Está perdido? Nós esclarecemos…mas só um bocadinho: este famoso
macaco, da espécie Macaca Nigra, mete Ellen DeGeneres “no bolso” no que
respeita a tirar selfies. O resto da história fica para 9 de maio!</em>

Já ouviu falar da Cópia Privada? Sabe o que é DRM? Numa era digital, a
produção, o uso e a reutilização de conteúdos torna-se cada vez mais banal. Mas
qual é a relação entre aquilo que fazemos diariamente nos meios digitais com os
direitos de autor? E será que estes estão adaptados às inovadoras formas de
criar e partilhar?

Num ambiente informal de partilha de experiências e aprendizagem, o ALL e a
ANSOL juntam-se e convidamo-lo a passar connosco uma tarde a falar sobre todos
estes assuntos que estão, cada vez mais, na ordem do dia. A sessão tem início
marcado para as **14H00** do próximo dia **9 de maio**, na **Escola Secundária
Marques de Castilho** e conta com a dinamização de Marcos Marado, Paula Simões
e Teresa Nobre.

A entrada é gratuita mas os lugares são limitados, por isso inscreva-se hoje
mesmo em <a
href="http://cm-agueda.us7.list-manage.com/track/click?u=2e765ab173a4f6c47a88ff607&amp;id=e63aeeab5a&amp;e=5d2bdc94f0">http://all.cm-agueda.pt/</a>.
