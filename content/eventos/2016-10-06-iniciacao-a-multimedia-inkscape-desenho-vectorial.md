---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 463
  event:
    location: Biblioteca dos Coruchéus, Lisboa
    site:
      title: ''
      url: http://blx.cm-lisboa.pt/noticias/detalhes.php?id=1122
    date:
      start: 2016-11-05 10:30:00.000000000 +00:00
      finish: 2016-11-05 12:30:00.000000000 +00:00
    map: {}
layout: evento
title: Iniciação à multimédia - INKSCAPE (desenho vectorial)
created: 1475748518
date: 2016-10-06
aliases:
- "/evento/463/"
- "/node/463/"
---

