---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 464
  event:
    location: Biblioteca dos Coruchéus, Lisboa
    site:
      title: ''
      url: http://blx.cm-lisboa.pt/noticias/detalhes.php?id=1122
    date:
      start: 2016-12-03 10:30:00.000000000 +00:00
      finish: 2016-12-03 12:30:00.000000000 +00:00
    map: {}
layout: evento
title: Iniciação à multimédia - BLENDER (Animação 3D)
created: 1475748587
date: 2016-10-06
aliases:
- "/evento/464/"
- "/node/464/"
---

