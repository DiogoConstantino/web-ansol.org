---
excerpt: "<p>Realizar-se-&aacute; no proximo dia 17 de Novembro, pelas 9:00, no Audit&oacute;rio
  1, Edificio ISCTE I, uma sess&atilde;o subordinada ao tema &ldquo;Desktop Linux
  na Tranquilidade &ndash; Um Case Study&rdquo;, no contexto do Mestrado de Software
  de C&oacute;digo Aberto (MOSS). A entrada &eacute; livre.</p>\r\n"
categories: []
metadata:
  node_id: 101
  event:
    location: Auditório 1, Edificio ISCTE I, Lisboa
    site:
      title: ''
      url: http://masteropensource.wordpress.com/2012/11/04/desktop-linux-na-tranquilidade-um-case-study/
    date:
      start: 2012-11-17 09:00:00.000000000 +00:00
      finish: 2012-11-17 09:00:00.000000000 +00:00
    map: {}
layout: evento
title: Desktop Linux na Tranquilidade – Um Case Study
created: 1352068635
date: 2012-11-04
aliases:
- "/evento/101/"
- "/node/101/"
---
<p>Realizar-se-&aacute; no proximo dia 17 de Novembro, pelas 9:00, no Audit&oacute;rio 1, Edificio ISCTE I, uma sess&atilde;o subordinada ao tema &ldquo;Desktop Linux na Tranquilidade &ndash; Um Case Study&rdquo;, no contexto do Mestrado de Software de C&oacute;digo Aberto (MOSS). A entrada &eacute; livre.</p>
<p>O orador convidado &eacute; Rui Lapa, administrador de sistemas Linux, Integrador de sistemas na Tranqulidade e respons&aacute;vel pelo projecto.</p>
