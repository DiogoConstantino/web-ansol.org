---
layout: evento
title: FOSDEM
metadata:
  event:
    location: ULB Campus Solbosch, Bruxelas, Bélgica
    site:
      url: https://fosdem.org/2023/
    date:
      start: 2023-02-04
      finish: 2023-02-05
---
FOSDEM is a free event for software developers to meet, share ideas and
collaborate.

Every year, thousands of developers of free and open source software from all
over the world gather at the event in Brussels.
