---
categories:
- open data
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 85
  node_id: 344
  event:
    location: UPTEC PINC, Porto
    site:
      title: ''
      url: http://datewithdata.pt/
    date:
      start: 2015-09-12 10:00:00.000000000 +01:00
      finish: 2015-09-12 17:00:00.000000000 +01:00
    map: {}
layout: evento
title: Date with Data
created: 1442006819
date: 2015-09-11
aliases:
- "/evento/344/"
- "/node/344/"
---
<p>O próximo encontro Date With Data vai ser dedicado à <strong>informação legislativa e parlamentar</strong>. Na semana entre 7 e 13 de setembro assinala-se a <a href="http://openparlweek.org" target="_blank">Global Legislative Openness Week</a>. Juntando a isso as eleições que aí vêm no dia 4 de outubro, não podia haver altura mais oportuna para olhar para o trabalho que foi sendo feito ao longo de 5 anos na frente legislativa e eleitoral, e congeminar sobre as novas direções que podemos encontrar.</p><p>As palavras-chave serão dados eleitorais, textos legislativos, dinâmicas parlamentares, sempre fugindo ao olhar burocrático e procurando formas humanas de nos relacionarmos com os órgãos que nos representam. Falar-se-à do <a href="http://demo.cratica.org" target="_blank">Demo.cratica</a> e de outras ferramentas de informação parlamentar, de datasets a publicar e do potencial de toda esta informação para pensarmos ferramentas para uma sociedade mais esclarecida.</p><p>Não é preciso ser hacker para participar, nem saber programar — o que é importante é boa disposição e vontade de trocar ideias, e o resto surge naturalmente!</p><p>Sábado, dia 12, das 10 às 17 no UPTEC PINC (<a href="http://datewithdata.pt/#about">mapa</a>), leve o seu portátil!</p>
