---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 204
  event:
    location: AltLab, Lisboa
    site:
      title: ''
      url: http://altlab.org/2013/08/14/workshop-introducao-a-robotica/
    date:
      start: 2013-09-14 00:00:00.000000000 +01:00
      finish: 2013-09-14 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Workshop Introdução à Robótica
created: 1376748407
date: 2013-08-17
aliases:
- "/evento/204/"
- "/node/204/"
---

