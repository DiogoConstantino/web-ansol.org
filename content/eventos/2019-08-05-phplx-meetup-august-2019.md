---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 685
  event:
    location: Uniplaces HQ, Lisbon
    site:
      title: ''
      url: https://www.meetup.com/php-lx/events/263548276/
    date:
      start: 2019-08-13 19:00:00.000000000 +01:00
      finish: 2019-08-13 19:00:00.000000000 +01:00
    map: {}
layout: evento
title: "<?phplx meetup - August 2019"
created: 1565000227
date: 2019-08-05
aliases:
- "/evento/685/"
- "/node/685/"
---

