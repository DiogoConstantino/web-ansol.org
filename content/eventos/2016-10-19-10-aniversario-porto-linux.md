---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 469
  event:
    location: Porto
    site:
      title: ''
      url: http://portolinux.org/doku.php?id=encontrostecnicos:out2016
    date:
      start: 2016-10-22 15:30:00.000000000 +01:00
      finish: 2016-10-22 15:30:00.000000000 +01:00
    map: {}
layout: evento
title: 10º Aniversário Porto Linux
created: 1476884549
date: 2016-10-19
aliases:
- "/evento/469/"
- "/node/469/"
---

