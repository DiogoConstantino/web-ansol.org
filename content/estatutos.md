---
metadata:
  node_id: 32
layout: page
title: Estatutos da ANSOL
created: 1334479481
date: 2012-04-15
aliases:
- "/node/32/"
- "/page/32/"
---
<style>
ol > li > ol { list-style-type: lower-alpha; }
ol.alpha { list-style-type: lower-alpha; }
</style>

Os estatutos da ANSOL foram revistos pela última vez em Março de 2022.

Veja a versão actual **e oficial** dos estatutos no [ficheiro em
anexo](/attachments/20151028L035F140-141vAltrcPrclEsttts.pdf). A reprodução que
se segue consiste meramente num formato mais prático para a pesquisa.

## CAPÍTULO I - Denominação, natureza, sede e fins

### Artigo primeiro - Denominação e natureza da associação

1. A associação adota o nome ANSOL - Associação Nacional para o Software Livre.
2. Por software livre entende-se todo o programa informático cujo código fonte
   seja de acesso livre e universal, e cuja licença ofereça cumulativamente, a
   todos sem exceção, as seguintes quatro liberdades:
   1. A liberdade de utilizar o programa para qualquer fim;
   2. A liberdade de estudar o funcionamento do programa e de o adaptar a novos problemas;
   3. A liberdade de distribuir o programa a terceiros;
   4. A liberdade de melhorar o programa e de tornar as modificações públicas, em benefício de toda a comunidade.
3. Quando haja dúvida insanável, ou ambiguidade, na interpretação do sentido
   das liberdades acima enunciadas recorrer-se-á às definições originais da
   Free Software Foundation, definições essas que se encontram na Internet no
   sitio da organização citada ([https://www.fsf.org](https://www.fsf.org))
4. A ANSOL é uma associação de âmbito nacional, sem fins lucrativos, que se
   rege pelas leis vigentes, pelos presentes estatutos e pelos respetivos
   regulamentos internos.
5. A associação constitui-se por tempo indeterminado.


### Artigo segundo - Sede

1. A associação tem sede na Rua de Mouzinho da Silveira, número duzentos e
   trinta e quatro, freguesia de Cedofeita, Santo Ildefonso, Sé, Miragaia, São
   Nicolau e Vitória, concelho do Porto.
2. A sede pode ser transferida para qualquer outro local do território nacional
   por simples deliberação da assembleia geral.
3. A associação pode criar delegações regionais ou locais ou outras formas de
   representação em qualquer ponto do território nacional.


### Artigo terceiro - Finalidades

A associação tem como fim a divulgação, promoção, desenvolvimento, investigação
e estudo da informática livre e das suas repercussões sociais, políticas,
filosóficas, culturais, técnicas e científicas.


### Artigo quarto - Atividades

1. Com vista à prossecução dos fins definidos no artigo anterior, a associação
   propõe-se levar a cabo, entre outras, as seguintes atividades:
   1. Contribuir para a produção e divulgação de conhecimento no domínio da
      informática livre;
   1. Fomentar a investigação e a troca constante de ideias, experiências e
      projetos nesta área;
   1. Estabelecer contactos preferenciais com universidades, empresas e outros
      organismos, públicos ou privados, e com associações congéneres,
      nacionais, internacionais e estrangeiras;
   1. Promover e apoiar atividades que contribuam para o desenvolvimento de
      software livre e respetiva documentação, tradução e localização;
   1. Exercer pressões políticas em Portugal e na União Europeia para que sejam
      publicadas leis que incentivem a produção e adoção de software livre, bem
      como para impedir a entrada em vigor de legislação que coloque em causa o
      progresso da informática livre;
   1. Promover atividades tais como cursos, estágios, seminários, colóquios,
      congressos, conferências, encontros e exposições;
   1. Promover e patrocinar a edição de publicações conforme aos objetivos da
      associação e que contribuam para um melhor esclarecimento público sobre
      as implicações e relevância da informática livre;
   1. Dialogar com as empresas de modo a desenvolver e aperfeiçoar modelos de
      negócio baseados na informática livre;
   1. Promover a utilização de normas, protocolos, interfaces e formatos de
      ficheiros não-proprietários, livres e abertos;
   1. Defender nos tribunais, ou por qualquer outro meio, os interesses da
      comunidade nacional e internacional de software livre;
   1. Prestar aos seus associados apoio jurídico, ou qualquer outro tipo de
      apoio necessário para a defesa dos seus interesses, quando estes se
      enquadrem nos objetivos da associação.

## CAPÍTULO II - Dos Associados

### Artigo quinto - Sócios

1. Podem ser sócios da associação todas as pessoas singulares, nacionais ou
   estrangeiras, que possam contribuir para a prossecução dos objetivos da
   associação.
2. Os associados adquirem o pleno gozo dos seus direitos seis meses após a
   aprovação do seu pedido de inscrição, podendo no entanto este prazo ser
   reduzido ou eliminado por decisão da assembleia geral.

### Artigo sexto - Direito dos sócios

1. Os sócios terão os seguintes direitos:
   1. Propor, colaborar, participar e ser informados das atividades da associação;
   1. Participar, ter voz e voto na assembleia geral;
   1. Eleger e serem eleitos para os órgãos sociais;
   1. Usufruir das regalias que a associação concede aos seus membros;
   1. Possuir um exemplar dos estatutos e dos regulamentos internos.

### Artigo sétimo - Deveres dos sócios

1. A todos os sócios cabem deveres iguais perante a associação, nomeadamente:
   1. Cumprir as disposições dos estatutos e dos regulamentos internos;
   1. Pagar as quotas conforme estabelecido no regulamento interno;
   1. Acatar as deliberações da direção;
   1. Exercer as funções em que sejam investidos.

### Artigo oitavo - Penalidades

1. As penalidades que podem ser impostas aos sócios são as seguintes:
   1. Suspensão;
   1. Exclusão.
2. Incorrem em pena de suspensão de direitos:
   1. Os sócios que não cumpram o disposto no artigo sétimo;
   1. Os que causarem danos ou prejuízos morais ou materiais à associação e os
      não repararem no prazo que a direção lhes indicar.
3. Incorrem em pena de exclusão:
   1. Os que tenham prestado informações falsas nas suas propostas para sócios;
   1. Os sócios reincidentes, que incorram em pena de suspensão;
   1. Os sócios que não regularizem as quotas no prazo definido no regulamento
      interno.
4. A aplicação de penas de suspensão é da competência da direção após
   admoestação do sócio e nunca pode ser superior a seis meses.
5. A aplicação de penas de exclusão é da competência da assembleia geral sob
   proposta da direção em exercício, exceto no caso do ponto 3) alínea c) que é
   da competência da direção.
6. A direção pode proceder à suspensão do sócio que incorra em pena de
   exclusão, até à deliberação da assembleia geral.
7. Os sócios que incorram em pena de suspensão ou exclusão não têm direito ao
   reembolso das quotas pagas.
8. Os sócios excluídos podem ser readmitidos em assembleia geral expressamente
   convocada para o efeito, se a decisão for aprovada por maioria de pelo menos
   dois terços dos presentes, em votação secreta.

## CAPÍTULO III - Funcionamento

### Artigo nono - Órgãos sociais

1. São órgãos sociais da associação:
   1. A assembleia geral;
   1. O conselho fiscal;
   1. A direção.
2. Os órgãos sociais são eleitos por votação secreta dos sócios no pleno gozo
   dos seus direitos, durante a assembleia geral, entrando em funções num prazo
   de quinze dias.
3. O mandato dos membros dos órgãos sociais é de dois anos.
4. Verificada, por qualquer motivo, uma vaga num dos órgãos sociais, os
   restantes membros do órgão em cousa escolhem, de entre os associados, um
   novo titular, que desempenhará o cargo até à realização da assembleia geral
   eleitoral seguinte.

### Artigo décimo - Assembleia geral

1. A assembleia geral é o órgão soberano da associação e é constituída por
   todos os sócios no pleno gozo dos seus direitos associativos e pelos membros
   da mesa da assembleia geral.
2. O funcionamento da assembleia geral é o previsto no artigo 175º, números 1,
   2, 3 e 4 do Código Civil.
3. A assembleia geral é convocada com a antecedência mínima de oito dias por
   correio eletrónico ou, se o sócio solicitar previamente, por carta; a
   convocatória é simultaneamente publicada no sítio electrónico da associação.
4. Da convocatória da Assembleia Geral constarão obrigatoriamente os seguintes
   elementos:
   1. O dia, o local, a hora e a ordem de trabalhos;
   2. Que a Assembleia reunirá em segunda convocatória trinta minutos após a
      primeira,   se a esta não estiver presente mais de metade dos associados
      com direito a voto, com  qualquer número de associados presentes.
5. As assembleias gerais podem ser realizadas por meios telemáticos, conforme
   definido em regulamento interno.
6. Os sócios podem fazer-se representar na assembleia geral, nos termos
   definidos em regulamento interno.
7. Não é admissível o voto por correspondência.

### Artigo décimo primeiro - mesa da assembleia geral

1. A mesa da assembleia geral é constituída por um presidente auxiliado por
   dois secretários e regula as atividades da assembleia geral, competindo-lhe:
   1. Emitir convocatórias, dirigir as sessões e elaborar as atas da assembleia
      geral;
   1. Apreciar a legalidade das votações;
   1. Dirigir o processo de eleição dos órgãos sociais.
2. Na falta ou impedimento do presidente da mesa, a assembleia pode funcionar,
   sendo aquele substituído por um dos secretários.

### Artigo décimo segundo - Competência da assembleia geral

1. A assembleia geral tem competência para deliberar sobre quaisquer matérias
   constantes da convocatória, nos termos destes estatutos, nomeadamente:
   1. Eleger os órgãos sociais;
   1. Discutir e aprovar anualmente o relatório, balanço e contas apresentado
      pela direção, bem como o parecer do conselho fiscal;
   1. Aprovar e alterar os regulamentos internos da associação, se a decisão
      for aprovada por dois terços;
   1. Deliberar sobre a destituição de quaisquer órgãos sociais ou sobre a
      demissão de algum dos seus titulares, mediante proposta da direção ou de
      qualquer sócio com indicação obrigatória dos deveres violados;
   1. Deliberar sobre a alteração dos estatutos, dissolução e liquidação da
      associação ou ainda sobre todas as matérias não compreendidas nas
      atribuições legais ou estatutárias dos restantes órgãos da associação;
   1. Aprovar o orçamento da associação para cada ano civil;
   1. Aprovar o plano anual de atividades;
   1. Deliberar sobre quaisquer outras questões não compreendidas na
      competência exclusiva de outros órgãos, que interessem à atividade da
      associação.

### Artigo décimo terceiro - Assembleias gerais ordinárias e extraordinárias

1. A assembleia geral ordinária realiza-se anualmente e compete-lhe:
   1. Apreciar e votar o relatório, balanço e contas da direção e o parecer do
      conselho fiscal do exercício anterior;
   1. Proceder à eleição das órgãos sociais para o próximo mandato, caso seja
      ano eleitoral;
   1. Deliberar sobre qualquer assunto mencionado na respetiva convocatória.
2. Poderão realizar-se assembleias gerais extraordinárias por convocação do
   presidente da mesa da assembleia geral, mediante solicitação feita a este
   pela direção, pelo conselho fiscal, ou por pelo menos uma quinta parte dos
   associados, com indicação precisa do objeto da reunião.

### Artigo décimo quarto - Conselho fiscal

1. O conselho fiscal é constituído por um presidente e dois secretários e é o
   órgão fiscalizador das atividades da direção. competindo-lhe:
   1. Conferir os saldos de caixa, verificando todos os documentos de entrada e
      saída e sua legalidade;
   1. Estar perfeitamente informado de todas as atividades da direção e da
      associação em geral;
   1. Dar o seu parecer de qualquer assunto, quando lhe seja feita consulta por
      parte da direção ou durante a assembleia geral pelo presidente da mesa;
   1. Elaborar o seu parecer, acerca do relatório e contas da direção, para ser
      apreciado em assembleia geral;
   1. Solicitar esclarecimentos à direção, sempre que as decisões ou ações
      desta aparentem violar os estatutos, os regulamentos internos, ou as leis
      vigentes.
2. O conselho fiscal é convocado pelo seu presidente.

### Artigo décimo quinto - Direção

1. A direção é constituída por um presidente, um vice-presidente, um
   tesoureiro, um secretário e um vogal e é o órgão colegial de administração
   da associação, competindo-lhe:
   1. Dar cumprimento às deliberações da assembleia geral;
   1. Administrar os assuntos da associação de acordo com a lei, os estatutos e
      os regulamentos internos;
   1. Coordenar todas as atividades desenvolvidas e nomear grupos de trabalho
      diferenciados, fazendo-se representar por um dos seus elementos;
   1. Representar a associação perante as entidades oficiais e outros
      organismos;
   1. Apresentar anualmente à assembleia geral um relatório de atividade
      desenvolvida e das contas para apreciação e votação;
   1. Responder solidariamente perante a assembleia geral;
   1. Responder, num prazo de cinco dias úteis, a qualquer questão colocada
      pelo conse1ho fiscal;
   1. Deliberar sobre a admissão de novos sócios, suspendê-los ou propor à
      assembleia geral a sua exclusão, depois de elaborado o respetivo processo
      em conformidade com os estatutos e regulamentos internos;
   1. Estabelecer e assinar protocolos considerados importantes para os fins e
      objetivos da associação.
2. A associação considerar-se-á validamente obrigada quando os atos e contratos
   em que intervenha forem assinados por pelo menos dois membros da direção,
   incluindo o presidente. Em caso de impedimento do presidente serão
   necessárias as assinaturas de três membros da direção.
3. A movimentação das contas bancárias necessita de duas assinaturas de entre o
   presidente, o vice-presidente e o tesoureiro.
4. A direção é convocada pelo seu presidente.

### Artigo décimo sexto - Listas de candidatos a membros dos órgãos sociais

1. As listas de candidatura terão de ser compostas por associados no pleno gozo
   dos seus direitos, cabendo aos candidatos a sua apresentação.
2. Os proponentes enviarão ao presidente da mesa da assembleia geral, até
   trinta dias antes das eleições, as listas de candidatura conforme definido
   no regulamento interno.
3. O presidente da mesa da assembleia geral divulgará aos associados, através
   dos meios especificados no regulamento interno, a composição das listas
   candidatas, até quinze dias antes da das das eleições.

## CAPÍTULO IV - Disposições finais e transitórias

### Artigo décimo sétimo - Destino do património em caso de extinção

Na eventualidade da extinção da associação, o seu património será atribuído a
uma organização, que possua objetivos análogos, a designar em assembleia geral,
sem prejuízo do disposto no artigo 166º, numero 1 do Código Civil.

### Artigo décimo oitavo - Decisões sobre questões omissas

1. No que os presentes estatutos, legislação aplicável ou regulamentos internos
   forem omissos, as decisões competirão à direção em exercício.
2. Dessas decisões pode qualquer sócio, no pleno gozo dos seus direitos,
   recorrer para a assembleia geral.
