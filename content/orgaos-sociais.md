---
metadata:
  node_id: 334
layout: page
title: Órgãos Sociais
created: 1437071553
date: 2015-07-16
aliases:
- "/node/334/"
- "/orgaos-sociais/"
- "/page/334/"
---

Os Órgãos Sociais para o mandato 2024 &ndash; 2026 foram eleitos a 16 de março de 2024.

## Direcção [direccao@ansol.org](mailto:direccao@ansol.org)

Presidente: **Tiago Carreira** [tiago.carreira@ansol.org](mailto:tiago.carreira@ansol.org)

Vice-Presidente: **Hugo Peixoto** [hugo.peixoto@ansol.org](mailto:hugo.peixoto@ansol.org)

Tesoureiro: **André Freitas** [andre.freitas@ansol.org](mailto:andre.freitas@ansol.org)

Vogal: **André Alves** [andre.alves@ansol.org](mailto:andre.alves@ansol.org)

Secretária: **Paula Simões** [paula.simoes@ansol.org](mailto:paula.simoes@ansol.org)


## Mesa da Assembleia [mesa.assembleia@ansol.org](mailto:mesa.assembleia@ansol.org)

Presidente: **Rômulo Sellani** [romulo.sellani@ansol.org](mailto:romulo.sellani@ansol.org)

1º Secretário: **Rúben Mendes** [ruben.mendes@ansol.org](mailto:ruben.mendes@ansol.org)

2º Secretário: **Rui Seabra** [rui.seabra@ansol.org](mailto:rui.seabra@ansol.org)


## Conselho Fiscal [conselho.fiscal@ansol.org](mailto:conselho.fiscal@ansol.org)

Presidente: **Jaime Pereira** [jaime.pereira@ansol.org](mailto:jaime.pereira@ansol.org)

1ª Secretária: **Sandra Fernandes** [sandra.fernandes@ansol.org](mailto:sandra.fernandes@ansol.org)

2º Secretário: **Tiago Carrondo** [tiago.carrondo@ansol.org](mailto:tiago.carrondo@ansol.org)
