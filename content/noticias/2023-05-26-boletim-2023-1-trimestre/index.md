---
categories:
- newsletter
- boletim
layout: article
title: Boletim 2023 - 1º trimestre
date: 2023-05-26
---

No 1° trimestre de 2023, a ANSOL participou na FOSDEM, na MiniDebConfPT 2023, e
reuniu com os membros do parlamento europeu portugueses. Também celebrámos o
Dia do Domínio Público e participámos numa consulta pública sobre Direitos de Autor.

<!--more-->

## FOSDEM / Parlamento Europeu

Este ano a [FOSDEM](https://fosdem.org) voltou ao modelo presencial. A ANSOL
enviou dois membros da direcção a Bruxelas para participar na conferência e nos
eventos envolventes.

A FOSDEM foi uma excelente oportunidade para estreitar relações com as pessoas
de outras comunidades/entidades (como a FSFE) e combinar actividades em conjunto,
incluindo a Festa do Software Livre 2023.

Durante a estadia em Bruxelas reunimos com membros do parlamento europeu
portugueses e alertar para os problemas da proposta do regulamento que
"estabelece regras para prevenir e combater o abuso sexual de crianças"
([ChatControl](https://chatcontrol.eu)). A lista de reuniões está disponível na
[nossa página de transparência](https://ansol.org/transparencia/).


## MiniDebConfPT 2023

A ANSOL participou na MiniDebConfPT 2023, um evento de 5 dias no Técnico
Lisboa. O Marcos Marado fez uma apresentação sobre o dia "I <3 Free Software" e
sobre a ANSOL ([o vídeo está disponível
online](https://pt2023.mini.debconf.org/talks/2-i-3-free-software/)). Fizemos
também um esforço de angariação de sócios, oferecendo t-shirts a quem se
inscrevesse durante o evento.


## Dia do Dominío Público

A Wikimedia Portugal, BNP e ANSOL juntaram-se para celebrar a ocasião, não só
publicando a anual listagem de autores cujas obras entram em domínio público,
mas também com um [evento presencial que decorreu no dia 6 de Janeiro de 2023](https://pt.wikimedia.org/wiki/Dia_do_Dom%C3%ADnio_P%C3%BAblico_2023).


## Participação em consulta pública sobre Direitos de Autor

No seguimento do contributo enviado ao Parlamento em Outubro passado, e depois
do apelo feito em [carta
aberta](https://tek.sapo.pt/noticias/internet/artigos/direito-de-autor-carta-aberta-de-11-organizacoes-quer-transposicao-da-diretiva-no-parlamento)
em que a ANSOL apelou ao Parlamento que a transposição da Directiva Europeia
que actualiza as matérias de Direito de Autor no âmbito do Mercado Único
Digital, a ANSOL participou agora na [consulta pública ao Decreto-Lei que
transpõe a directiva](https://ansol.org/noticias/2023-05-06-consulta-publica/).


## Outras reuniões

A pedido dos próprios reunimos com uma investigadora da Academia Austríaca de
Ciências e um investigador da Universidade de Bolonha que, no âmbito das suas
teses de doutoramento, solicitaram entrevistas para conhecer o trabalho
desenvolvido pela ANSOL.


## Segue as actividades da ANSOL

Podem seguir as actividades através da nossa presença em várias redes:

- Segue-nos no Mastodon: <https://floss.social/@ansol>
- Segue-nos no Peertube: <https://viste.pt/c/ansol/>
- Segue-nos no Twitter: <https://twitter.com/ANSOL>
- Junta-te à nossa sala via Matrix: <https://matrix.to/#/#geral:ansol.org>
- Vê os nossos repositórios de Software Livre: <https://git.ansol.org/ansol>

Se consideras importante o tema de Software Livre na sociedade, considera
[juntar-te à ANSOL](https://ansol.org/inscricao/). A ANSOL depende
exclusivamente da contribuição dos seus membros para suportar as suas
actividades, e gostaríamos de contar com a tua participação.
