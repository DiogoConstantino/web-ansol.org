---
excerpt: A ANSOL e mais de 5000 peticionários opuseram-se hoje à proposta de lei 246/XII
  perante a presidência da Assembleia da República por volta das 12:00, valor bem
  acima do mínimo a que obriga a AR a debater a petição em plenário.
categories:
- "#pl246"
- cópia privada
- press release
- imprensa
- "#pl118"
metadata:
  tags:
  - tags_tid: 45
  - tags_tid: 11
  - tags_tid: 9
  - tags_tid: 19
  - tags_tid: 44
  node_id: 225
layout: article
title: '5000+ disseram NÃO ao #pl246. AR não pode ignorar'
created: 1410979222
date: 2014-09-17
aliases:
- "/article/225/"
- "/node/225/"
- "/pr-20140917/"
---
<p>Lisboa, 17 de Setembro de 2014 - A ANSOL (Associação Nacional para o Software Livre) e mais de 5000 peticionários opuseram-se hoje à proposta de lei 246/XII perante a presidência da Assembleia da República por volta das 12:00, valor bem acima do mínimo a que obriga a AR a debater a petição em plenário.</p><p>«<em>Fomos mil mais que os exigidos para o Parlamento ter que debater esta petição em plenário, não podem ignorar os peticionários</em>», diz Rui Seabra, presidente da Direção da ANSOL, acrescentando ainda que «<em>a desinformação propagada em plenário revela que o assunto não está claro para poder ser votado, muito menos com seis milhares de opositores reunidos em dois dias</em>».</p><p>A ANSOL defende que os&nbsp; deputados da assembleia da república deveriam parar o processo por muitos não apresentarem compreensão suficiente do tema para o poder votar de consciêncie tranquila.</p><p>«<em>Parar o processo, alargar o debate, ouvir os cidadãos</em>» conclui Rui Seabra.</p><p>Às 19:30 a petição encontra-se a poucas dezenas das 6000 assinaturas e encontra-se disponível em <a href="http://bit.do/pl246">http://bit.do/pl246</a></p>
