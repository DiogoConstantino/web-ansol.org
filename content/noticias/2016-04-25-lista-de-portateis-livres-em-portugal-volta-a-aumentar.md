---
categories:
- portáteis
- portáteis livres
- portátil livre
- portátil
- dell
metadata:
  tags:
  - tags_tid: 164
  - tags_tid: 165
  - tags_tid: 166
  - tags_tid: 167
  - tags_tid: 168
  node_id: 417
layout: article
title: Lista de Portáteis Livres em Portugal volta a aumentar
created: 1461622600
date: 2016-04-25
aliases:
- "/article/417/"
- "/node/417/"
---
<div id="magicdomid4"><span class="author-a-1z86zz69z8z122zyosgnz67zjz73ziz79zz122z">Uma das perguntas mais frequentes feitas à ANSOL, e uma das páginas mais requisitadas do site, é aquela que mantemos com informação de <a href="https://ansol.org/portateis-livres">como adquirir em Portugal um Portátil Livre</a> -- um Portátil que seja vendido sem Sistema Operativo ou com um Sistema Operativo Livre.</span></div><div id="magicdomid5">&nbsp;</div><div id="magicdomid6"><span class="author-a-1z86zz69z8z122zyosgnz67zjz73ziz79zz122z">Comprar um portátil é uma actividade que deveria ser simples e comum. No entanto, o normal é o consumidor ser forçado, na compra de um dispositivo, a adquirir também licenciamento para diverso software, mesmo que dele não queira usufruir. Ainda estamos longe de nos encontrarmos numa posição de poder entrar numa qualquer loja de retalho e sair de lá com o portátil que queremos e só com o Software que queremos, mas felizmente é já possível escolher de uma pequena lista de opções, principalmente se for adepto das compras online.</span></div><div id="magicdomid7">&nbsp;</div><div id="magicdomid8"><span class="author-a-1z86zz69z8z122zyosgnz67zjz73ziz79zz122z">A lista tem-se vindo a alterar ao longo dos anos, mas agora cresceu um pouco mais:</span></div><div id="magicdomid9">&nbsp;</div><div id="magicdomid10"><span class="author-a-1z86zz69z8z122zyosgnz67zjz73ziz79zz122z">O <a href="http://www.dell.com/pt/empresas/p/xps-13-9350-laptop-ubuntu/pd">Dell XPS 13 Developer Edition</a> "alia a performance e a mobilidade do XPS 13 ao Ubuntu 14.04 LTS para criar uma solução cliente-para-nuvem para os programadores", e está agora também disponível em Portugal através da <a href="http://www.tcsi-digiberia.pt/P05.aspx?10">TCSI</a>.</span></div><div id="magicdomid11">&nbsp;</div><div id="magicdomid13"><span class="author-a-1z86zz69z8z122zyosgnz67zjz73ziz79zz122z">De especial interesse, este portátil acrescenta uma diferente gama à lista de opões, sendo um portátil popular por entre os clientes empresariais, que vêm pela primeira vez uma verdadeira hipótese de reduzir os seus custos com os postos de trabalho, não tendo de adquirir para eles licenças que não irão utilizar.</span></div><div id="magicdomid14">&nbsp;</div><div id="magicdomid15"><span class="author-a-1z86zz69z8z122zyosgnz67zjz73ziz79zz122z">Esperemos que esta lista continue a crescer. Boas compras!</span></div>
