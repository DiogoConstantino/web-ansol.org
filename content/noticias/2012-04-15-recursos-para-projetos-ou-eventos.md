---
categories: []
metadata:
  node_id: 34
layout: page
title: Recursos para projetos ou eventos
created: 1334480944
date: 2012-04-15
aliases:
- "/node/34/"
- "/page/34/"
- "/recursos/"
---
<h2>
	Lista de recursos dispon&iacute;veis para qualquer projecto de software livre.</h2>
<p>Esta &eacute; uma lista de recursos (principalmente f&iacute;sicos) dispon&iacute;veis para qualquer projecto ou evento de Software Livre:</p>
<ul>
	<li>
		<b>Espa&ccedil;o Musas</b> - Contacto: Espa&ccedil;o Musas, R. Bonjardim, 998, 4000-121 Porto e/ou <span class="link-mailto"><a href="mailto:musas@musas.pegada.net">musas@musas.pegada.net</a></span>.</li>
	<li>
		<b>Oradores</b> - Enquanto n&atilde;o &eacute; publicada uma lista de oradores, contacte a <a href="/contacto">ANSOL</a> com uma breve descri&ccedil;&atilde;o do que procura. Temas poss&iacute;veis incluem: Software Livre, implementa&ccedil;&atilde;o, migra&ccedil;&otilde;es, escola livre, entre outros.</li>
	<li>
		<b>V&iacute;deoprojector</b> - Dispon&iacute;vel para eventos ou projectos de Software Livre. Contacto: <a href="/contacto">ANSOL</a>. Local habitual: Lisboa.</li>
</ul>
<p>Se conhecer mais algum recurso dispon&iacute;vel para este fim, por favor <a href="/contacto">contacte-nos</a>. Teremos todo o prazer em fazer crescer esta lista.</p>
