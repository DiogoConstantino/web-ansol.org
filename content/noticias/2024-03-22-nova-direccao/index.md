---
layout: article
title: Tomada de posse dos Órgãos Sociais
date: 2024-03-22
---

A 16 de março de 2024 foram eleitos e tomaram posse os [novos órgãos
sociais](orgaos-sociais) da ANSOL para o mandato 2024-2026:

**Direcção**:
- Presidente: Tiago Carreira
- Vice-Presidente: Hugo Peixoto
- Tesoureiro: André Freitas
- Vogal: André Alves
- Secretária: Paula Simões

**Mesa da Assembleia**:
- Presidente: Rômulo Sellani
- 1º Secretário: Rúben Mendes
- 2º Secretário: Rui Seabra

**Conselho Fiscal**:
- Presidente: Jaime Pereira
- 1ª Secretária: Sandra Fernandes
- 2º Secretário: Tiago Carrondo

Os relatórios de contas e actividades relativos a 2023 e o plano de actividades
e orçamento para 2024 podem ser consultados na nossa [página de
transparência](/transparencia).

Foi também aprovado um plano estratégico até 2030 que define quatro áreas de
actuação que consideramos críticos para o sucesso da associação e do Software
Livre em Portugal. Resumidamente:

- **Legislação**: Propomos investir esforços para garantir que o RNID é bem
  actualizado, e que a liberdade de roteadores é respeitada;
- **Comunidade**: Propomos organizar uma Festa do Software Livre todos os anos;
- **Divulgação**: Propomos fazer contribuições regulares de artigos para duas
  publicações de terceiros;
- **Projectos de Software Livre**: Propomos construir Software Livre de
  utilidade pública.

O documento completo também se encontra disponível na página de transparência.
