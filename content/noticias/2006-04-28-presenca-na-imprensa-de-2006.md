---
categories:
- imprensa
- '2006'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 19
  - tags_tid: 25
  node_id: 152
layout: page
title: Presença na Imprensa de 2006
created: 1146225199
date: 2006-04-28
aliases:
- "/imprensa/2006/"
- "/node/152/"
- "/page/152/"
---
<ul><li><p class="line862">2006-01-29:&nbsp;<a href="http://tek.sapo.pt/extras/site_do_dia/sapo_livre_com_ansol_886325.html" class="http">SAPO livre com Ansol</a></p></li><li><p class="line862">2006-02-02:&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/ansol_responde_a_acordo_da_microsoft_com_dist_872326.html" class="http">ANSOL responde a acordo da Microsoft com distribuição de pacote de software livre</a></p></li><li><p class="line862">2006-03-14:&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/acordos_recentes_do_governo_deixam_verdadeiro_872354.html" class="http">Acordos recentes do Governo deixam verdadeiro software livre de fora</a></p></li><li><p class="line862">2006-03-31:&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/grupo_parlamentar_do_pcp_pede_esclarecimentos_872361.html" class="http">Grupo Parlamentar do PCP pede esclarecimentos sobre memorando com Microsoft</a></p></li><li><p class="line862">2006-05-18:&nbsp;<a href="http://tek.sapo.pt/Arquivo/ansol_renovada_667335.html" class="http">ANSOL renovada</a></p></li></ul>
