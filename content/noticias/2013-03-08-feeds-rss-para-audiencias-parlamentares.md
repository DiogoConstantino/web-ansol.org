---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 133
layout: page
title: Feeds RSS para Audiências Parlamentares
created: 1362774323
date: 2013-03-08
aliases:
- "/node/133/"
- "/page/133/"
- "/politica/ap/parl/feeds/"
---
*Esta página, montada para facilitar o uso do antigo portal do Parlamento, encontra-se aqui por motivos históricos. As comissões são relativas à XII legislatura. Os feeds RSS já não são mantidos.*

Para que possamos todos estar mais a par das audiências que decorrem nas
Comissões Parlamentares, a ANSOL montou um conjunto de *feeds* RSS para
as audiências de cada Comissão:

1. [Comissão de Assuntos Constitucionais, Direitos, Liberdades e Garantias](https://www.parlamento.pt/sites/com/XIILeg/1CACDLG) - [<a href="https://ansol.org/feeds/audiencias-cacdlg.xml">feed RSS</a>]
1. [Comissão de Negócios Estrangeiros e Comunidades Portuguesas](https://www.parlamento.pt/sites/com/XIILeg/2CNECP)             - [<a href="https://ansol.org/feeds/audiencias-cnecp.xml">feed RSS</a>]
1. [Comissão de Defesa Nacional](https://www.parlamento.pt/sites/com/XIILeg/3CDN)                                               - [<a href="https://ansol.org/feeds/audiencias-cdn.xml">feed RSS</a>]
1. [Comissão de Assuntos Europeus](https://www.parlamento.pt/sites/com/XIILeg/4CAE)                                             - [<a href="https://ansol.org/feeds/audiencias-cae.xml">feed RSS</a>]
1. [Comissão de Orçamento, Finanças e Administração Pública](https://www.parlamento.pt/sites/com/XIILeg/5COFAP)                 - [<a href="https://ansol.org/feeds/audiencias-cofap.xml">feed RSS</a>]
1. [Comissão de Economia e Obras Públicas](https://www.parlamento.pt/sites/com/XIILeg/6CEOP)                                    - [<a href="https://ansol.org/feeds/audiencias-ceop.xml">feed RSS</a>]
1. [Comissão de Agricultura e Mar](https://www.parlamento.pt/sites/com/XIILeg/7CAM)                                             - [<a href="https://ansol.org/feeds/audiencias-cam.xml">feed RSS</a>]
1. [Comissão de Educação, Ciência, e Cultura](https://www.parlamento.pt/sites/com/XIILeg/8CECC)                                 - [<a href="https://ansol.org/feeds/audiencias-cecc.xml">feed RSS</a>]
1. [Comissão de Saúde](https://www.parlamento.pt/sites/com/XIILeg/9CS)                                                          - [<a href="https://ansol.org/feeds/audiencias-cs.xml">feed RSS</a>]
1. [Comissão de Segurança Social e Trabalho](https://www.parlamento.pt/sites/com/XIILeg/10CSST)                                 - [<a href="https://ansol.org/feeds/audiencias-csst.xml">feed RSS</a>]
1. [Comissão do Ambiente, Ordenamento do Território e Poder Local](https://www.parlamento.pt/sites/com/XIILeg/11CAOTPL)         - [<a href="https://ansol.org/feeds/audiencias-caotpl.xml">feed RSS</a>]
1. [Comissão para a Ética, a Cidadania e a Comunicação](https://www.parlamento.pt/sites/com/XIILeg/12CPECC)                     - [<a href="https://ansol.org/feeds/audiencias-cpecc.xml">feed RSS</a>]
