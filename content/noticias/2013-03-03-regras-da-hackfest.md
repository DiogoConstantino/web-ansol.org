---
categories: []
metadata:
  node_id: 132
layout: page
title: Regras da Hackfest
created: 1362345017
date: 2013-03-03
aliases:
- "/dfd2013/hackfestrules/"
- "/node/132/"
- "/page/132/"
---
<p>Este &eacute; um conjunto de regras simples para os projetos candidatos &agrave; Hackfest:</p>
<ul>
	<li>
		N&atilde;o podem receber pr&eacute;mios os atuais membros de org&atilde;os sociais da ANSOL, nem dos membros do Juri, nem seus familiares diretos</li>
	<li>
		Os participantes devem ter menos de 30 anos de idade (ou seja, idade para poder tirar o Cart&atilde;o Jovem)</li>
	<li>
		As propostas t&ecirc;m de ser licenciadas como <a href="https://ansol.org/filosofia">Software Livre</a>, preferencialmente (mas n&atilde;o obrigatoriamente) utilizando as licen&ccedil;as da fam&iacute;lia GNU (<a href="http://www.gnu.org/licenses/gpl.html">GNU GPL</a>, <a href="http://www.gnu.org/licenses/lgpl.html">GNU Lesser GPL</a>, <a href="http://www.gnu.org/licenses/agpl.html">GNU Affero GPL</a>)</li>
	<li>
		Pontos extras para projetos hospedados em sites que promovam Software Livre e que sejam implementados com Software Livre (<a href="http://gitorious.org/">Gitorious</a>, <a href="http://savannah.gnu.org/">Savannah</a>, etc...)</li>
</ul>
<p>Para esclarecimento de d&uacute;vidas, <a href="https://ansol.org/contato">contactar a Dire&ccedil;&atilde;o da ANSOL</a> ou comentar abaixo.</p>
