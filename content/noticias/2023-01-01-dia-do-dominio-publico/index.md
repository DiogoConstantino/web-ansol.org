---
categories:
- domínio público
- portugal
- autores
- direito de autor
- copyright
layout: article
aliases:
- "/dominio-publico-2023/"
title: Feliz Dia do Domínio Público 2023
date: 2023-01-01
---

No dia 1 de Janeiro, em Portugal, entram em domínio público as obras daqueles autores que morreram há mais de 70 anos. Continuando uma [tradição anual](https://ansol.org/noticias/2022-01-01-dia-do-dominio-publico), a ANSOL celebra o dia de hoje, este ano juntamente com a Biblioteca Nacional de Portugal e a Wikimedia Portugal.

A Biblioteca Nacional de Portugal publicou no seu site a [lista dos autores Portugueses cujas obras entram hoje em domínio público](https://www.bnportugal.gov.pt/index.php?option=com_content&view=article&id=1753%3Adia-do-dominio-publico-1-janeiro&catid=174%3A2023&Itemid=1749&lang=pt).

A Wikipedia também publicou uma [lista de autores](https://pt.wikipedia.org/wiki/Lista_de_autores_portugueses_que_entram_em_dom%C3%ADnio_p%C3%BAblico_em_2023), obtida através da Wikidata, e um artigo no seu site que pode ser [lido aqui](https://blog.wikimedia.pt/2023/01/01/dia-do-dominio-publico-2023/).

Tal como no ano passado, em 2023 Wikimedia Portugal, BNP e ANSOL juntam-se para celebrar a ocasião, não só publicando a anual listagem de autores cujas obras entram em domínio público, mas também com um evento presencial a decorrer no [dia 6 de Janeiro de 2023](https://pt.wikimedia.org/wiki/Dia_do_Dom%C3%ADnio_P%C3%BAblico_2023).

---

*Nota:* A imagem deste artigo representa Teixeira de Pascoais, por António Carneiro, cujas obras entram hoje em Domínio Público.
