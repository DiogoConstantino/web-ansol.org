---
categories: []
metadata:
  image:
  - image_fid: 61
    image_alt: Excerto do relatório
    image_title: ''
    image_width: 1696
    image_height: 393
  node_id: 758
layout: article
title: 'OSOR: ANSOL é "Strategic Player" na política open source em Portugal'
created: 1602670520
date: 2020-10-14
aliases:
- "/article/758/"
- "/node/758/"
---
<p data-pm-slice="1 1 []">O Open Source Observatory (<a href="https://joinup.ec.europa.eu/collection/open-source-observatory-osor/about" target="_blank" title="https://joinup.ec.europa.eu/collection/open-source-observatory-osor/about">OSOR</a>), da Comissão Europeia, considera a Associação Nacional para o Software Livre (ANSOL) um ator estratégico no desenvolvimento das políticas de software de código aberto em Portugal. Na ficha técnica sobre Portugal, o Observatório concluiu não existir uma entidade central no governo português para o desenvolvimento e supervisão de software de código aberto, mas destacou o papel estratégico da ANSOL na disseminação, promoção, desenvolvimento, investigação, e estudo daquele software.</p><p>O OSOR ressalta ainda algumas políticas governamentais, como a aprovação da Lei das Normas Abertas de 2011, e estratégias de promoção do software de código aberto na administração pública. O trabalho da Agência para a Modernização Administrativa e do Município de Sintra são algumas das iniciativas distinguidas.</p><p>No relatório sobre Portugal, mais detalhado, que acompanha a ficha técnica, o observatório inclui ainda a Associação de Empresas de Software Open Source Portuguesas (<a href="https://www.esop.pt/" target="_blank" title="https://www.esop.pt/">ESOP</a>) como outro ator estratégico no panorama nacional e completa a lista de estratégias políticas, assim como iniciativas de munícipios, universidades, e outras entidades da administração pública portuguesas relativas ao uso e promoção do software de código aberto.</p><p>Open Source Software Country Intelligence Report Portugal 2020 <a href="https://joinup.ec.europa.eu/sites/default/files/inline-files/OSS%20Country%20Intelligence%20Report_PT_1.pdf" target="_blank" title="https://joinup.ec.europa.eu/sites/default/files/inline-files/OSS%20Country%20Intelligence%20Report_PT_1.pdf">Descarregar PDF</a></p><p>Open Source Software Country Intelligence Factsheet Portugal 2020 <a href="https://joinup.ec.europa.eu/sites/default/files/inline-files/OSS%20Country%20Intelligence%20Factsheet_PT_0.pdf" target="_blank" title="https://joinup.ec.europa.eu/sites/default/files/inline-files/OSS%20Country%20Intelligence%20Factsheet_PT_0.pdf">Descarregar PDF</a></p><p>Página do Observatório Open Source com os <a href="https://joinup.ec.europa.eu/collection/open-source-observatory-osor/open-source-software-country-intelligence" target="_blank" title="https://joinup.ec.europa.eu/collection/open-source-observatory-osor/open-source-software-country-intelligence">relatórios de vários países europeus</a>.</p>
