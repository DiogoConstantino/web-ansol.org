---
categories:
- ilovefs
- "#ilovefs"
- ansol
- fsfe
- phabricator
- nextcloud
- freescout
- bootstrap form builder
layout: article
title: Eu adoro Software Livre
created: 1613299476
date: 2021-02-14
aliases:
- "/article/777/"
- "/ilovefs2021/"
- "/node/777/"
---
Todos os anos a <a href="https://fsfe.org/activities/ilovefs/"
title="https://fsfe.org/activities/ilovefs/" rel="noopener noreferrer
nofollow">14 de Fevereiro</a>, a ANSOL junta-se à Free Software Foundation
Europe e pede a todos os utilizadores de Software Livre que pensem naquelas
pessoas trabalhadoras na comunidade de Software Livre e mostrem o seu apreço
individualmente neste dia de "Eu adoro Software Livre".

Empresas, comunidades, projectos, associações e vários outros organismos têm
usado este dia para mostrar o seu agradecimento público àqueles que fazem o
mundo do Software Livre tão vibrante como é. Assim, também a ANSOL decide
aproveitar este dia para fazer alguns agradecimentos públicos.

Manter uma associação implica a necessidade de um conjunto de recursos
técnicos, mais ou menos elaborados. O uso de várias peças de tecnologia por
parte da ANSOL tem variado e evoluído ao longo dos tempos, e no último ano
implementámos algumas novas soluções que nos ajudam diariamente:

* <a href="https://www.phacility.com/phabricator/"
  title="https://www.phacility.com/phabricator/" rel="noopener noreferrer
  nofollow">Phabricator</a> - a ANSOL tem agora a sua própria <a
  href="https://git.ansol.org/" title="https://git.ansol.org/" rel="noopener
  noreferrer nofollow">instância de phabricator</a>, que conta usar para muitos
  outros fins, mas, para já, já está a usar para o alojamento de vários
  projectos git;
* <a href="https://nextcloud.com/" title="https://nextcloud.com/" rel="noopener
  noreferrer nofollow">Nextcloud</a> - a coordenação da associação envolve
  trabalho conjunto, e o nextcloud está a provar ser uma ferramenta
  imprescindível para o nosso dia a dia. Em particular, usamos extensivamente
  as funcionalidades de gestão e edição online e colaborativa de documentos,
  tal como o Deck, onde organizamos e distribuímos as nossas tarefas;
* <a href="https://minikomi.github.io/Bootstrap-Form-Builder/"
  title="https://minikomi.github.io/Bootstrap-Form-Builder/" rel="noopener
  noreferrer nofollow">Bootstrap Form Builder</a> - o nosso formulário de
  inscrição de novos sócios foi criado com este software que veio melhorar e
  anular algumas pequenas incongruências do passado, facilitando o processo de
  recepção dos novos membros da ANSOL;
* <a href="https://freescout.net" title="https://freescout.net" rel="noopener
  noreferrer nofollow">Freescout</a> - as caixa de correio electrónico
  partilhadas por vários utilizadores passaram a ser geridas pelo freescout,
  permitindo que a atribuição, respostas e acompanhamento das mensagens que nos
  chegam possam ter uma gestão mais lógica e ordenada.

Como resposta à necessidade de adaptação que o mundo exigiu graças os momento pandémico que ainda atravessamos, recomendámos também um conjunto de software que visa dar resposta às necessidades da comunidade sejam alunos, professores ou tele-trabalhadores no geral:

<a href="https://adistancia.ansol.org/" title="https://adistancia.ansol.org/" rel="noopener noreferrer nofollow">https://adistancia.ansol.org/</a>

Estes são apenas alguns exemplos do software que utilizamos e/ou recomendamos,
e a quem dirigimos o nosso muito grande <strong>OBRIGADO</strong>! Mas queremos
também agradecer a toda a comunidade de Software Livre no geral, e lançar-vos o
desafio: que tal aproveitarem o dia de hoje para, também vocês, agradecerem
àqueles que fazem o Software Livre que tanto adoram?
