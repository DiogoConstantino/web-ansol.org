---
categories:
- imprensa
- '2008'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 19
  - tags_tid: 27
  node_id: 154
layout: page
title: Presença na Imprensa de 2008
created: 1209383804
date: 2008-04-28
aliases:
- "/imprensa/2008/"
- "/node/154/"
- "/page/154/"
---
<ul><li><p class="line862">2008/02/26 -&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/ansol_e_esop_desconfiam_da_proposta_de_intero_873014.html" class="http">Ansol e ESOP desconfiam da proposta de interoperabilidade da Microsoft</a></p></li><li><p class="line862">2008/03/20 - DFD2008 -&nbsp;<a href="http://sol.sapo.pt/PaginaInicial/Tecnologia/Interior.aspx?content_id=85820" class="http">SOL: Associação de open source comemora Dia dos Documentos Livres</a></p></li><li><p class="line862">2008/03/20 - DFD2008 -&nbsp;<a href="http://sol.sapo.pt/PaginaInicial/Sociedade/Interior.aspx?content_id=85916" class="http">SOL: Microsoft afirma que declarações da Associação de Software Livre «não correspondem à verdade»</a></p></li><li>2008/03/25 - Software Livre para Empresas - Entrevista para a Rádio Renascença</li><li>2008/03/25 - DFD2008 - Entrevista para TSF</li><li><p class="line862">2008/03/25 - DFD2008 -&nbsp;<a href="http://tek.sapo.pt/extras/site_do_dia/liberdade_para_os_documentos_887136.html" class="http">Liberdade para os documentos!</a>&nbsp;(Casa dos Bits)</p></li><li><p class="line862">2008/03/26 - DFD2008 -&nbsp;<a href="http://sol.sapo.pt/PaginaInicial/Tecnologia/Interior.aspx?content_id=86363" class="http">SOL: Software no Parlamento - ANSOL acusa Microsoft de «falta de argumentos»</a></p></li><li><p class="line862">2008/03/26 - DFD2008 -&nbsp;<a href="http://sol.sapo.pt/PaginaInicial/Tecnologia/Interior.aspx?content_id=86359" class="http">SOL: ANSOL - Documentos digitais podem acabar como os escritos egípcios</a></p></li><li><p class="line862">2008/04/01 -&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/ooxml_aprovado_como_norma_iso_873053.html" class="http">OOXML aprovado como norma ISO</a></p></li><li><p class="line862">2008/04/02 -&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/microsoft_portugal_considera_aprovacao_de_ope_873056.html" class="http">Microsoft Portugal considera aprovação de Open XML positiva para a indústria</a></p></li><li><p class="line862">2008/04/02 -&nbsp;<a href="http://tek.sapo.pt/noticias/computadores/ffii_acusa_iso_de_favorecer_microsoft_com_a_n_873057.html" class="http">FFII acusa ISO de favorecer Microsoft com a normalização do OOXML</a></p></li><li><p class="line862">2008/12/30 -&nbsp;<a href="http://tek.sapo.pt/opiniao/2008_ano_visto_e_revisto_pelas_associacoes_do_906218.html" class="http">2008: Ano visto e revisto pelas associações do sector</a></p></li><li><p class="line862">2008/12/30 -&nbsp;<a href="http://tek.sapo.pt/opiniao/revista_do_ano_2008_ansol_um_ano_cheio_de_act_906211.html" class="http">Revista do Ano 2008: ANSOL - "Um ano cheio de actividade em volta do Software Livre"</a></p></li></ul>
