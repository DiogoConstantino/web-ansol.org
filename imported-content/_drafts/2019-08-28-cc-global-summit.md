---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://creativecommons.org/2019/08/28/cc-global-summit-lisbon-may-14-16/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-05-13 23:00:00.000000000 +01:00
    event_start_value2: 2020-05-15 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 692
layout: evento
title: CC Global Summit
created: 1567028990
date: 2019-08-28
---
<p>Cancelada por causa do Coronavirus.</p>
