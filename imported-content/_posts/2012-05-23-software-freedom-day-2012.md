---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa e Aveiro
  event_site:
  - event_site_url: http://wiki.softwarefreedomday.org/2012/Portugal/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-09-15 16:00:00.000000000 +01:00
    event_start_value2: 2012-09-15 16:00:00.000000000 +01:00
  node_id: 71
layout: evento
title: Software Freedom Day 2012
created: 1337792118
date: 2012-05-23
---
<p><a href="http://www.softwarefreedomday.org/pt/"><img alt="Celebra o SFD 2012 no Sábado, 15 
de Setembro!" border="0" height="90" src="http://www.softwarefreedomday.org/countdown/banner1-UTC+0-pt.png" width="160" /></a></p>
<p>O Software Freedom Day 2012 vai ser celebrado em Lisboa e em Aveiro, no s&aacute;bado dia 15 a partir das 17 horas.</p>
<p>O evento em Lisboa come&ccedil;ar&aacute; na esplanada no bar-restaurante Rep&uacute;blica da Cerveja, &agrave; beira rio, no Parque das Na&ccedil;&otilde;es. Ser&aacute; um encontro conv&iacute;vio a decorrer durante a tarde e culminando num jantar no mesmo local. Iremos ter surpresas para oferecer aos presentes, boa conversa e anima&ccedil;&atilde;o.</p>
<p>O evento em Aveiro &eacute; tamb&eacute;m a partir das 17 horas no jardim &aacute; frente da C&acirc;mara Municipal de Aveiro, ao lado da est&aacute;tua do Jos&eacute; Estev&atilde;o. Mais tarde jantar-se-&agrave; num local perto, seguindo-se um momento de conv&iacute;vio.</p>
