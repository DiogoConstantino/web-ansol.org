---
categories: []
metadata:
  node_id: 4
layout: page
title: Documentos
created: 1332700260
date: 2012-03-25
---
<ul>
	<li>
		Imagens</li>
	<li>
		Manual do utilizador/administrador de ANSOL</li>
	<li>
		Relat&oacute;rio de actividades da lista fsfe-portugal</li>
	<li>
		Acta da 1&ordf; reuni&atilde;o dos membros da lista de correio FSFE-Portugal</li>
	<li>
		Estatutos da Associa&ccedil;&atilde;o Nacional para o Software Livre</li>
	<li>
		Lan&ccedil;amento p&uacute;blico de ANSOL</li>
	<li>
		Acta da 2&ordf; reuni&atilde;o dos membros fundadores de ANSOL</li>
	<li>
		Apresenta&ccedil;&atilde;o da ANSOL</li>
	<li>
		Acta da reuni&atilde;o de apresenta&ccedil;&atilde;o da ANSOL</li>
	<li>
		Plano de ac&ccedil;&atilde;o para 2002</li>
	<li>
		Acta da 1&ordf; Assembleia Geral da ANSOL</li>
	<li>
		Relat&oacute;rio e Parecer do Conselho Fiscal - 2002</li>
	<li>
		Regulamento Interno da ANSOL</li>
	<li>
		Plano de ac&ccedil;&atilde;o para 2003</li>
	<li>
		Acta da 2&ordf; Assembleia Geral da ANSOL</li>
	<li>
		Recursos para projectos de Software Livre</li>
	<li>
		Lista de recursos dispon&iacute;veis para qualquer projecto de software livre.<br />
		&nbsp;</li>
</ul>
