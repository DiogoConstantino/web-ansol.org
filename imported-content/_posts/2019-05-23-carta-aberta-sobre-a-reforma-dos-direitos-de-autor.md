---
categories:
- direitos de autor
- direito de autor
- direitos digitais
metadata:
  tags:
  - tags_tid: 96
  - tags_tid: 43
  - tags_tid: 113
  node_id: 677
layout: article
title: Carta Aberta sobre a Reforma dos Direitos de Autor
created: 1558650091
date: 2019-05-23
---
<div style="margin: 0px;"><img src="https://liberties.imgix.net/images/9b039996-5a24-41e9-8e4e-da96ae1a030c/copyright_eu.png_effected.png?ixlib=rails-0.3.2&amp;auto=format&amp;fit=crop&amp;fm=jpg&amp;h=400&amp;lossless=true&amp;q=60&amp;trim=auto&amp;w=1050&amp;s=a1a9b7b9fb4f9e732ef11aeb048c5b88" alt="Copyright na Europa" style="display: block; margin-left: auto; margin-right: auto;" width="1050" height="400"></div><div style="margin: 0px;"><h3 style="margin: 0px;">&nbsp;</h3><h3 style="margin: 0px;">Carta Aberta à Comissão Europeia: Grupo de Trabalho sobre Diretiva do Direito de Autor deve incluir Organizações de Direitos Fundamentais</h3><p>&nbsp;</p></div><div style="margin: 0px;">A Associação Nacional para o Software Livre (ANSOL) e mais de 40 organizações não governamentais enviaram esta semana uma carta à Comissão Europeia lembrando que o Grupo de Trabalho que irá emitir orientações sobre a aplicação e transposição do Artigo 17 (antigo Artigo 13) deverá incluir "organizações de utilizadores e outras partes interessadas pertinentes", como estipulado no ponto 10 daquele artigo.</div><div style="margin: 0px;">Os signatários incluem organizações de direitos fundamentais; de comunidades de conhecimento (bibliotecas, em particular); associações de comunidades e desenvolvedores de software de código aberto e software livre; entre outras, que, em conjunto com outras partes interessadas, como a comunidade científica e académica, vários eurodeputados, e alguns Estados-Membros, expressaram, durante todo o processo legislativo, as suas preocupações no que respeita às questões sobre direitos humanos e fundamentais que se colocam na implementação do artigo 17.</div><div style="margin: 0px;">Tal participação alargada e inclusiva é crucial para garantir que as implementações nacionais do Artigo 17 e a cooperação regular entre plataformas e titulares dos direitos respeita a Carta dos Direitos Fundamentais, salvaguardando a liberdade de expressão e de informação de cidadãos e criadores, enquanto protege a sua privacidade. Estes são os princípios que devem guiar uma transposição harmonizada do Artigo 17 no Mercado Único Digital.</div><div style="margin: 0px;">&nbsp;</div><div style="margin: 0px;">A carta pode ser lida, na íntegra, <a href="https://dq4n3btxmr8c9.cloudfront.net/files/zhGEHk/copyright_Open_letter.pdf">aqui</a>.</div>
