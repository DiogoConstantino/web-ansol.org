---
categories: []
metadata:
  event_location:
  - event_location_value: São Mamede de Infesta
  event_start:
  - event_start_value: 2017-08-15 11:00:00.000000000 +01:00
    event_start_value2: 2017-08-15 13:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 519
layout: evento
title: Almoço-convívio para sócios da ANSOL
created: 1502479543
date: 2017-08-11
---
<p>Precedendo a <a href="https://ansol.org/node/516">Assembleia Geral</a>, a ANSOL está a promover um almoço de convívio para os seus associados.</p><p>Inscrições devem ser feitas via e-mail para a lista de sócios.</p><p>&nbsp;</p><p>Mais informações sobre este evento podem ser encontradas também nessa lista.</p>
