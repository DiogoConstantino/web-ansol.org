---
categories: []
metadata:
  event_location:
  - event_location_value: LCD, Guimarães
  event_start:
  - event_start_value: 2012-12-09 00:00:00.000000000 +00:00
    event_start_value2: 2012-12-09 00:00:00.000000000 +00:00
  node_id: 110
layout: evento
title: Open Data Hackaton
created: 1354045989
date: 2012-11-27
---
<p>&nbsp;</p>
<p>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
	No dia 9 de Dezembro de 2012, realiza-se no Laborat&oacute;rio de Cria&ccedil;&atilde;o Digital em Guimar&atilde;es um evento aberto ao publico com o objectivo de dinamizar o movimento de Open Data em Portugal.<br />
	<br />
	O evento inicia &agrave;s 10 da manh&atilde;, nas instala&ccedil;&otilde;es do LCD no Centro para os Assuntos da Arte e Arquitectura (CAAA), junto ao mercado municipal. Da parte da manh&atilde; ser&atilde;o realizadas algumas apresenta&ccedil;&otilde;es sobre o estado da Open Data em Portugal e movimentos civicos relacionados. Convidam-se os presentes que estejam envolvidos em projectos desta natureza a preparar apresenta&ccedil;&otilde;es de 5-10 minutos para apresenta&ccedil;&atilde;o dos seus projectos.<br />
	<br />
	Segue-se um almo&ccedil;o convivio para discutir informalmente quest&otilde;es relacionadas ao desenvolvimento do Open Data em Portugal.<br />
	<br />
	Da parte da tarde retornam as actividades ao LCD Guimar&atilde;es para sess&atilde;o de desenvolvimento em conjunto (o chamado hackaton) nos diversos projectos presentes at&eacute; &agrave;s 18:00 da tarde. Convidam-se os presentes a conhecer melhor os diversos projectos em desenvolvimento e descobrir formas de colabora&ccedil;&atilde;o e entre-ajuda.</p>
