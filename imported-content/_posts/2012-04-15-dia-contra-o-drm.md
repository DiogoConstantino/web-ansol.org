---
categories: []
metadata:
  event_location:
  - event_location_value: Planeta Terra
  event_site:
  - event_site_url: http://drm-pt.info/moin/DayAgainstDRM
    event_site_title: http://drm-pt.info/moin/DayAgainstDRM
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-05-03 23:00:00.000000000 +01:00
    event_start_value2: 2012-05-03 23:00:00.000000000 +01:00
  node_id: 55
layout: evento
title: Dia contra o DRM
created: 1334523363
date: 2012-04-15
---
<p>&Eacute; j&aacute; no pr&oacute;ximo dia <strong>4 de Maio de 2012</strong> que se celebra o <strong>Dia Contra o DRM</strong>, uma campanha mundial do <a href="http://libreplanet.org/wiki/Group:DefectiveByDesign">grupo Defective by Design</a> da FSF.</p>
<p>As medidas tecnol&oacute;gicas de protec&ccedil;&atilde;o de direitos digitais (DRM &ndash; Digital Right Management) subvertem a Lei do Direito de Autor, encapsulando os objectos fisicos que guardam os nossos conte&uacute;dos culturais em inv&oacute;lucros legalmente inviol&aacute;veis e congelados no tempo, impedindo-os de &ldquo;expirar&rdquo; naturalmente e reverterem ao dom&iacute;nio p&uacute;blico, onde deveriam ir fertilizar a nossa cultura atrav&eacute;s do seu re-uso e transforma&ccedil;&atilde;o. Todos os anos se celebra o &quot;Dia Contra o DRM&quot;, com ac&ccedil;&otilde;es de sensibiliza&ccedil;&atilde;o sobre esta tem&aacute;tica.</p>
<p>As prepara&ccedil;&otilde;es iniciais para este dia est&atilde;o a ser feitas em <a href="http://libreplanet.org/wiki/Group:DefectiveByDesign/Day_Against_DRM_2012">http://libreplanet.org/wiki/Group:DefectiveByDesign/Day_Against_DRM_2012</a></p>
