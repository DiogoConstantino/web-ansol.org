---
categories: []
metadata:
  event_location:
  - event_location_value: Braga, Portugal
  event_site:
  - event_site_url: http://openaccess.sdum.uminho.pt/?page_id=1229
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-02-06 00:00:00.000000000 +00:00
    event_start_value2: 2013-02-08 00:00:00.000000000 +00:00
  node_id: 126
layout: evento
title: UMinho Open Access Seminar
created: 1358337186
date: 2013-01-16
---
<p>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
</p>
<p style="margin-top: 0px; margin-bottom: 15px; ">The University of Minho, in conjunction with&nbsp;<strong>OpenAIRE&nbsp;</strong>Project and&nbsp;<strong>MedOANet</strong>&nbsp;Project, is pleased to announce the UMinho Open Access Seminar. The event will take place at the University of Minho, Braga, from 6 to 8 February 2013.</p>
<p style="margin-top: 0px; margin-bottom: 15px; ">Taking advantage of the opportunity that OA international experts working on both the OpenAIRE and MedOANet projects are gathering at the same place, UMinho is organizing an event which will include not only meetings of each project, but also public sessions on open access policies, open science, data repositories, interoperability and research infrastructures, that will bring together the communities from both projects and will be open to all interested in these issues.</p>
