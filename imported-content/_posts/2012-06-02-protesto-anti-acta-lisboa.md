---
categories:
- acta
- manifestação
metadata:
  event_location:
  - event_location_value: Em frente à Brasileira - Chiado - Lisboa
  event_site:
  - event_site_url: http://wiki.ansol.org/ACTA
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-06-09 14:00:00.000000000 +01:00
    event_start_value2: 2012-06-09 14:00:00.000000000 +01:00
  tags:
  - tags_tid: 12
  - tags_tid: 13
  node_id: 77
layout: evento
title: Protesto Anti-ACTA - Lisboa
created: 1338638356
date: 2012-06-02
---
<p>A ANSOL juntou-se ao TugaLeaks para organizar um protesto anti-<a href="https://ansol.org/acta">ACTA</a> em Lisboa.</p>
<p>Juntamo-nos assim aos j&aacute; mais de 100 protestos marcados um pouco por toda a Europa e que ir&atilde;o decorrer ao longo do dia 9 de Junho de 2012.</p>
<p>Este evento ir&aacute; decorrer no dia 9 &agrave;s 15:00 no Chiado, &agrave; frente do caf&eacute; Brasileira.</p>
<p>&nbsp;</p>
<p>(veja tamb&eacute;m informa&ccedil;&atilde;o sobre o <a href="https://ansol.org/node/78">protesto em Coimbra</a>)</p>
