---
categories:
- "#pl118"
- "#pl246"
metadata:
  event_location:
  - event_location_value: RTP1
  event_site:
  - event_site_url: http://www.rtp.pt/programa/tv/p30738/e17
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-09-15 21:30:00.000000000 +01:00
    event_start_value2: 2014-09-15 23:45:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 44
  - tags_tid: 45
  node_id: 217
layout: evento
title: Prós e Contras sobre Cópia Privada
created: 1410460008
date: 2014-09-11
---
<p><span class="userContent" data-ft="{&quot;tn&quot;:&quot;K&quot;}">A próxima sessão do "Prós e Contras", programa informativo da RTP1, terá o título "O Imposto da Discórdia", falando sobre o novo Projecto-Lei sobre a Cópia Privada.</span></p><p><cite>A taxa da cópia privada.<br></cite></p><p><cite><span class="userContent" data-ft="{&quot;tn&quot;:&quot;K&quot;}"> Telemóveis, tablets, computadores, equipamentos electrónicos, mais caros.<br> De um lado, o Governo em defesa do direito dos autores.<br> Do outro, os protestos da indústria, comércio e consumidores.<br> Governo, autores, empresários e consumidores, frente a frente no maior debate da televisão portuguesa.<br> O Imposto da Discórdia no regresso do Prós e Contras, 2a feira à noite na RTP1.</span></cite></p><p><span class="userContent" data-ft="{&quot;tn&quot;:&quot;K&quot;}">A ANSOL particpará neste programa e aproveita para relembrá-lo que é contra este Projecto-Lei. Leia mais sobre a posição da ANSOL <a href="https://ansol.org/politica/copiaprivada">aqui</a>.</span></p>
