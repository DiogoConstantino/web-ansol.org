---
categories:
- press release
- cópia privada
- petição
- imprensa
metadata:
  tags:
  - tags_tid: 9
  - tags_tid: 11
  - tags_tid: 46
  - tags_tid: 19
  node_id: 220
layout: article
title: Petição contra novas taxas de cópia privada
created: 1410805494
date: 2014-09-15
---
<p>Lisboa, 15 de Setembro de 2014 - A ANSOL (Associação Nacional para o Software Livre) opõe-se à proposta de lei 246/XII e lança petição contra o projeto no Dia Internacional da Democracia em desafio ao processo de portas fechadas onde o Governo negociou em consenso com os beneficiários das taxas a nova proposta.</p><p>«<em>O #PL246 é uma afronta à cidadania, faz parecer que o Governo só tem ouvidos para quem quer extorquir taxas, desde que leve o seu quinhão no IVA</em>» diz Rui Seabra, presidente da Direção da ANSOL lamentando que já no ano passado <a href="https://c.ansol.org/node/28">a associação tinha solicitado à Secretaria de Estado da Cultura</a>, sem reposta apesar da obrigatoriedade legal e ao abrigo da Lei do Acesso aos Documentos Administrativos, informação sobre a proposta de lei que estava a elaborar.</p><p>Em http://bit.do/pl246 está disponível uma petição que cumpre com os requisitos legais para apresentação às entidades oficiais e que apela a que a proposta não seja aprovada, citando entre outros motivos a ausência de prejuízo para os autores, uma das conclusões do <a href="http://ec.europa.eu/internal_market/copyright/docs/studies/140623-limitations-economic-impacts-study_en.pdf">estudo da Comissão Europeia sobre a matéria</a> que concluiu também haver maiores efeitos positivos para todas as partes acabando com as taxas sobre a cópia privada.</p><p>«<em>Inacreditável: quando um projeto de lei equilibraria a lei para os cidadãos o Governo pede que a maioria o chumbe alegando necessidade de discussão alargada, mas quando é para beneficiar entidades intermediárias obsoletas já não há problema nenhum em legislar ad-hoc</em>» acrescenta Rui Seabra referindo-se a um projeto de lei que resolve problemas na implementação da proteção legal do DRM que prejudicam o acessos dos portugueses às obras assim restringidas.</p><p>«<em>Apelamos a todos os portugueses que evitem mais taxas para beneficiar entidades obsoletas</em>», conclui.</p><p>Para mais informações, consultar <a href="https://c.ansol.org/pl246">https://c.ansol.org/pl246 </a>ou a página de <a href="https://ansol.org/contato">contactos</a>.</p>
