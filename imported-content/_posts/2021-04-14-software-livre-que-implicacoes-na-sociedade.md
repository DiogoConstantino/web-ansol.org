---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://sites.google.com/anpri.pt/mind-bytes-week/dia-15?authuser=0#h.rkbf9sxge74d
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-04-15 10:30:00.000000000 +01:00
    event_start_value2: 2021-04-15 10:30:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 785
layout: evento
title: Software livre! que implicações na sociedade
created: 1618409722
date: 2021-04-14
---
<p>Inserido no programa da "<a href="https://sites.google.com/anpri.pt/mind-bytes-week/página-inicial?authuser=0" class="GAuSPc">Mind &amp; Bytes Week</a>", Tiago Carrondo, presidente da ANSOL - Associação Nacional para o Software Livre, fez uma palestra entitulada "Software livre! que implicações na sociedade", cujo vídeo agora se encontra disponível.<br><img src="https://ansol.org/sites/ansol.org/files/ANSOL-ANPRI.png" alt="Imagem do evento" style="display: block; margin-left: auto; margin-right: auto;" width="1212" height="589"></p>
