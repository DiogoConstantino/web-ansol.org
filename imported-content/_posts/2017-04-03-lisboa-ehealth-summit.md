---
categories: []
metadata:
  event_location:
  - event_location_value: SALA TEJO | MEO ARENA, Lisboa
  event_site:
  - event_site_url: http://ehealthsummit.pt/
    event_site_title: Portugal Health Summit
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-04-04 07:30:00.000000000 +01:00
    event_start_value2: 2017-04-06 16:30:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 495
layout: evento
title: Lisboa eHealth Summit
created: 1491236816
date: 2017-04-03
---
<div class="moz-text-flowed" lang="x-unicode">A cidade de Lisboa recebe, de 4 a 6 de abril, a 1ª edição do Portugal eHealth Summit, o maior evento internacional de empreendedorismo, inovação e tecnologia na área da transformação digital da saúde. <br>A iniciativa irá acolher reputados oradores internacionais, profissionais, investidores, algumas das mais promissoras startups a nível nacional, académicos entre outros <br>A Federação Académica de Lisboa, enquanto entidade promotora da formação e espírito empreendedor da comunidade estudantil, é parceira da iniciativa, promovida pelos Serviços Partilhados do Ministério de Saúde, na qual durante três dias se pretende envolver a Academia na discussão da inovação em saúde. <br> <br>A inscrição poderá ser realizada a partir do site oficial do evento <a href="http://ehealthsummit.pt/" class="moz-txt-link-freetext">http://ehealthsummit.pt/</a>, de forma gratuita e certificada, caso pretendido. <br> </div>
