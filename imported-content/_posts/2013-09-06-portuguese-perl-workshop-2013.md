---
categories:
- '2013'
metadata:
  event_location:
  - event_location_value: Av. Fontes Pereira de Melo, nº 40 1069-300 Lisboa
  event_site:
  - event_site_url: http://workshop.perl.pt/ptpw2013/index.html
    event_site_title: PTPW2013
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-10-08 23:00:00.000000000 +01:00
    event_start_value2: 2013-10-09 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 32
  node_id: 206
layout: evento
title: Portuguese Perl Workshop 2013
created: 1378454012
date: 2013-09-06
---
<p>A edição de 2013 da Portuguese Perl Workshop vai se realizar na sala A10 na sede da Portugal Telecom na Av. Fontes Pereira de Melo (mais detalhes <a href="http://www.telecom.pt/InternetResource/PTSite/PT/Canais/Investidores/Contactos/Sede/sedept.htm">aqui</a>), em Lisboa, Portugal, nos dias 9 e 10 de outubro.</p><p>O evento de dois deste ano vai se dividir entre:</p><ul><li>Sessão de formação de Dancer2, com <a href="http://sukria.net/">Alexis Sukrieh</a></li><li>Tutoriais sobre AnyEvent e Coro GreenThreads, com <a href="http://www.simplicidade.org/notes/">Pedro Melo</a></li><li>Sessão de apresentações: TBA</li><li>.. e muito mais!</li></ul><p>A participação na workshop é gratuita para membros da <a href="http://www.perl.pt/">APPP</a> e terá um custo de €20 pera os restantes participantes. Para mais informações de como se pode fazer sócio da APPP consultar <a href="http://perl.pt/faq.html">esta página</a>. A formação sobre Dancer2 a realizar no dia 9, tem um custo de €65 pera membros da APPP e €70 para os restantes participantes.</p><p>Mais detalhes sobre o processo de registo <a href="http://workshop.perl.pt/ptpw2013/registration.html">aqui</a>.</p>
