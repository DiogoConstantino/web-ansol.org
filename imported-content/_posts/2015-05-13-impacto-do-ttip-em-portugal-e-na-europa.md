---
categories:
- ttip
metadata:
  event_site:
  - event_site_url: https://www.nao-ao-ttip.pt/debate-dia-14-de-maio-fcsh-lisboa/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-05-14 15:00:00.000000000 +01:00
    event_start_value2: 2015-05-14 15:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 321
layout: evento
title: Impacto do TTIP em Portugal e na Europa
created: 1431513918
date: 2015-05-13
---

