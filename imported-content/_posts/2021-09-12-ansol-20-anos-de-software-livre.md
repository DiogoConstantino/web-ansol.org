---
categories:
- 20 anos
- aniversário
- ansol
- software livre
- evento
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://ansol.org/20anos
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-10-09 14:00:00.000000000 +01:00
    event_start_value2: 2021-10-09 17:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 340
  - tags_tid: 341
  - tags_tid: 33
  - tags_tid: 41
  - tags_tid: 84
  node_id: 816
layout: evento
title: ANSOL - 20 anos de Software Livre
created: 1631485432
date: 2021-09-12
---
<p>A ANSOL faz 20 anos, e nada melhor que uma festa de aniversário para os celebrar!</p><p>Neste evento <em>online</em>, iremos falar sobre o que foram estes 20 anos de Software Livre, e espreitar para aquilo que nos trarão os próximos 20.</p><p>Junta-te a nós!</p><p>&nbsp;</p><p>Mais informações em breve.</p>
