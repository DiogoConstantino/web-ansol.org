---
categories: []
metadata:
  event_start:
  - event_start_value: 2016-04-06 16:30:00.000000000 +01:00
    event_start_value2: 2016-04-06 16:30:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 410
layout: evento
title: Reunião Aberta sobre as opções políticas e legislativas para a partilha de
  dados
created: 1459769085
date: 2016-04-04
---
<p>A ANSOL irá estar presente na "Reunião Aberta sobre as opções políticas e legislativas para a partilha de dados", a decorrer na Assembleia da República.</p>
