---
categories: []
metadata:
  event_location:
  - event_location_value: Porto
  event_site:
  - event_site_url: https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-prn2/992788_664348756925454_1263225931_n.jpg
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-06-07 17:00:00.000000000 +01:00
    event_start_value2: 2013-06-07 17:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 172
layout: evento
title: Copiar é ser pirata? (Porto)
created: 1370352275
date: 2013-06-04
---
<p>Debate sobre DRM's e a privativação da Cultura.</p>
