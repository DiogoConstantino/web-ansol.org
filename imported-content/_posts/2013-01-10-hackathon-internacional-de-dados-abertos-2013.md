---
categories: []
metadata:
  event_location:
  - event_location_value: Porto
  event_site:
  - event_site_url: http://opendataday.org/index_pt.html
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-02-23 00:00:00.000000000 +00:00
    event_start_value2: 2013-02-23 00:00:00.000000000 +00:00
  node_id: 121
layout: evento
title: Hackathon Internacional de Dados Abertos 2013
created: 1357820630
date: 2013-01-10
---
Este evento é um ajuntamento de cidadãos em cidades de todo o mundo para escrever aplicações, libertar dados, criar visualizações e publicar análises usando dados abertos públicos para dar apoio e encorajar a adoção de políticas de libertação de dados por instituições, governos locais, regionais e nacionais em todo o mundo.
