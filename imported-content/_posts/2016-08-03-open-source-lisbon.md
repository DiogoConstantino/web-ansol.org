---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: http://www.opensourcelisbon.com/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-09-28 23:00:00.000000000 +01:00
    event_start_value2: 2016-09-28 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 441
layout: evento
title: Open Source Lisbon
created: 1470229747
date: 2016-08-03
---
<p class="ProfileHeaderCard-bio u-dir" dir="ltr">Open Source Lisbon is a one day event for business professionals &amp; tech enthusiasts, with interest in everything related to Open Source.</p>
