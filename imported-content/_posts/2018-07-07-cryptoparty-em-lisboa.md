---
categories: []
metadata:
  event_location:
  - event_location_value: "      Biblioteca dos Coruchéus Rua Alberto Oliveira 1700-019
      Lisboa (Freguesia de Alvalade)"
  event_site:
  - event_site_url: http://privacylx.org/events/cryptoparty/
    event_site_title: http://privacylx.org/events/cryptoparty/
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-07-14 14:00:00.000000000 +01:00
    event_start_value2: 2018-07-14 17:30:00.000000000 +01:00
  mapa:
  - mapa_geom: !binary |-
      AQEAAABDVABrxkkiwBJb04AeYENA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38750930884553e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.9144091934004e1
    mapa_left: !ruby/object:BigDecimal 27:-0.9144091934004e1
    mapa_top: !ruby/object:BigDecimal 27:0.38750930884553e2
    mapa_right: !ruby/object:BigDecimal 27:-0.9144091934004e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38750930884553e2
    mapa_geohash: eyckrymkx1nfexp3
  slide:
  - slide_value: 0
  node_id: 625
layout: evento
title: Cryptoparty em Lisboa
created: 1530988440
date: 2018-07-07
---
<p>Cryptoparties são workshops de formato aberto com o objetivo de proporcionar a troca de conhecimentos sobre como podemos proteger a privacidade no mundo digital.</p><p>Serão abordados tópicos como: * Tor / browsers * Segurança de comunicações * Extensões para browsers que aumentam a privacidade * motores de busca * encriptação de e-mail * etc…</p><p>A entrada é livre!</p>
