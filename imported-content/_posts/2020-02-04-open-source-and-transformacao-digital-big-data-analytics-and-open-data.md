---
categories: []
metadata:
  event_location:
  - event_location_value: LNEC - Laboratório Nacional de Engenharia Civil, Lisboa
  event_site:
  - event_site_url: https://www.eventbrite.com/e/open-source-transformacao-digital-big-data-analytics-open-data-tickets-91325778863
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-02-20 00:00:00.000000000 +00:00
    event_start_value2: 2020-02-20 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 730
layout: evento
title: 'Open Source & Transformação Digital: Big Data Analytics & Open Data'
created: 1580808067
date: 2020-02-04
---

