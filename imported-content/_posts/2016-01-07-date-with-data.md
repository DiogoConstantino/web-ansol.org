---
categories: []
metadata:
  event_location:
  - event_location_value: Pavilhão Jardim do UPTEC PINC, Porto
  event_site:
  - event_site_url: http://datewithdata.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-01-09 10:00:00.000000000 +00:00
    event_start_value2: 2016-01-09 17:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 386
layout: evento
title: Date With Data
created: 1452180356
date: 2016-01-07
---
<div><span>Abrimos 2016 com um Date With Data dedicado aos datasets!</span></div><div>&nbsp;</div><div><span>Vamos aproveitar o primeiro encontro deste ano para arrumar a casa -- publicar dados novos que recolhemos, atualizar os que temos online, fazer o balanço dos projetos feitos e pensar em caminhos novos.</span><span> Vai ser um pretexto perfeito para vir aos encontros Date With Data pela primeira vez e saber o que por cá se cozinha!</span></div><div>&nbsp;</div><div><span>Como se publicam e utilizam datasets abertos? Vamos meter as mãos na massa com um tutorial sobre <a href="http://transparenciahackday.us6.list-manage.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=49651314cb&amp;e=b684bb14e9" target="_blank" style="color: #2baadf; font-weight: normal; text-decoration: underline;">Data Packages</a> </span><span>e sobre a <a href="http://transparenciahackday.us6.list-manage.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=b59a3537dc&amp;e=b684bb14e9" target="_blank" style="color: #2baadf; font-weight: normal; text-decoration: underline;">Central de Dados</a></span><span>, o nosso repositório livre de informação pública em formatos livres e fáceis de usar. Vamos também aproveitar o pretexto para criar a documentação necessária para incentivar à participação de mais pessoas.<br> <br> Como de costume, toda a gente é bem vinda, seja para bater código, pensar novos projetos, testar e dar feedback sobre as ferramentas que vamos fazendo -- há muitas tarefas de todo o tipo à espera de alguma atenção! Por isso, tenhas ou não background técnico, precisamos de ti!</span></div><p style="margin: 10px 0; padding: 0; color: #202020; font-family: Helvetica; font-size: 16px; line-height: 150%; text-align: left;">Quase sempre ao segundo sábado de cada mês, é já no dia 9, das 10 às 17, no <a href="http://transparenciahackday.us6.list-manage1.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=8ce04be9c2&amp;e=b684bb14e9" target="_blank" style="color: #2baadf; font-weight: normal; text-decoration: underline;">Pavilhão Jardim do UPTEC PINC</a>. Mais pormenores <a href="http://transparenciahackday.us6.list-manage1.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=8e26b3b576&amp;e=b684bb14e9" target="_blank" style="color: #2baadf; font-weight: normal; text-decoration: underline;">aqui</a>. Traz o teu portátil!</p>
