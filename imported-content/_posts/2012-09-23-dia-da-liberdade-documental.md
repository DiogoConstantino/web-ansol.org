---
excerpt: "<p class=\"line862\">O pr&oacute;ximo Dia da Liberdade Documental (Document
  Freedom Day) vai ser celebrado pela ANSOL nos <strong>pr&oacute;ximos dias 24 e
  27 de Mar&ccedil;o</strong> no Espa&ccedil;o J (em ante-estreia do mesmo), da Junta
  de Freguesia de Benfica, em Lisboa, sito no Jardim do Eucaliptal na Avenida dos
  Arneiros em Benfica e no Audit&oacute;rio Carlos Paredes, na sede da Junta sita
  na Rua Gomes Pereira, N&deg;17. As celebra&ccedil;&otilde;es deste dia, em 2013,
  v&atilde;o ser espalhadas pelo mundo, e podem decorrer entre os dias <em>21 de Mar&ccedil;o</em>
  e <em>2 de Abril</em>. O evento vai incluir uma <a href=\"http://membros.ansol.org/civicrm/event/info?id=4\">hackfest</a>,
  palestras e concertos gratuitos. Inscreva-se j&aacute; para a <a href=\"http://membros.ansol.org/civicrm/event/info?id=4\">hackfest</a>
  e para o <a href=\"http://membros.ansol.org/civicrm/event/register?reset=1&amp;id=5\">resto
  do evento</a> tamb&eacute;m, embora a inscri&ccedil;&atilde;o s&oacute; seja obrigat&oacute;ria
  para a hackfest.</p>\r\n"
categories: []
metadata:
  event_location:
  - event_location_value: Hackfest e Palestras no Espaço J, Jardim da Estrada dos
      Arneiros, Benfica, Lisboa; Concertos no Auditório Carlos Paredes, Junta de Freguesia
      de Benfica, Avenida Gomes Pereira N°17 Lisboa
  event_site:
  - event_site_url: http://documentfreedom.org/index.pt.html
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-03-24 00:00:00.000000000 +00:00
    event_start_value2: 2013-03-27 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 90
layout: evento
title: Dia da Liberdade Documental
created: 1348434643
date: 2012-09-23
---
<p class="line862"><img src="/sites/ansol.org/files/pictures/dfd-2013-a7-front-en.png" alt="dfd-2013-a7-front-en.png" style="width: 45%; float: right;">O próximo Dia da Liberdade Documental (Document Freedom Day) vai ocorrer no próximo dia 27 de Março de 2013 embora a <strong>ANSOL o vá celebrar nos dias 24 e 27 de Março</strong>. As celebrações deste dia, em 2013, vão ser espalhadas pelo mundo, e podem decorrer entre os dias <em>21 de Março</em> e <em>2 de Abril</em>.</p><ul><li class="line862"><a href="https://membros.ansol.org/civicrm/event/info?reset=1&amp;id=4">Inscrições</a> para a <a href="https://membros.ansol.org/civicrm/event/info?id=4">Hackfest</a> (normas abertas e CloudPT) que decorerrá na manhã (<a href="https://ansol.org/dfd2013/hackfestrules">consulta as regras aqui</a>)</li><li class="line862"><a href="https://membros.ansol.org/civicrm/event/info?reset=1&amp;id=5">Inscrição</a> para as palestras à tarde</li><li class="line862"><a href="https://membros.ansol.org/civicrm/event/info?reset=1&amp;id=6">Inscrição para os concertos gratuitos da noite do dia 27</a> para festejar no dia oficial ( <a href="http://www.reverbnation.com/merankorii">Merankorii</a>, <a href="http://www.ricardowebbens.tk">Ricardo Webbens</a>&nbsp;e&nbsp;<a href="http://www.cityarts.com/">Jonh Klima</a>, <a href="http://www.youtube.com/watch?v=1itHxh3_8vU">dOISsEMIcIRCUITOSiNVERTIDOS</a> e&nbsp;<a href="http://soundcloud.com/mpex">M-PeX</a>)</li></ul><h3 class="line862">Agenda do dia 24</h3><ul><li>09:30 - 12:30 Hackfest - venha finalizar e apresentar o seu projeto</li><li>14:30 - 19:30 Palestras e Debates<ul><li>Resultados da Hackfest e entrega do prémio</li><li>Sobre a ANSOL</li><li>Comunidade LibreOffice Portugal /&nbsp;ManualTIC e LibreOffice</li><li>DRM como entrave às Normas Abertas</li><li>A importância das Normas Abertas na Administração Pública, com a presença de deputados envolvidos na Lei das Normas Abertas, 36/2011, com a presença dos deputados Bruno Dias (PCP) e Michael Seufert (CDS).</li></ul></li></ul><h3 class="line862">Agenda do dia 27</h3><ul><li>21:00 - 00:00 Concertos gratuitos<ul><li><a href="http://www.reverbnation.com/merankorii">Merankorii</a></li><li><a href="http://www.ricardowebbens.tk">Ricardo Webbens</a>&nbsp;e&nbsp;<a href="http://www.cityarts.com/">Jonh Klima</a></li><li><a href="http://www.youtube.com/watch?v=1itHxh3_8vU">dOISsEMIcIRCUITOSiNVERTIDOS</a></li><li><a href="http://soundcloud.com/mpex">M-PeX</a></li></ul></li></ul>
