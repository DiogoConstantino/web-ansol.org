---
categories: []
metadata:
  event_location:
  - event_location_value: Fundação Portuguesa das Comunicações, Lisboa
  event_site:
  - event_site_url: http://www.miudossegurosna.net/eprivacidade.html
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-01-28 00:00:00.000000000 +00:00
    event_start_value2: 2020-01-28 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAAAs/EwiwIgrNOibWkNA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38707882905458e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.9150361418724e1
    mapa_left: !ruby/object:BigDecimal 27:-0.9150361418724e1
    mapa_top: !ruby/object:BigDecimal 27:0.38707882905458e2
    mapa_right: !ruby/object:BigDecimal 27:-0.9150361418724e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38707882905458e2
    mapa_geohash: eyckpy8cv07gxbxz
  slide:
  - slide_value: 0
  node_id: 725
layout: evento
title: Evento de Lançamento Público da Iniciativa "ePrivacidade Trocada Por Miúdos"
created: 1579867893
date: 2020-01-24
---
<p>A iniciativa "ePrivacidade Trocada Por Miúdos", à qual a ANSOL se associou, e da qual é parceira, vai ter o seu Lançamento Público no dia 28 de Janeiro, em Lisboa.</p><p>“ePrivacidade Trocada Por Miúdos” é uma campanha sobre privacidade e segurança&nbsp;<em>online</em>&nbsp;que visa sensibilizar crianças, pré-adolescentes, adolescentes e jovens adultos para a confiança e segurança&nbsp;<em>online</em>. A iniciativa inclui quatro componentes: um evento anual para assinalar o Dia da Proteção de Dados; a disponibilização de tutoriais e outros recursos sobre privacidade e segurança&nbsp;<em>online</em>; um concurso anual destinado a estudantes; disseminação de trabalhos de estudantes sobre a privacidade e segurança&nbsp;<em>online</em>.</p>
