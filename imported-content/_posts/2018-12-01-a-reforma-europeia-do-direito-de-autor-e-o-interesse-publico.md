---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: http://www.bnportugal.gov.pt/index.php?option=com_content&view=article&id=1399%3Aencontro-a-reforma-europeia-do-direito-de-autor-e-o-interesse-publico-4-dez-18-9h30-17h30&catid=169%3A2018&Itemid=255&lang=pt&fbclid=IwAR3wBgJyJCYYSHFk32HzjU2_21J1U8clw4zfPFUVwa85WjHCUKfOfnPxc0M
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-12-04 00:00:00.000000000 +00:00
    event_start_value2: 2018-12-04 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 638
layout: evento
title: A Reforma Europeia do Direito de Autor e o interesse público
created: 1543693740
date: 2018-12-01
---
<p><img src="https://shifter.sapo.pt/wp-content/uploads/2018/11/Encontro-Direitos-Autor-BN_01.jpg" style="display: block; margin-left: auto; margin-right: auto;" width="1000" height="667"></p><p>Encontro organizado pela Wikimédia Portugal (WMPT), D3 - Defesa dos Direitos Digitais e pela ANSOL, que visa atualizar a legislação de direito de autor para o mercado digital.</p><p>Este evento ocorre no auditório da Biblioteca Nacional, e é de entrada livre. Mais detalhes e o programa do evento podem ser vistos no <a href="http://www.bnportugal.gov.pt/index.php?option=com_content&amp;view=article&amp;id=1399%3Aencontro-a-reforma-europeia-do-direito-de-autor-e-o-interesse-publico-4-dez-18-9h30-17h30&amp;catid=169%3A2018&amp;Itemid=255&amp;lang=pt&amp;fbclid=IwAR3wBgJyJCYYSHFk32HzjU2_21J1U8clw4zfPFUVwa85WjHCUKfOfnPxc0M">site da biblioteca nacional</a>.</p>
