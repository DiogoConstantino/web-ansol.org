---
categories: []
metadata:
  event_location:
  - event_location_value: Albergaria-a-Velha
  event_site:
  - event_site_url: http://www.bad.pt/1cigia/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-11-02 00:00:00.000000000 +00:00
    event_start_value2: 2017-11-04 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 523
layout: evento
title: 1ª Conferência Internacional de Gestão da Informação e Arquivos (CIGIA)
created: 1509300172
date: 2017-10-29
---
<p>O evento terá a presença da Artefactual: <a href="https://www.artefactual.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.artefactual.com/&amp;source=gmail&amp;ust=1509376611452000&amp;usg=AFQjCNEZncNDBS7Vit12Ul9Gijcb_EtoEw">https://www.artefactual.com/</a>, empresa Canadiana, autora das soluções<strong> <a href="https://www.accesstomemory.org/pt/" target="_blank" style="background-color: #ffff00;" data-saferedirecturl="https://www.google.com/url?q=https://www.accesstomemory.org/pt/&amp;source=gmail&amp;ust=1509376611452000&amp;usg=AFQjCNERqSYXH4pZExOPXPCt9ArGIjvt_g">AtoM</a></strong> e <strong><a href="https://www.archivematica.org" target="_blank" style="background-color: #ffff00;" data-saferedirecturl="https://www.google.com/url?q=https://www.archivematica.org&amp;source=gmail&amp;ust=1509376611452000&amp;usg=AFQjCNF9uBYxxJyTbIUFXiNo5uO-_oBLMg">Archivematica</a></strong> em Software Livre, que recomenda e é conforme os Standards Internacionais do International Council of Archives (ICA): <a href="http://www.ica.org" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://www.ica.org&amp;source=gmail&amp;ust=1509376611452000&amp;usg=AFQjCNElMPBKmMtgoVuWlp8-Db4LRwvmwQ">http://www.ica.org</a><br><br>A solução "Access to Memory" (AtoM) está a ser adoptada por diversas entidades Portuguesas e é um caso de sucesso em todo o mundo da utilização de Software Livre: <a href="https://wiki.accesstomemory.org/Community/Users" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://wiki.accesstomemory.org/Community/Users&amp;source=gmail&amp;ust=1509376611452000&amp;usg=AFQjCNH-___gB66rki_t8M-zOdAr8pRpUQ">https://wiki.accesstomemory.org/Community/Users</a></p>
