---
excerpt: Tomando conhecimento de uma adjudicação problemática do Infarmed para criação
  de uma solução mobile de pesquisa de medicamentos, a ANSOL) propôs neste Domingo
  à Autoridade a publicação das aplicações recentemente encomendadas sob licenciamento
  de Software Livre no mesmo repositório da AMA onde já se encontra o software do
  Cartão do Cidadão, http://svn.gov.pt/
categories:
- press release
- imprensa
- infarmed
- ama
- software livre
metadata:
  image:
  - image_fid: 26
    image_alt: Logotipo do infarmed
    image_title: ''
    image_width: 242
    image_height: 80
  tags:
  - tags_tid: 9
  - tags_tid: 19
  - tags_tid: 39
  - tags_tid: 40
  - tags_tid: 41
  node_id: 215
layout: article
title: ANSOL desafia Infarmed a libertar apps móveis
created: 1397419654
date: 2014-04-13
---
<p>Lisboa, 13 de Abril de 2014 - Tomando conhecimento de uma adjudicação problemática do&nbsp;<a href="http://www.infarmed.pt/portal/page/portal/INFARMED">Infarmed</a> (Autoridade Nacional de Medicamentos e Produtos de Saúde, I. P.) para criação de uma <em>solução mobile de pesquisa de medicamentos</em>, a Associação Nacional para o Software Livre (<a href="https://ansol.org/">ANSOL</a>) propôs neste Domingo à Autoridade a publicação das aplicações recentemente encomendadas sob licenciamento de Software Livre no mesmo repositório da Agência para a Modernização Administrativa (<a href="http://www.ama.pt/">AMA</a>) onde já se encontra o software do Cartão do Cidadão, <a href="http://svn.gov.pt/">http://svn.gov.pt/</a></p><p>«<em>Uma boa decisão da Administração Central foi a criação de um repositório público onde as entidades públicas podem desenvolver colaborativamente o Software Livre que publicam</em>», diz Rui Seabra, o presidente da Direção da ANSOL pois isso «<em>permitiria que voluntários interessados em colmatar as lacunas pudessem participar, reduzindo assim os custos de manutenção e desenvolvimento evolutivo</em>», continua.</p><p>Não existe nenhuma aplicação de livre acesso aos cidadãos que permita a pesquisa de medicamentos, não se tratando de uma área muito procurada por programadores de forma voluntária, pelo que a ANSOL considera que se fossem Software Livre isso permitiria vencer a barreira da existência de um esqueleto funcional e permitiria que pequenas contribuições fossem adicionadas «<em>como por exemplo traduções para outros idiomas para que turistas pudessem recorrer às aplicações ou o tal suporte a invisuais que aparentemente não fez parte da encomenda original do software</em>» acrescenta o presidente.</p><p>A ANSOL relata que na ausência de um contacto por email para a administração da Autoridade, recorreu aos emails gerais, compras públicas e equipa de publicidade constantes na página de contactos «<em>Temos esperança que o Infarmed fale connosco e agarre esta oportunidade rara de sanear uma compra que não correu bem</em>», conclui Rui Seabra</p><p>Para mais informações, consultar a <a href="https://ansol.org/contato">página de contactos</a> da ANSOL.</p>
