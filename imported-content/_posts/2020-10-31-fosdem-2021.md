---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://fosdem.org/2021/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-02-06 00:00:00.000000000 +00:00
    event_start_value2: 2021-02-07 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 761
layout: evento
title: FOSDEM 2021
created: 1604167228
date: 2020-10-31
---

