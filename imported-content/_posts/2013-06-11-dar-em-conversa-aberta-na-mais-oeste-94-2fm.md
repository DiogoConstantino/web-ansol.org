---
categories: []
metadata:
  event_location:
  - event_location_value: 'Mais Oeste - 94.2FM '
  event_site:
  - event_site_url: https://www.facebook.com/events/362254213896699/?notif_t=plan_user_invited
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-06-11 20:00:00.000000000 +01:00
    event_start_value2: 2013-06-11 21:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 179
layout: evento
title: '"DAR em Conversa Aberta" na Mais Oeste - 94.2FM'
created: 1370971399
date: 2013-06-11
---
<p>Neste programa falar-se-à sobre Licenças Open e a "Propriedade Intelectual Livre".</p>
