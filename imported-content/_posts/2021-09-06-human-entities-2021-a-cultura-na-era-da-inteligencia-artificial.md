---
categories: []
metadata:
  event_location:
  - event_location_value: Palácio Sinel de Cordes - Trienal de Arquitectura de Lisboa
  event_site:
  - event_site_url: https://www.eventbrite.pt/e/registo-human-entities-2021-ines-cisneiros-163552101739
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-09-14 23:00:00.000000000 +01:00
    event_start_value2: 2021-09-14 23:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 814
layout: evento
title: 'Human Entities 2021: a cultura na era da inteligência artificial'
created: 1630925936
date: 2021-09-06
---

