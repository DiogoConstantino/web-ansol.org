---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://openforumeurope.org/event/ofe-lounge-series-standardisation-and-open-source/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-02-11 17:00:00.000000000 +00:00
    event_start_value2: 2021-02-11 18:00:00.000000000 +00:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 776
layout: evento
title: 'OFE Lounge Series: Standardisation and Open Source'
created: 1612728853
date: 2021-02-07
---

