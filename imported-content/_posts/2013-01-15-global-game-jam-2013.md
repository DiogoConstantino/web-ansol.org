---
categories: []
metadata:
  event_location:
  - event_location_value: AltLab - Lisboa
  event_site:
  - event_site_url: http://globalgamejam.org/sites/2013/altlab-lisbons-hackerspace
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-01-25 00:00:00.000000000 +00:00
    event_start_value2: 2013-01-27 00:00:00.000000000 +00:00
  node_id: 125
layout: evento
title: Global Game Jam 2013
created: 1358252312
date: 2013-01-15
---
<p>O Global Game Jam &eacute; um evento inter&shy;na&shy;cional onde durante um fim-de-semana gru&shy;pos de pro&shy;gra&shy;madores, design&shy;ers, artis&shy;tas visuais&nbsp;e m&uacute;si&shy;cos de todo o mundo, se jun&shy;tam em equipas locais for&shy;madas&nbsp;espon&shy;t&acirc;nea&shy;mente com o objec&shy;tivo de ten&shy;tar desen&shy;har e desen&shy;volver&nbsp;um video&shy;jogo em 48h.Trata-se uma exper&shy;i&ecirc;n&shy;cia cria&shy;tiva intensa e&nbsp;extremamente enrique&shy;ce&shy;dora para todos aque&shy;les inter&shy;es&shy;sa&shy;dos no&nbsp;desen&shy;volvi&shy;mento de video&shy;jo&shy;gos, desde profis&shy;sion&shy;ais a estu&shy;dantes,&nbsp;artis&shy;tas, ou at&eacute; meros curiosos, e ofer&shy;ece uma opor&shy;tu&shy;nidade&nbsp;exce&shy;lente para apren&shy;der, par&shy;til&shy;har exper&shy;i&ecirc;n&shy;cias e mostrar tal&shy;ento&nbsp;a uma comu&shy;nidade global.</p>
