---
categories:
- consultoria
- desenvolvimento
- distribuição/venda
- suporte
metadata:
  email:
  - email_email: geral@intraneia.com
  servicos:
  - servicos_tid: 7
  - servicos_tid: 5
  - servicos_tid: 8
  - servicos_tid: 2
  site:
  - site_url: http://www.intraneia.com/servicos/suporte-sl.xhtml
    site_title: http://www.intraneia.com/servicos/suporte-sl.xhtml
    site_attributes: a:0:{}
  node_id: 44
layout: servicos
title: Intraneia - Sistemas de Informação
created: 1334500107
date: 2012-04-15
---
<p>Servi&ccedil;o de apoio ao desenvolvimento, cria&ccedil;&atilde;o, distribui&ccedil;&atilde;o, licenciamento e migra&ccedil;&atilde;o para software livre.</p>
<p>A Intraneia oferece um conjunto de servi&ccedil;os dedicado aos utilizadores de software livre:</p>
<ul>
	<li>
		Apoio telef&oacute;nico e/ou presencial ao utilizador.</li>
	<li>
		Correc&ccedil;&atilde;o de problemas.</li>
	<li>
		Apoio &agrave; configura&ccedil;&atilde;o e administra&ccedil;&atilde;o de software livre.</li>
	<li>
		Garantias de funcionamento.</li>
</ul>
