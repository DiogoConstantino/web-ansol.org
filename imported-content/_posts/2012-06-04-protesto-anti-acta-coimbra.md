---
categories: []
metadata:
  event_location:
  - event_location_value: Praça da República, Coimbra
  event_site:
  - event_site_url: https://www.facebook.com/#!/events/284963381600253/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-06-09 14:00:00.000000000 +01:00
    event_start_value2: 2012-06-09 14:00:00.000000000 +01:00
  node_id: 78
layout: evento
title: Protesto anti-ACTA - Coimbra
created: 1338803188
date: 2012-06-04
---
<p>(veja tamb&eacute;m informa&ccedil;&atilde;o sobre o <a href="https://ansol.org/acta/protesto-20120609">protesto em Lisboa</a>)</p>
