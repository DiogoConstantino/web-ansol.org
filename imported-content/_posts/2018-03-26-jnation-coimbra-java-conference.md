---
categories:
- java conference
- coimbra
metadata:
  event_location:
  - event_location_value: Convento de São Francisco, Coimbra
  event_site:
  - event_site_url: https://jnation.pt
    event_site_title: JNation Conference
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-06-18 23:00:00.000000000 +01:00
    event_start_value2: 2018-06-18 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 232
  - tags_tid: 233
  node_id: 560
layout: evento
title: JNation Coimbra - Java Conference
created: 1522019394
date: 2018-03-26
---
<div class="wpb_column col-sm-6" style="height: 335px;"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="wpb_text_column wpb_content_element "><div class="wpb_wrapper"><h2>The JNation Conference is about bringing together technology enthusiasts from all over the world with a passion for Java.</h2><p>For a full day, you will get to hear Rock Star speakers talk about Java and JVM related technologies, frameworks, tools, programming languages, cloud, internet of things and many more.</p><p>Join us in Coimbra, Portugal to share the knowledge and meet other enthusiasts in the Java Community.</p></div></div></div></div></div>
