---
categories: []
metadata:
  event_location:
  - event_location_value: ISCTE, Lisboa
  event_site:
  - event_site_url: https://lisbon2018.drupaldays.org/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-07-01 23:00:00.000000000 +01:00
    event_start_value2: 2018-07-05 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 528
layout: evento
title: Drupal Developer Days 2018
created: 1510413469
date: 2017-11-11
---
<div class="intro-section__title"><h1 style="text-align: center;">Drupal Developer Days</h1><h2 style="text-align: center;">is coming to Lisbon in 2018<br>Monday 2nd July - Friday 6th July on ISCTE</h2></div><div class="intro-section__text"><p style="text-align: center;">Join us for a week of Drupal with a Portuguese touch.</p><h3><strong>SPRINTS, SESSIONS, WORKSHOPS, SOCIAL ACTIVITIES.</strong></h3><p>Drupal Developer Days is an event organized by the Drupal community which gathers people who contribute on the progress of Drupal from all over the world, happening in Europe every year.</p></div>
