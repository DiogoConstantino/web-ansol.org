---
categories:
- linux
- software livre
- asia
- taiwan
metadata:
  event_location:
  - event_location_value: National Taiwan University of Science and Technology, Taiwan
  event_site:
  - event_site_url: https://2018.coscup.org/#/home
    event_site_title: COSCUP
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-08-10 23:00:00.000000000 +01:00
    event_start_value2: 2018-08-11 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 242
  - tags_tid: 41
  - tags_tid: 243
  - tags_tid: 244
  node_id: 567
layout: evento
title: COSCUP
created: 1522846365
date: 2018-04-04
---
<p><strong>Conference for Open Source Coders, Users and Promoters</strong> (<strong>COSCUP</strong>; <a href="https://en.wikipedia.org/wiki/Chinese_language" title="Chinese language">Chinese</a>: <span lang="zh">開源人年會</span>) is an annual conference held by Taiwanese <a href="https://en.wikipedia.org/wiki/Open_source_community" title="Open source community" class="mw-redirect">Open source community</a> participants since 2006. It's a major force of <a href="https://en.wikipedia.org/wiki/Free_software_movement" title="Free software movement">Free software movement</a> advocacy in <a href="https://en.wikipedia.org/wiki/Taiwan" title="Taiwan">Taiwan</a>. The event is often held in two days, with talks, sponsor and communities booths, and <a href="https://en.wikipedia.org/wiki/Birds_of_a_feather_%28computing%29" title="Birds of a feather (computing)">Birds of a feather (computing)</a>. In addition to international speakers, many Taiwanese local <a href="https://en.wikipedia.org/wiki/Open_Source" title="Open Source" class="mw-redirect">Open Source</a> contributors often give their talks here. The chief organizer, other staffs, and speakers are all volunteers.</p><p>COSCUP's aim is to provide a platform to connect <a href="https://en.wikipedia.org/wiki/Open_Source" title="Open Source" class="mw-redirect">Open Source</a> coders, users, and promoters, and promote <a href="https://en.wikipedia.org/wiki/FLOSS" title="FLOSS" class="mw-redirect">FLOSS</a> with the annual conference. The conference is free to attend because of the enthusiastic sponsors and donators. Since the conference venue is limited, tickets are often sold out immediately after the online registration starts.</p>
