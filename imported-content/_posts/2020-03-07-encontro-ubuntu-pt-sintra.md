---
categories: []
metadata:
  event_location:
  - event_location_value: Sintra
  event_site:
  - event_site_url: https://www.meetup.com/ubuntupt/events/268225079
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-03-12 20:00:00.000000000 +00:00
    event_start_value2: 2020-03-12 20:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 736
layout: evento
title: Encontro Ubuntu-pt @ Sintra
created: 1583598271
date: 2020-03-07
---

