---
categories: []
metadata:
  event_location:
  - event_location_value: Espaço J, Jardim da Estrada dos Arneiros, Benfica
  event_start:
  - event_start_value: 2013-07-27 15:00:00.000000000 +01:00
    event_start_value2: 2013-07-27 19:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 199
layout: evento
title: Encontros ANSOL - CryptoParty
created: 1373528902
date: 2013-07-11
---
<div style="float: left; padding: 1em;"><img src="http://www.cryptoparty.in/_media/cp-logo-200x67.png" alt="CryptoParty"></div><p>No <strong>dia 27 de Julho</strong> vai decorrer no <strong>Espaço J do Jardim da Estrada dos Arneiros, em Benfica das 16 às 20 horas</strong>, a primeira edição mensal dos <strong>Encontros ANSOL</strong> com o tema <a href="http://www.cryptoparty.in/parties/howto"><strong>Crypto Party</strong></a>.</p><p>&nbsp;</p><p><a href="http://commons.wikimedia.org/wiki/File%3AEdward_Snowden-2.jpg" title="Laura Poitras / Praxis Films [CC-BY-3.0 (http://creativecommons.org/licenses/by/3.0)], via Wikimedia Commons"><img src="//upload.wikimedia.org/wikipedia/commons/thumb/6/60/Edward_Snowden-2.jpg/256px-Edward_Snowden-2.jpg" alt="Edward Snowden-2" style="float: right; border: 2px solid black;" width="128"></a>Quem é Edward Snowden? Porque estão as agências de segurança norte-americanas desesperadamente a tentar apanhá-lo qual jogo "<em>Where in the world is Carmen Sandiego?</em>"? Somos espiados? Os EUA espiam a Europa, os Ingleses espiam os Alemães, os Franceses queixam-se dos EUA os espiarem mas espiam os cidadãos, afinal a NSA está em conluio com os Alemães... A Microsoft e o Skype ajudam a NSA a espiar-nos. E a Apple. E o Google. Quem espia o quê? E em Portugal?</p><p>&nbsp;</p><p><a href="http://commons.wikimedia.org/wiki/File%3ANational_Security_Agency.svg" title="By U.S. Government (www.nsa.gov) [Public domain], via Wikimedia Commons"><img src="//upload.wikimedia.org/wikipedia/commons/thumb/0/04/National_Security_Agency.svg/128px-National_Security_Agency.svg.png" alt="National Security Agency" style="float: left;" width="128"></a><strong>E nós? Como nos podemos proteger destes abusos? Trás o teu computador e/ou telefone ou tablet e vem descobrir connosco!</strong></p><p>&nbsp;</p><p>Nota: se puderes trás qualquer coisinha para comer ou beber, não é preciso muito, se formos poucos come-se e bebe-se pouco, se formos muitos e cada um trouxer haverá para todos.</p>
