---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_start:
  - event_start_value: 2011-04-24 23:00:00.000000000 +01:00
    event_start_value2: 2011-04-24 23:00:00.000000000 +01:00
  node_id: 92
layout: evento
title: Dia da Liberdade
created: 1350237580
date: 2012-10-14
---
<p>Desfile Comemorativo: 25 de Abril<br><br></p><p>A ANSOL esteve presente no desfile, a distribuir informação e celebrar a Liberdade também no Software.</p>
