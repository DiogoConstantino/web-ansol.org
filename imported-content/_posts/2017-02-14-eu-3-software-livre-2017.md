---
categories:
- "#ilovefs"
- ilovefs
metadata:
  event_location:
  - event_location_value: Todo o Mundo
  event_site:
  - event_site_url: https://fsfe.org/campaigns/ilovefs/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-02-14 00:00:00.000000000 +00:00
    event_start_value2: 2017-02-14 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 62
  - tags_tid: 56
  node_id: 483
layout: evento
title: Eu <3 Software Livre 2017
created: 1487070196
date: 2017-02-14
---
<div style="float: left; padding: 1em;"><img src="https://ansol.org/sites/ansol.org/files/ilovefs-heart-px.png" alt="#ILoveFS" title="#ILoveFS" height="309" width="350"></div><p>Todos os anos a 14 de Fevereiro, a ANSOL junta-se à Free Software Foundation Europe e pede a todos os utilizadores de Software Livre que pensem naquelas pessoas trabalhadoras na comunidade de Software Livre e mostrem o seu apreço individualmente neste dia de "Eu adoro o Software Livre".</p><p>&nbsp;Tal como no ano passado, a campanha é dedicada às pessoas por detrás do Software Livre porque eles permitem-nos usar, estudar, partilhar e melhorar o software que nos permite trabalhar em liberdade.</p><p>Este ano, a ANSOL celebra o dia de forma ligeiramente aos anteriores: quando mostrarem (durante o dia de hoje) o vosso apreço pelo software livre (no geral, software em particular, ou mesmo uma pessoa ou equipa que contribui para o Software Livre), <a href="mailto:marcos.marado@ansol.org">enviem-nos um mail</a> com informação do que fizeram (uma foto, um link, o que for mais conveniente). A declaração de amor pelo software livre mais criativa será premiada com uma anuidade nas quotas de sócio da ANSOL* para o seu criador.</p><p>Mais sobre a campanha (e ideias sobre como festejar o dia) <a href="https://fsfe.org/campaigns/ilovefs/">no site oficial</a>.</p><hr><p>&nbsp;</p><p><em>*Esta campanha está aberta a todos. Se o vencedor não for sócio terá a hipótese de se associar gratuitamente</em></p>
