---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://www.crowdcast.io/e/oss-automotive/register
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-05-03 23:00:00.000000000 +01:00
    event_start_value2: 2021-05-03 23:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 796
layout: evento
title: 'Digitising European Industry: Open Source Driving the Automotive Sector'
created: 1619376721
date: 2021-04-25
---

