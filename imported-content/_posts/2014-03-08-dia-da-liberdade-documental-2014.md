---
categories: []
metadata:
  event_location:
  - event_location_value: ISCTE, Auditório B204
  event_site:
  - event_site_url: http://documentfreedom.org/pt
    event_site_title: documentfreedom.org
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-03-29 14:00:00.000000000 +00:00
    event_start_value2: 2014-03-29 18:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 214
layout: evento
title: Dia da Liberdade Documental 2014
created: 1394300560
date: 2014-03-08
---
<p><a href="http://www.adrianoafonso.net/blog/wp-content/uploads/2014/03/dfd-2014-portugal_small.png" target="_blank"><img src="http://www.adrianoafonso.net/blog/wp-content/uploads/2014/03/dfd-2014-portugal_small.png" alt="Poster DFD2014" title="Poster DFD2014" style="float: right;" height="212" width="150"></a> No próximo dia <strong>29 de Março de 2014</strong>, a <a href="http://www.libreoffice.pt/">Comunidade LibreOffice Portugal</a>, a ANSOL e o&nbsp;<a href="http://iscte-iul.pt/cursos/mestrados/10702/apresentacao.aspx">Mestrado em Software de Código Aberto do ISCTE</a> celebram o Dia da Liberdade Documental (Document Freedom Day) e convidam-te, bem como aos teus amigos a estarem presentes. A celebração deste dia regista-se no mundo inteiro, sendo que pode decorrer entre os dias 22 e 30 de Março.</p><p class="line862"><span style="text-decoration: line-through;"><a href="https://membros.ansol.org/civicrm/event/register?reset=1&amp;id=7">Inscreve-te já!</a> Apesar de não ser obrigatória, a inscrição ajuda-nos a planear um bom evento.</span> :)</p><p class="line862">Perdeste este espetacular evento? Que pena... :( mas não é grave, poderás consultar as apresentações feitas mais abaixo <span style="text-decoration: line-through;">em breve</span></p><h3 class="line862">Agenda</h3><ul><li>14h - 18h Palestras e Debates<ul><li>14h-14h30 // Prof. Carlos Costa e Rui Seabra - Painel de Abertura</li><li>14h30-15h // Alexandre Marreiros (html5pt.org) - <a href="https://ansol.org/sites/ansol.org/files/dfd/2014/html5ignitionNewWebOrder.pdf">HTML5 and the new Web Order</a></li><li>15h-15h30 // Sérgio Prazeres (use.com.pt) - <a href="https://ansol.org/sites/ansol.org/files/dfd/2014/SIG/29mar2014_SIG_ISCTE_v2.pdf">Sistemas de Informação Geográfica em formatos Livres</a></li><li>15h30-16h // Tiago Carrondo (ubuntu-pt.org) - <a href="https://ansol.org/sites/ansol.org/files/dfd/2014/FormacaoSL/dfd2014.pdf">Formação com Software Livre</a></li><li>16h-16h30 // Ana Branco (ama.pt) - Documentação Livre na Administração Pública</li><li>16h30-17h // Adriano Afonso (libreoffice.pt) - <a href="https://ansol.org/sites/ansol.org/files/dfd/2014/ManualTICLiBo/dfd2014.odp">Manual TIC e LibreOffice</a></li><li>17h-18h // Debate - Documentação e CMS's</li></ul></li></ul><h3>&nbsp;Ligações</h3><ul><li><a href="https://www.facebook.com/events/406502256153488" target="_blank">Evento no Facebook</a></li><li><a href="https://plus.google.com/u/0/events/c6n381j55dp4riqf89lpm2dc9f8" target="_blank">Evento no Google+</a></li><li><a href="https://plus.google.com/u/0/events/gallery/c6n381j55dp4riqf89lpm2dc9f8">Fotografias no G+</a></li><li><a href="https://blog.1407.org/gallery/document-freedom-day-2014/">Evento no blog do Rui Seabra</a></li></ul><p>&nbsp;</p>
