---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://op.europa.eu/en/web/euopendatadays/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-11-23 00:00:00.000000000 +00:00
    event_start_value2: 2021-11-25 00:00:00.000000000 +00:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 803
layout: evento
title: EU Open Data Days
created: 1622125222
date: 2021-05-27
---
<p>As primeiras Jornadas de Dados Abertos da União Europeia decorrerão entre 23 e 25 de novembro de 2021. Servindo de pólo de difusão de conhecimento e trazendo os benefícios dos dados abertos para o setor público da UE e, através dele, para as pessoas e as empresas, trata-se de um evento que procurará servir de inspiração e de montra de divulgação para as tendências mais recentes e as soluções mais inovadoras desta área.</p>
