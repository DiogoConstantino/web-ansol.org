---
categories:
- noites de sopa e programação no museu
- alemanha
metadata:
  event_location:
  - event_location_value: Hochschule fuer Gestaltung (HFG) and Zentrum fuer Kunst
      und Medien (ZKM), Karlsruhe, Germany
  event_site:
  - event_site_url: https://entropia.de/GPN18/en
    event_site_title: Gulaschprogrammiernacht
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-09 23:00:00.000000000 +01:00
    event_start_value2: 2018-05-12 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 259
  - tags_tid: 260
  node_id: 578
layout: evento
title: GPN18 - Gulaschprogrammiernacht 2018 - Gulasch programming night 2018 - Noites
  de Sopa e programação no museu 2018
created: 1522860172
date: 2018-04-04
---
<p>Gulasch programming night</p><p>Hacking, goulash, lectures, cloud, tschunk, workshops, lounge, mate, fun with gadgets...</p><p>Gulasch programming night (the translation of Gulaschprogrammiernacht) could be mistaken for a cooking marathon for computer science students, it is in fact the second largest CCC affiliated event/conference. It all started back in 2002 with a few technology enthusiasts and has steadily grown since. The topics presented at GPN are widely varied. The bulk of talks and workshops focus on hard- and software development, as well as digital rights and privacy on the internet. Beside those obvious topics we have had many talks answering questions like "How do I fly a spaceship?" to "How do I make the perfect Gulasch?".</p><p>The GPN is organized by the Entropia e.V., the local branch of the Chaos Communication Club. It is held in the halls of the Hochschule fuer Gestaltung (HFG) and Zentrum fuer Kunst und Medien (ZKM).</p><p>The ZKM is a museum focused on digital art. During the event visitors can freely move through the GPN as we don't limit access. Because of that we ask possible speakers to try to speak to a general audience and not only to the digital-natives found at more specialized events. We however understand that this is not allways possible as some topics require prior knowledge. It is our goal that as many people as reasonably possible can learn something.</p><p>As mentioned in the name programming is nevertheless an important part. Therefor you will be able to find tables, chairs, power, and a network connection in our Hackcenter that hopefully will facilitate your programming endevour.</p><p>That said, most of the participants are bringing their project with them to spend a few days (and maybe nights) working on it. This does not mean that you cant ask them about it. Many of our participants will be happy to talk to you about what they are doing. As a result the Hackcenter is normally full of flashing lights of all colors oscilloscopes and many other types of odd and less odd hardware. So why not come along.</p><table class="highlight" style="border: 1px solid; font-size: 150%; margin: 2em 1em 1em; padding: 0.3em 1em;"><tbody><tr><td class="highlight"><p>10.&nbsp;-&nbsp;13. may 2018</p></td></tr></tbody></table><h3><span id="Attending_the_GPN:" class="mw-headline">Attending the GPN:</span></h3><p>The Gulaschprogrammiernacht is an open event for everybody. We do not charge an entry fee or impose access restrictions (without a good reason). We would however ask you to register in advance if you are planing to attend. This helps to optimize our setup to the amount of people that are going to attend and reduces avoidable waste. As the Gulaschprogrammiernacht does not sell tickets and tries to sell food and drinks at a reasonable price we would want to kindly ask for your support through a donation. Furthermore we rely on volunteers to keep everything going. If you find yourself asking what to do, you can find many suggestions in our "Trollsystem" where we manage the tasks that are given to volunteers.</p>
