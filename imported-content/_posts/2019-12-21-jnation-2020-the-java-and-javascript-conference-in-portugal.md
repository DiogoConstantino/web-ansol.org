---
categories: []
metadata:
  event_location:
  - event_location_value: online
  event_site:
  - event_site_url: https://2020.jnation.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-06-01 23:00:00.000000000 +01:00
    event_start_value2: 2020-06-02 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 721
layout: evento
title: JNation 2020 - The Java and Javascript Conference in Portugal
created: 1576956413
date: 2019-12-21
---

