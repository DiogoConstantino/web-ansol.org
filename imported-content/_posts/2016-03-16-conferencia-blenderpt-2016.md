---
categories:
- blender
metadata:
  event_location:
  - event_location_value: Instituto Superior de Engenharia de Coimbra, Rua Pedro Nunes,
      3030-199 Coimbra
  event_site:
  - event_site_url: http://www.conferencia.problender.pt/
    event_site_title: Conferência BlenderPT
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-04-15 08:00:00.000000000 +01:00
    event_start_value2: 2016-04-16 20:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 161
  node_id: 405
layout: evento
title: Conferência BlenderPT 2016
created: 1458143150
date: 2016-03-16
---

