---
categories: []
metadata:
  event_location:
  - event_location_value: Albergaria-a-Velha (Biblioteca Municipal)
  event_site:
  - event_site_url: http://osgeopt.pt/meetup2019/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-10-01 23:00:00.000000000 +01:00
    event_start_value2: 2019-10-01 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 698
layout: evento
title: 'OSGeo-PT Meetup: A INFORMAÇÃO nas Autarquias - Desafios e Oportunidades'
created: 1569947415
date: 2019-10-01
---

