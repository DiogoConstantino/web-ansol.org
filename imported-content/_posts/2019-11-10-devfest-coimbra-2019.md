---
categories: []
metadata:
  event_location:
  - event_location_value: Coimbra
  event_site:
  - event_site_url: https://devfest.gdgcoimbra.xyz/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-11-16 00:00:00.000000000 +00:00
    event_start_value2: 2019-11-16 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 706
layout: evento
title: DevFest Coimbra 2019
created: 1573413784
date: 2019-11-10
---

