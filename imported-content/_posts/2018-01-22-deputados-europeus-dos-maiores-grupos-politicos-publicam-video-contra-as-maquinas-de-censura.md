---
categories:
- imprensa
metadata:
  tags:
  - tags_tid: 19
  node_id: 540
layout: article
title: Deputados Europeus dos maiores grupos políticos publicam vídeo contra as “máquinas
  de censura”
created: 1516631373
date: 2018-01-22
---
<p>&nbsp;</p><center><iframe src="https://www.youtube.com/embed/r8pi6e5GLaQ" width="560" height="315" frameborder="0"></iframe></center><p>&nbsp;</p><p><strong>22 de janeiro de 2018</strong> - Deputados de seis grupos políticos com representação no Parlamento Europeu juntaram-se para criar um vídeo alertando para os problemas das máquinas de censura que a Comissão Europeia (CE) está a propor.</p><p>&nbsp;</p><p>A proposta da CE passa a obrigar todas as plataformas na Internet que permitem a publicação de conteúdos por cidadãos a verificar previamente tudo o que os cidadãos quiserem publicar. Os cidadãos não poderão publicar conteúdos que não passem nos filtros de censura prévia utilizados pela plataformas.</p><p>O alerta surge como uma mensagem para o Conselho da União Europeia, constituído pelos Governos dos vários Estados-Membros, que reúne hoje sobre o artigo 13º da proposta da CE.</p><p>No vídeo, participam deputados Europeus dos maiores grupos políticos, como o partido dos conservadores (EPP), dos sociais-democratas (S&amp;D) ou dos Liberais (ALDE).<br><br>A deputada Marietje Schaake do grupo político Europeu ALDE dá o seu testemunho relativamente a um vídeo que colocou no YouTube de um discurso político que teve lugar no Parlamento Europeu e que o YouTube removeu, sem que a deputada saiba porque foi removido.<br><br>A <a href="http://direitosdigitais.pt">Associação D3 - Defesa dos Direitos Digitais</a>, a <a href="https://ansol.org">Associação Nacional para o Software Livre (ANSOL)</a> e a <a href="http://ensinolivre.pt">Associação Ensino Livre (AEL)</a> juntam-se aos deputados Europeus neste alerta contra as máquinas de censura, que eliminam direitos fundamentais.</p><p><strong>O vídeo, com legendas em Português, pode ser visto, partilhado e incluído em qualquer outra publicação através da seguinte hiperligação <a href="https://youtu.be/r8pi6e5GLaQ">https://youtu.be/r8pi6e5GLaQ</a> .</strong></p><p><br>Mais informação:</p><ul><li>Artigo da deputada Europeia Julia Reda - <a href="https://juliareda.eu/2018/01/censorship-machines/">https://juliareda.eu/2018/01/censorship-machines/</a></li><li>Eduardo Santos (D3) - <a href="mailto:eduardo.santos@direitosdigitais.pt">eduardo.santos@direitosdigitais.pt</a></li><li>Marcos Marado (ANSOL) - 964321998 -&nbsp; <a href="mailto:marcos.marado@ansol.org">marcos.marado@ansol.org</a></li><li>Paula Simões (AEL) - 932283394 - <a href="mailto:paulasimoes@gmail.com">paulasimoes@gmail.com</a></li></ul>
