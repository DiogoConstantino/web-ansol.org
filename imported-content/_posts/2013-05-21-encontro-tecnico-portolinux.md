---
categories: []
metadata:
  event_location:
  - event_location_value: Porto
  event_site:
  - event_site_url: http://s.clix.pt/bl
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-05-25 13:00:00.000000000 +01:00
    event_start_value2: 2013-05-25 20:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 163
layout: evento
title: Encontro Técnico PortoLinux
created: 1369129164
date: 2013-05-21
---
<ul><li><span>Dia a Dia de um Devops na Blip - Miguel Fonseca</span></li><li><span><span>"Bring it to the cloud" -&nbsp;</span><span>Hugo Tavares, CTO da Lunacloud</span></span></li><li><span><span>Pequena apresentação da Cultura da Blip e uma pequena visita às instalações</span></span></li></ul>
