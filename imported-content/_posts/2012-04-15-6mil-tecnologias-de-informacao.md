---
categories:
- consultoria
- distribuição/venda
- suporte
metadata:
  email:
  - email_email: mail@6mil.pt
  servicos:
  - servicos_tid: 7
  - servicos_tid: 8
  - servicos_tid: 2
  site:
  - site_url: http://www.6mil.pt
    site_title: http://www.6mil.pt
    site_attributes: a:0:{}
  node_id: 36
layout: servicos
title: 6mil - Tecnologias de Informação
created: 1334496129
date: 2012-04-15
---
<p>A 6mil disponibiliza servi&ccedil;os de suporte telef&oacute;nico e electr&oacute;nico. Adicionalmente temos planos de suporte &quot;on-site&quot;. A nossa especializa&ccedil;&atilde;o &eacute; nas distribui&ccedil;&otilde;es SUSE, mas temos experi&ecirc;ncia em todas as distribui&ccedil;&otilde;es mais populares (Redhat, Debian, Gentoo, Slackware, etc). Os nossos servi&ccedil;os v&atilde;o desde a assist&ecirc;ncia com problemas de configura&ccedil;&atilde;o e administra&ccedil;&atilde;o at&eacute; ao &quot;outsourcing&quot; e administra&ccedil;&atilde;o remota.</p>
