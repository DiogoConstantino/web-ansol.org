---
categories:
- ttip
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://www.nao-ao-ttip.pt/debate-18-de-junho-lisboa/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-06-18 18:30:00.000000000 +01:00
    event_start_value2: 2015-06-18 18:30:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 327
layout: evento
title: 'Debate: Tratado Transatlântico'
created: 1433090613
date: 2015-05-31
---

