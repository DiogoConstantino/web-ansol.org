---
categories: []
metadata:
  event_site:
  - event_site_url: https://fsfe.org/campaigns/ilovefs/2016/index.pt.html
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-02-14 00:00:00.000000000 +00:00
    event_start_value2: 2016-02-14 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 388
layout: evento
title: Eu <3 Software Livre 2016
created: 1452986677
date: 2016-01-16
---
<div style="float: left; padding: 1em;"><img src="https://ansol.org/sites/ansol.org/files/ilovefs-heart-px.png" alt="#ilovefs"></div><p>Todos os anos a 14 de Fevereiro, a ANSOL junta-se à Free Software Foundation Europe e pede a todos os utilizadores de Software Livre que pensem naquelas pessoas trabalhadoras na comunidade de Software Livre e mostrem o seu apreço individualmente neste dia de "Eu adoro o Software Livre".</p><p>Tal como no ano passado, a campanha é dedicada às pessoas por detrás do Software Livre porque eles permitem-nos usar, estudar, partilhar e melhorar o software que nos permite trabalhar em liberdade.</p>
