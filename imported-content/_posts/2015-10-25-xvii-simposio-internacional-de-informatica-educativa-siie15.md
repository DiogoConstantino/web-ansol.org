---
categories:
- informática educativa
- software livre
- academia
metadata:
  event_location:
  - event_location_value: Instituto Politécnica de Setúbal,Escola Superior De Educação
      , Setúbal, Portugal
  event_site:
  - event_site_url: http://siie15.ese.ips.pt/
    event_site_title: SIIE15
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-25 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-27 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 139
  - tags_tid: 41
  - tags_tid: 140
  node_id: 370
layout: evento
title: XVII Simpósio Internacional de Informática Educativa  SIIE15
created: 1445802154
date: 2015-10-25
---
<h2><strong>O SIMPÓSIO</strong></h2><p><strong>A 17.ª edição do Simposio Internacional de Informática Educativa (SIIE)</strong>&nbsp;vai realizar-se em Setúbal, entre 25 e 27 de novembro de 2015.</p><p>O SIIE é um fórum internacional de apresentação, discussão e reflexão em torno da investigação, desenvolvimento e práticas no domínio das Tecnologias de Informação e Comunicação (TIC) na Educação.</p><p>As várias edições do SIIE têm decorrido entre Portugal e Espanha e têm proporcionado um importante espaço de debate e reflexão entre investigadores, representantes institucionais e educadores dispostos a partilhar os seus pontos de vista, conhecimentos e experiências.</p><p>&nbsp;</p><p>Para a edição de 2015 do&nbsp;<strong>SIIE</strong>&nbsp;foram seleccionados os seguintes temas;</p><ul><li><p>As TIC na formação de professores</p></li><li><p>Integração das TIC no Ensino/Aprendizagem</p></li><li><p>Conceção, construção e avaliação de recursos educativos digitais (RED)</p></li><li><p>Computação e aprendizagem</p></li><li><p>A computação móvel/ubíqua na educação</p></li><li><p>Jogos e Simulações em Educação</p></li><li><p>Robótica educativa</p></li><li><p>Redes sociais e aprendizagem</p></li><li><p>Os MOOC e a formação a distância</p></li></ul><p>O SIIE é um espaço aberto e, nesse sentido, estes temas não devem ser considerados exclusivos. Serão aceites também outras trabalhos relevantes que, não estando incluídos nos temas apresentados, se integrem em outras temáticas do uso das Tecnologias da Informação e Comunicação na Educação.</p>
