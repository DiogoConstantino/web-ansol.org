---
categories: []
metadata:
  event_site:
  - event_site_url: http://app.parlamento.pt/webutils/docs/doc.pdf?Path=6148523063446f764c324679626d56304c334e706447567a4c31684a5355786c5a793944543030764d554e425130524d52793942636e463161585a765132397461584e7a5957387654334a6b5a57357a4947526c4946527959574a68624768764c304e425130524d52313878587a49324d4335775a47593d&Fich=CACDLG_1_260.pdf&Inline=true
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-10-22 09:00:00.000000000 +01:00
    event_start_value2: 2014-10-22 09:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 233
layout: evento
title: Petição contra Lei da Cópia Privada na Comissão de Especialidade
created: 1412369625
date: 2014-10-03
---
<p><span id="ctl00_ctl12_g_032dfdc5_423a_437d_a71d_a2567a1bb1ce_ctl00_lblNome">A Comissão de Assuntos Constitucionais, Direitos, Liberdades e Garantias irá nesta data fazer a </span>apreciação e votação de relatórios finais de petições, possivelmente incluindo a petição entregue pela ANSOL, contra a Lei da Cópia Privada.</p>
