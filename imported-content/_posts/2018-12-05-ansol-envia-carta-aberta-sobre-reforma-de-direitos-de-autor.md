---
categories:
- imprensa
- direitos de autor
metadata:
  tags:
  - tags_tid: 19
  - tags_tid: 96
  node_id: 641
layout: article
title: ANSOL envia carta aberta sobre reforma de Direitos de Autor
created: 1544047669
date: 2018-12-05
---
<p>Organizações focadas nos direitos humanos e nos direitos digitais, incluindo a ANSOL, <a href="https://mailchi.mp/liberties/copyright-directive-needs-human-rights-safeguards" target="_blank" rel="nofollow">enviaram uma carta aberta</a> aos decisores Europeus sobre a reforma de Direitos de Autor.</p><p>Mais informação <a href="https://www.liberties.eu/en/news/copyright-trialogue-open-letter-article/16462">aqui</a>.</p>
