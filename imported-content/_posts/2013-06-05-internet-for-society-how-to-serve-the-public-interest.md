---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: http://www.eurodig.org/eurodig-2013/programme
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-06-19 23:00:00.000000000 +01:00
    event_start_value2: 2013-06-20 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 175
layout: evento
title: Internet For Society – How To Serve The Public Interest?
created: 1370454126
date: 2013-06-05
---

