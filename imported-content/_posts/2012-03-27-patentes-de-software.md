---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 9
layout: page
title: Patentes de Software
created: 1332884990
date: 2012-03-27
---
<p>A ANSOL tem acompanhado também as várias tentativas de, contra o interesse de quem desenvolve software, se tentar legalizar as patentes de software.</p><p>Estamos contentes por termos feito parte do grupo de pessoas que conseguiu derrotar a horrível <a href="http://en.swpat.org/wiki/EU_software_patents_directive">proposta de diretiva europeia de patentes de software</a> numa estrondosa maioria de 94% dos votos dos eurodeputados, mas agora estão a tentar introduzir patentes de software através duma <a href="http://endsoftpatents.org/2012/12/unitary-patent/">"harmonização" europeia do sistema jurídico de litigação de patentes</a> que entregaria o poder máximo ao Gabinete Europeu de Patentes, que tem a prática de registar patentes de software ainda que isso vá contra as leis vigentes.</p><p>O sistema Unitário de Patentes teve a sua aplicação foi contestada e foi analisada pelo Tribunal Constitucional Alemão, mas a 9 de Julho o Tribunal não deu razão à contestação. <a href="http://blog.ffii.org/germany-can-no-longer-ratify-the-unitary-patent-due-to-brexit-and-the-established-aetr-case-law-says-ffii/">De acordo com a FFII</a>, visto que a decisão não foi tomada antes do Brexit, a Alemanha já não pode ractificar a Patente Unitária. No entanto, o Tribunal de Patentes Unitárias aparenta ter uma interpretação diferente, <a href="https://www.unified-patent-court.org/news/german-federal-constitutional-court-declares-complaints-against-upcas-ratification-bill">dizendo</a> que a Alemanha está livre para ratificar este acordo, e que além disso, para o projecto passar para a sua fase final, será ainda a intevenção de dois Estados Signatários.</p>
