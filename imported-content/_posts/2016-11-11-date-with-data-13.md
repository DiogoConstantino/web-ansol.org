---
categories: []
metadata:
  event_location:
  - event_location_value: UPTEC - Porto
  event_site:
  - event_site_url: http://datewithdata.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-11-12 10:00:00.000000000 +00:00
    event_start_value2: 2016-11-12 17:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 472
layout: evento
title: 'Date With Data #13'
created: 1478880428
date: 2016-11-11
---
<p>No <a href="http://transparenciahackday.us6.list-manage.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=4b23ee3f62&amp;e=b684bb14e9" target="_blank" class="gmail_msg" style="color: #2baadf; font-weight: normal; text-decoration: underline;" data-saferedirecturl="https://www.google.com/url?q=http://transparenciahackday.us6.list-manage.com/track/click?u%3D7c39a9eb2d8160fd5975e6fec%26id%3D4b23ee3f62%26e%3Db684bb14e9&amp;source=gmail&amp;ust=1478897835607000&amp;usg=AFQjCNFxOnwhxwst9KmFUHwKkJ1rN57gpA">Date With Data</a> deste mês vamos dedicar esforços e atenção aos projetos em curso. Cada edição traz para a mesa um tema — em setembro olhámos para os dados parlamentares, em outubro para os dados do turismo — que serve de ponto de partida para experiências visuais, análises estatísticas, recolha de dados para novos datasets... e um sem fim de ideias. Apesar da nossa metodologia intensiva — "Fuck it! Ship it!" é um dos nossos lemas — nem sempre conseguimos chegar ao final do encontro com tudo terminado. Assim, chega a altura de olhar para o que temos, fazer um plano e meter as mãos na massa para acabar e lançar.<br class="gmail_msg"> <br class="gmail_msg"> Queres saber mais acerca de Dados Abertos? Aprender que informação está disponível e como pode ser usada? Precisas de um bom plano para ocupar um sábado de inverno? Anda ter connosco!<br class="gmail_msg"> <br class="gmail_msg"> Temos muitos afazares e todas a mãos são bem vindas: podes dar uma ajuda na análise de temas/termos dos debates do parlamento, ajudar a terminar o dataset das hamburguerias do Porto, rever as traduções de alguns textos sobre dados abertos, empacotar datasets para a <a href="http://transparenciahackday.us6.list-manage.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=03ceafbefb&amp;e=b684bb14e9" target="_blank" class="gmail_msg" style="color: #2baadf; font-weight: normal; text-decoration: underline;" data-saferedirecturl="https://www.google.com/url?q=http://transparenciahackday.us6.list-manage.com/track/click?u%3D7c39a9eb2d8160fd5975e6fec%26id%3D03ceafbefb%26e%3Db684bb14e9&amp;source=gmail&amp;ust=1478897835607000&amp;usg=AFQjCNHgaxxr_mHp-T3BEHJFd1jW93sorA">Central de Dados</a>... As possibilidades são variadas e para todos os gostos, tanto para quem gosta do lado técnico como para quem não gosta!</p><p>Além disto estamos a cozinhar duas páginas web novas: uma com a definição de Dados Abertos (pois é, ainda há muito por explicar); outra dedicada ao Hackday &amp; Date With Data, onde escrevemos sobre o nosso interesse pelos dados, quem somos e porque continuamos ativos e sem vontade de parar.<br class="gmail_msg"> <br class="gmail_msg"> Esta sábado, 12 de novembro, das 10:00 às 17:00 no <a href="http://transparenciahackday.us6.list-manage.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=f55aa2687e&amp;e=b684bb14e9" target="_blank" class="gmail_msg" style="color: #2baadf; font-weight: normal; text-decoration: underline;" data-saferedirecturl="https://www.google.com/url?q=http://transparenciahackday.us6.list-manage.com/track/click?u%3D7c39a9eb2d8160fd5975e6fec%26id%3Df55aa2687e%26e%3Db684bb14e9&amp;source=gmail&amp;ust=1478897835608000&amp;usg=AFQjCNFprYsERIsIAU0igwdv0CIXn6MueQ">UPTEC PINC (Praça Coronel Pacheco)</a>. Traz uma tripla, portátil e um caderno. Nós levamos os dados ;)<br class="gmail_msg"> <br class="gmail_msg"> <span class="gmail_msg" style="font-size: 12px;"><span class="gmail_msg" style="color: #696969;">Guarda no calendário, o Date With Data seguinte é dia <strong class="gmail_msg">10 de dezembro</strong>!</span></span></p>
